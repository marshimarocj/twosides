import os
import json
import sys
sys.path.append('../')

import numpy as np

import model.vevd
import model.qvevd
import model.qvevd_score
import model.qvevd_score_sample
import model.qvevd_score_sample_fix
import model.qvevd_score_sample_reward
import model.qvevd_reward_margin
import model.qvevd_gpc
import model.qvevd_gpc_sample
import model.qvevd_gpc_all
import model.self_critique


'''func
'''
def get_mean_ft_files(root_dir, modal_feature_names, splits, dir_name='mp_feature'):
  dim_fts = []
  split_ftfiles = []
  for split in splits:
    ftfiles = []
    for name in modal_feature_names:
      ftfile = os.path.join(root_dir, dir_name, name, '%s_ft.npy'%split)
      ftfiles.append(ftfile)
      if split == 'val':
        data = np.load(ftfile)
        dim_ft = data.shape[1]

        dim_fts.append(dim_ft)

    split_ftfiles.append(ftfiles)

  return dim_fts, split_ftfiles


'''expr
'''
def prepare_vevd_cfg():
  # root_dir = '/mnt/data1/jiac/tgif' # neptune
  # root_dir = '/mnt/data1/jiac/mscoco' # neptune
  # root_dir = '/data1/jiac/mscoco' # mercurial
  root_dir = '/data1/jiac/MSCOCO' # uranus 
  modal_feature_names = [
    # 'i3d',
    # 'tf_resnet101',
    # 'resnet200',
    'tf_resnet101_450',
  ]
  split_dir = os.path.join(root_dir, 'split')
  # split_dir = os.path.join(root_dir, 'mao_split')
  annotation_dir = os.path.join(root_dir, 'aux')
  # outdir = os.path.join(root_dir, 'twosides', 'vevd_expr')
  outdir = os.path.join(root_dir, 'low_resource', 'vevd_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits, dir_name='mp_feature')

  params = {
    # 'num_step': 30,
    'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 50,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,
    'tst_task': 'generation',
    'topk': 10,
  }

  model_cfg = model.vevd.gen_cfg(**params)
  outprefix = '%s.%d.%d.%s'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    model_spec)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    # 'val_retrieve_file': os.path.join(split_dir, 'val_retrieve_id_caption_mask.pkl'),
    # 'tst_retrieve_file': os.path.join(split_dir, 'tst_retrieve_id_caption_mask.pkl'),
    'val_retrieve_file': '',
    'tst_retrieve_file': '',
    'output_dir': output_dir,
    'model_file': os.path.join(output_dir, 'model', 'epoch-146'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_vevd_low_resource_cfg():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  modal_feature_names = [
    'tf_resnet101_450',
  ]
  num_resource = 4
  split_dir = os.path.join(root_dir, 'split.%d'%num_resource)
  annotation_dir = os.path.join(root_dir, 'aux.%d'%num_resource)
  outdir = os.path.join(root_dir, 'low_resource', 'vevd_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 150,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,
    'tst_task': 'generation',
    'topk': 10,
  }

  model_cfg = model.vevd.gen_cfg(**params)
  outprefix = '%s.%d.%d.%s.%d'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    model_spec, num_resource)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'val_retrieve_file': '',
    'tst_retrieve_file': '',
    'output_dir': output_dir,
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_vevd_low_resource_equal_annotation_cfg():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  modal_feature_names = [
    'tf_resnet101_450',
  ]
  split_dir = os.path.join(root_dir, 'split.4.half')
  annotation_dir = os.path.join(root_dir, 'aux.4.half')
  outdir = os.path.join(root_dir, 'low_resource', 'vevd_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm.4.half'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 150,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,
    'tst_task': 'generation',
    'topk': 10,
  }

  model_cfg = model.vevd.gen_cfg(**params)
  outprefix = '%s.%d.%d.%s'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    model_spec)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'val_retrieve_file': '',
    'tst_retrieve_file': '',
    'output_dir': output_dir,
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_qvevd_cfg():
  # root_dir = '/data1/jiac/tgif' # uranus
  root_dir = '/data1/jiac/mscoco' # mercurial
  # root_dir = '/mnt/data1/jiac/tgif' # neptune
  modal_feature_names = [
    'resnet200',
    # 'i3d',
    # 'tf_resnet101'
  ]
  # split_dir = os.path.join(root_dir, 'split')
  split_dir = os.path.join(root_dir, 'mao_split')
  annotation_dir = os.path.join(root_dir, 'aux')
  outdir = os.path.join(root_dir, 'twosides', 'qvevd_expr')
  splits = ['trn', 'val', 'tst']
  # model_spec = 'lstm'
  model_spec = 'lstm.lifted'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits, dir_name='mao_mp_feature')

  params = {
    # 'num_step': 30,
    'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    # 'num_epoch': 200,
    # 'num_epoch': 100,
    # 'num_epoch': 50,
    'num_epoch': 20,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,
    # 'alpha': 0.5,
    'alpha': 1.,
    'num_neg': 16,
    'tst_task': 'generation',
    'topk': 10,
  }

  model_cfg = model.qvevd.gen_cfg(**params)
  outprefix = '%s.%d.%d.%.1f.%d.%s'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    params['alpha'], params['num_neg'], 
    model_spec)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    # 'quotient_file': os.path.join(root_dir, 'nli', 'retrieve.quotient.json'),
    'quotient_file': os.path.join(root_dir, 'nli', 'mao.retrieve.quotient.json'),
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'val_retrieve_file': os.path.join(split_dir, 'val_retrieve_id_caption_mask.pkl'),
    'tst_retrieve_file': os.path.join(split_dir, 'tst_retrieve_id_caption_mask.pkl'),
    'output_dir': output_dir,
    # 'model_file': os.path.join(output_dir, 'model', 'epoch-146'),
    'model_file': os.path.join(output_dir, 'model', 'pretrain'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_qvevd_score_cfg():
  # root_dir = '/data1/jiac/tgif' # uranus
  # root_dir = '/data1/jiac/MSCOCO' # uranus
  # root_dir = '/mnt/data1/jiac/tgif' # neptune
  # root_dir = '/data1/jiac/tgif' # mercurial
  root_dir = '/data1/jiac/mscoco' # mercurial
  modal_feature_names = [
    # 'i3d',
    # 'tf_resnet101',
    'resnet200',
  ]
  # split_dir = os.path.join(root_dir, 'split')
  split_dir = os.path.join(root_dir, 'mao_split')
  annotation_dir = os.path.join(root_dir, 'aux')
  outdir = os.path.join(root_dir, 'twosides', 'qvevd_score_expr')
  splits = ['trn', 'val', 'tst']
  # model_spec = 'lstm'
  model_spec = 'lstm.lifted.bleu'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  # dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)
  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits, dir_name='mao_mp_feature')

  params = {
    # 'num_step': 30,
    'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    # 'num_epoch': 200,
    'num_epoch': 20,
    # 'num_epoch': 100,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,
    # 'alpha': 0.5,
    'alpha': 1.0,
    'num_neg': 16,
    'max_margin': 0.5,
    'tst_task': 'generation',
    'topk': 10,
  }

  model_cfg = model.qvevd_score.gen_cfg(**params)
  model_cfg.metric = 'bleu'
  outprefix = '%s.%d.%d.%.1f.%d.%.1f.%s'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    params['alpha'], params['num_neg'], params['max_margin'],
    model_spec)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    # 'quotient_file': os.path.join(root_dir, 'nli', 'retrieve.quotient.json'),
    'quotient_file': os.path.join(root_dir, 'nli', 'mao.retrieve.quotient.json'),
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'val_retrieve_file': os.path.join(split_dir, 'val_retrieve_id_caption_mask.pkl'),
    'tst_retrieve_file': os.path.join(split_dir, 'tst_retrieve_id_caption_mask.pkl'),
    'output_dir': output_dir,
    # 'model_file': os.path.join(output_dir, 'model', 'epoch-146'),
    'model_file': os.path.join(output_dir, 'model', 'pretrain'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_qvevd_score_sample_cfg():
  root_dir = '/data1/jiac/tgif' # uranus
  modal_feature_names = [
    'i3d',
  ]
  split_dir = os.path.join(root_dir, 'split')
  annotation_dir = os.path.join(root_dir, 'aux')
  outdir = os.path.join(root_dir, 'twosides', 'qvevd_score_sample_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    'num_step': 30,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 200,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,

    'alphas': [.25, .25, .25],
    'num_neg': 16,
    'num_sample': 1,
    'max_margin': .5,
    'sn_delta_threshold': .3,
    'tst_task': 'generation',
    'topk': 10,
  }

  model_cfg = model.qvevd_score_sample.gen_cfg(**params)
  model_cfg.base_lr = 1e-5
  outprefix = '%s.%d.%d.%s.%d.%d.%.1f.1e-5.%s'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    '_'.join([str(d) for d in params['alphas']]),
    params['num_neg'], params['num_sample'], params['max_margin'],
    model_spec)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'quotient_file': os.path.join(root_dir, 'nli', 'retrieve.quotient.json'),
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'val_retrieve_file': os.path.join(split_dir, 'val_retrieve_id_caption_mask.pkl'),
    'tst_retrieve_file': os.path.join(split_dir, 'tst_retrieve_id_caption_mask.pkl'),
    'output_dir': output_dir,
    'model_file': os.path.join(output_dir, 'model', 'epoch-146'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_qvevd_score_sample_fix_cfg():
  # root_dir = '/mnt/data1/jiac/tgif' # neptune
  root_dir = '/jiac/data/tgif' # aws
  modal_feature_names = [
    'i3d',
  ]
  split_dir = os.path.join(root_dir, 'split')
  annotation_dir = os.path.join(root_dir, 'aux')
  outdir = os.path.join(root_dir, 'twosides', 'qvevd_score_sample_fix_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm'
  # trn_sample_file = os.path.join(root_dir, 'split', 'trn_id_caption_mask_sample.100.pkl')
  trn_sample_file = os.path.join(root_dir, 'split', 'trn_id_caption_mask_sample.500.pkl')

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    'num_step': 30,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 200,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,

    'alphas': [.25, .25, .25],
    'num_neg': 16,
    # 'num_sample': 1,
    'num_sample': 5,
    'max_margin': .5,
    'sn_delta_threshold': .3,
    'topk': 10,
  }

  model_cfg = model.qvevd_score_sample_fix.gen_cfg(**params)
  model_cfg.base_lr = 1e-5
  outprefix = '%s.%d.%d.%s.%d.%d.%.1f.1e-5.100.%s'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    '_'.join([str(d) for d in params['alphas']]),
    params['num_neg'], params['num_sample'], params['max_margin'],
    model_spec)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'quotient_file': os.path.join(root_dir, 'nli', 'retrieve.quotient.json'),
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'val_retrieve_file': os.path.join(split_dir, 'val_retrieve_id_caption_mask.pkl'),
    'tst_retrieve_file': os.path.join(split_dir, 'tst_retrieve_id_caption_mask.pkl'),
    'trn_sample_file': trn_sample_file,
    'output_dir': output_dir,
    'model_file': os.path.join(output_dir, 'model', 'epoch-146'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_qvevd_score_sample_reward_cfg():
  # root_dir = '/data1/jiac/tgif' # uranus
  root_dir = '/mnt/data1/jiac/tgif' # neptune
  modal_feature_names = [
    'i3d',
  ]
  split_dir = os.path.join(root_dir, 'split')
  annotation_dir = os.path.join(root_dir, 'aux')
  outdir = os.path.join(root_dir, 'twosides', 'qvevd_score_sample_reward_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    'num_step': 30,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 200,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,

    'alphas': [.2, .8],
    'num_neg': 16,
    'num_sample': 1,
    'max_margin': .5,
    'sn_delta_threshold': .3,
    'tst_task': 'generation',
    'topk': 10,
  }

  model_cfg = model.qvevd_score_sample_reward.gen_cfg(**params)
  outprefix = '%s.%d.%d.%s.%d.%d.%.1f.%s'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    '_'.join([str(d) for d in params['alphas']]),
    params['num_neg'], params['num_sample'], params['max_margin'],
    model_spec)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'quotient_file': os.path.join(root_dir, 'nli', 'retrieve.quotient.json'),
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'val_retrieve_file': os.path.join(split_dir, 'val_retrieve_id_caption_mask.pkl'),
    'tst_retrieve_file': os.path.join(split_dir, 'tst_retrieve_id_caption_mask.pkl'),
    'output_dir': output_dir,
    'model_file': os.path.join(output_dir, 'model', 'epoch-146'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_self_critique_cfg():
  root_dir = '/mnt/data1/jiac/tgif' # neptune
  modal_feature_names = [
    'i3d',
  ]
  split_dir = os.path.join(root_dir, 'split')
  annotation_dir = os.path.join(root_dir, 'aux')
  outdir = os.path.join(root_dir, 'twosides', 'self_critique')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    # 'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 100,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,

    'alpha': 1.,
    'num_sample': 1,
    'tst_task': 'generation',
    'topk': 10,
  }

  model_cfg = model.self_critique.gen_cfg(**params)
  outprefix = '%s.%d.%d.%.1f.%d.%s'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    params['alpha'], params['num_sample'],
    model_spec)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    # 'tst_retrieve_file': os.path.join(split_dir, 'tst_retrieve_id_caption_mask.pkl'),
    'output_dir': output_dir,
    'model_file': os.path.join(output_dir, 'model', 'pretrain'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_low_resource_sc_cfg():
  # root_dir = '/mnt/data1/jiac/mscoco' # neptune
  root_dir = '/data1/jiac/MSCOCO' # uranus
  modal_feature_names = [
    'tf_resnet101_450',
  ]
  num_resource = 1
  split_dir = os.path.join(root_dir, 'split.%d'%num_resource)
  annotation_dir = os.path.join(root_dir, 'aux.%d'%num_resource)
  outdir = os.path.join(root_dir, 'low_resource', 'vevd_sc_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 100,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,

    'alpha': 1.,
    'num_sample': 1,
    'tst_task': 'generation',
    'topk': 10,
  }

  model_cfg = model.self_critique.gen_cfg(**params)
  outprefix = '%s.%d.%d.%.1f.%d.%s.%d'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    params['alpha'], params['num_sample'],
    model_spec, num_resource)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'output_dir': output_dir,
    'model_file': os.path.join(output_dir, 'model', 'pretrain'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_low_resource_equal_annotation_sc_cfg():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  modal_feature_names = [
    'tf_resnet101_450',
  ]
  split_dir = os.path.join(root_dir, 'split.4.half')
  annotation_dir = os.path.join(root_dir, 'aux.4.half')
  outdir = os.path.join(root_dir, 'low_resource', 'vevd_sc_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm.4.half'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 50,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,

    'alpha': 1.,
    'num_sample': 1,
    'tst_task': 'generation',
    'topk': 10,
  }

  model_cfg = model.self_critique.gen_cfg(**params)
  outprefix = '%s.%d.%d.%.1f.%d.%s'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    params['alpha'], params['num_sample'],
    model_spec)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'output_dir': output_dir,
    'model_file': os.path.join(output_dir, 'model', 'pretrain'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_qvevd_reward_margin_cfg():
  # root_dir = '/mnt/data1/jiac/tgif' # neptune
  root_dir = '/data1/jiac/MSCOCO' # uranus
  # root_dir = '/mnt/data1/jiac/mscoco' # neptune
  # root_dir = '/jiac/data/mscoco' # gpu9
  # root_dir = '/jiac/hdd/mscoco' # aws
  # root_dir = '/qjin/data/MSCOCO' # sun
  modal_feature_names = [
    # 'i3d',
    # 'tf_resnet101',
    'tf_resnet101_450',
    # 'resnet200',
  ]
  split_dir = os.path.join(root_dir, 'split')
  # split_dir = os.path.join(root_dir, 'kapathy_split')
  annotation_dir = os.path.join(root_dir, 'aux')
  # outdir = os.path.join(root_dir, 'twosides', 'qvevd_reward_margin_expr')
  outdir = os.path.join(root_dir, 'low_resource', 'vevd_reward_margin_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm'
  # model_spec = 'lstm.sc'
  # model_spec = 'lstm.bcmr.sc'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    # 'num_step': 30,
    'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 50,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,

    'alpha': 1.,
    'reward_alpha': .5,
    'num_neg': 16,
    'num_hard_neg': 3,
    'num_sample': 5,
    'margin': .1,
    'tst_task': 'generation',
    'topk': 10,
  }

  model_cfg = model.qvevd_reward_margin.gen_cfg(**params)
  # model_cfg.metric = 'bcmr'
  outprefix = '%s.%d.%d.%.1f.%.1f.%d.%d.%d.%.1f.%s'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    params['alpha'], params['reward_alpha'],
    params['num_neg'], params['num_hard_neg'], params['num_sample'], params['margin'],
    model_spec)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'quotient_file': os.path.join(root_dir, 'nli', 'retrieve.quotient.json'),
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    # 'val_retrieve_file': os.path.join(split_dir, 'val_retrieve_id_caption_mask.pkl'),
    # 'tst_retrieve_file': os.path.join(split_dir, 'tst_retrieve_id_caption_mask.pkl'),
    'val_retrieve_file': '',
    'tst_retrieve_file': '',
    'output_dir': output_dir,
    'model_file': os.path.join(output_dir, 'model', 'pretrain'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_low_resource_qvevd_reward_margin_cfg():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  # root_dir = '/data1/jiac/MSCOCO' # uranus
  # root_dir = '/home/jiac/hdd/mscoco' # aws1
  modal_feature_names = [
    'tf_resnet101_450',
  ]
  num_resource = 1
  split_dir = os.path.join(root_dir, 'split.%d'%num_resource)
  annotation_dir = os.path.join(root_dir, 'aux.%d'%num_resource)
  outdir = os.path.join(root_dir, 'low_resource', 'vevd_reward_margin_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 50,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,

    'alpha': 1.,
    'reward_alpha': .5,
    # 'num_neg': 16,
    # 'num_hard_neg': 3,
    # 'num_sample': 5,
    'num_neg': 20,
    'num_hard_neg': 20,
    'num_sample': 1,
    'margin': .1,
    'tst_task': 'generation',
    'topk': 10,
  }

  model_cfg = model.qvevd_reward_margin.gen_cfg(**params)
  outprefix = '%s.%d.%d.%.1f.%.1f.%d.%d.%d.%.1f.%s.%d'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    params['alpha'], params['reward_alpha'],
    params['num_neg'], params['num_hard_neg'], params['num_sample'], params['margin'],
    model_spec, num_resource)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'quotient_file': os.path.join(split_dir, 'quotient.json'),
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'val_retrieve_file': '',
    'tst_retrieve_file': '',
    'output_dir': output_dir,
    'model_file': os.path.join(output_dir, 'model', 'pretrain'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_low_resource_qvevd_gpc_cfg():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  modal_feature_names = [
    'tf_resnet101_450',
  ]
  num_resource = 1
  split_dir = os.path.join(root_dir, 'split.%d'%num_resource)
  annotation_dir = os.path.join(root_dir, 'aux.%d'%num_resource)
  outdir = os.path.join(root_dir, 'low_resource', 'vevd_gpc_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 100,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,

    'num_neg': 5,
    'num_hard_neg': 5,
    'num_sample': 1,
    # 'margin': .1,
    'margin': .0,
    'topk': 10,
  }

  model_cfg = model.qvevd_gpc.gen_cfg(**params)
  outprefix = '%s.%d.%d.%d.%d.%d.%.1f.%s.%d'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    params['num_neg'], params['num_hard_neg'], params['num_sample'], params['margin'],
    model_spec, num_resource)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'quotient_file': os.path.join(split_dir, 'quotient.json'),
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'val_retrieve_file': '',
    'tst_retrieve_file': '',
    'output_dir': output_dir,
    'model_file': os.path.join(output_dir, 'model', 'pretrain'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_low_resource_qvevd_gpc_sample_cfg():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  modal_feature_names = [
    'tf_resnet101_450',
  ]
  num_resource = 1
  split_dir = os.path.join(root_dir, 'split.%d'%num_resource)
  annotation_dir = os.path.join(root_dir, 'aux.%d'%num_resource)
  outdir = os.path.join(root_dir, 'low_resource', 'vevd_gpc_sample_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 100,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,

    'num_neg': 5,
    'num_hard_neg': 5,
    'num_sample': 1,
    'margin': 0.,
    'topk': 10,
  }

  model_cfg = model.qvevd_gpc_sample.gen_cfg(**params)
  outprefix = '%s.%d.%d.%d.%d.%d.%.1f.%s.%d'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    params['num_neg'], params['num_hard_neg'], params['num_sample'], params['margin'],
    model_spec, num_resource)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'quotient_file': os.path.join(split_dir, 'quotient.json'),
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'val_retrieve_file': '',
    'tst_retrieve_file': '',
    'output_dir': output_dir,
    'model_file': os.path.join(output_dir, 'model', 'pretrain'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


def prepare_low_resource_qvevd_gpc_all_cfg():
  # root_dir = '/mnt/data1/jiac/mscoco' # neptune
  root_dir = '/hdd/mscoco' # my-aws
  modal_feature_names = [
    'tf_resnet101_450',
  ]
  num_resource = 5
  split_dir = os.path.join(root_dir, 'split.%d'%num_resource)
  annotation_dir = os.path.join(root_dir, 'aux.%d'%num_resource)
  outdir = os.path.join(root_dir, 'low_resource', 'vevd_gpc_all_expr')
  splits = ['trn', 'val', 'tst']
  model_spec = 'lstm'

  if not os.path.exists(outdir):
    os.mkdir(outdir)

  dim_fts, split_ftfiles = get_mean_ft_files(root_dir, modal_feature_names, splits)

  params = {
    'num_step': 20,
    'dim_input': 512,
    'dim_hidden': 512,
    'num_epoch': 100,
    'content_keepin_prob': 1.,
    'cell_keepin_prob': 0.5,
    'cell_keepout_prob': 0.5,
    'dim_fts': dim_fts,
    # 'margin': .0,
    'margin': .1,

    'num_gt_neg': 2,
    'num_greedy_neg': 1,
    'num_sample_neg': 2,
    'num_hard_neg': 5,
    'num_sample': 1,
    'topk': 10,
  }

  model_cfg = model.qvevd_gpc_all.gen_cfg(**params)
  outprefix = '%s.%d.%d.%d.%d.%d.%d.%d.%.1f.%s.%d'%(
    os.path.join(outdir, '_'.join(modal_feature_names)),
    params['dim_hidden'], params['dim_input'],
    params['num_gt_neg'], params['num_greedy_neg'], params['num_sample_neg'],
    params['num_hard_neg'], params['num_sample'],
    params['margin'],
    model_spec, num_resource)
  model_cfg_file = '%s.model.json'%outprefix
  model_cfg.save(model_cfg_file)

  output_dir = outprefix
  path_cfg = {
    'trn_ftfiles': split_ftfiles[0],
    'val_ftfiles': split_ftfiles[1],
    'tst_ftfiles': split_ftfiles[2],
    'quotient_file': os.path.join(split_dir, 'quotient.json'),
    'split_dir': split_dir,
    'annotation_dir': annotation_dir,
    'val_retrieve_file': '',
    'tst_retrieve_file': '',
    'output_dir': output_dir,
    'model_file': os.path.join(output_dir, 'model', 'pretrain'),
  }
  path_cfg_file = '%s.path.json'%outprefix

  if not os.path.exists(path_cfg['output_dir']):
    os.mkdir(path_cfg['output_dir'])

  json.dump(path_cfg, open(path_cfg_file, 'w'), indent=2)


if __name__ == '__main__':
  # prepare_vevd_cfg()
  # prepare_qvevd_cfg()
  # prepare_qvevd_score_cfg()
  # prepare_qvevd_score_sample_cfg()
  # prepare_qvevd_score_sample_fix_cfg()
  # prepare_qvevd_score_sample_reward_cfg()
  # prepare_self_critique_cfg()
  # prepare_qvevd_reward_margin_cfg()

  # prepare_vevd_low_resource_cfg()
  # prepare_vevd_low_resource_equal_annotation_cfg()
  # prepare_low_resource_sc_cfg()
  # prepare_low_resource_equal_annotation_sc_cfg()
  # prepare_low_resource_qvevd_reward_margin_cfg()
  # prepare_low_resource_qvevd_gpc_cfg()
  # prepare_low_resource_qvevd_gpc_sample_cfg()
  prepare_low_resource_qvevd_gpc_all_cfg()
