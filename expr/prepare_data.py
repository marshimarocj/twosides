import os
import cPickle
import random
import json
import sys
sys.path.append('../')

import numpy as np

# import framework.util.caption.utility
# import model.fast_cider


'''func
'''
def caption2captionid_mask(caption, num_step, word2wid):
  captionid = np.zeros((num_step,), dtype=np.int32)
  caption_mask = np.zeros((num_step,), dtype=np.float32)
  words = caption.split(' ')
  for i, word in enumerate(words):
    if i+1 == num_step:
      break
    if word in word2wid:
      wid = word2wid[word]
    else:
      wid = 2
    captionid[i+1] = wid
    caption_mask[i+1] = 1.
  i = i+1
  if i+1 < num_step:
    captionid[i+1] = 1
    caption_mask[i+1] = 1.

  return captionid, caption_mask


'''expr
'''
def prepare_data_for_retrieval():
  # root_dir = '/data1/jiac/tgif' # uranus
  # root_dir = '/data1/jiac/MSCOCO' # uranus
  root_dir = '/data1/jiac/mscoco' # mercurial
  caption_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')
  word_file = os.path.join(root_dir, 'aux', 'int2word.pkl')
  # split_file = os.path.join(root_dir, 'split', 'tst_videoids.npy')
  # out_file = os.path.join(root_dir, 'split', 'tst_retrieve_id_caption_mask.pkl')
  # split_file = os.path.join(root_dir, 'split', 'val_videoids.npy')
  # out_file = os.path.join(root_dir, 'split', 'val_retrieve_id_caption_mask.pkl')
  split_file = os.path.join(root_dir, 'mao_split', 'val_videoids.npy')
  out_file = os.path.join(root_dir, 'mao_split', 'val_retrieve_id_caption_mask.pkl')

  # num_sample = 1000
  num_sample = 500

  # num_step = 30
  num_step = 20

  with open(word_file) as f:
    words = cPickle.load(f)
  word2wid = {}
  for i, word in enumerate(words):
    word2wid[word] = i

  with open(caption_file) as f:
    vid2captions = cPickle.load(f)

  vids = np.load(split_file)

  idxs = range(len(vids))
  random.shuffle(idxs)

  ft_idxs = []
  captionids = []
  caption_masks = []
  for i in range(num_sample):
    idx = idxs[i]
    vid = vids[idx]
    caption = vid2captions[vid][0].split(' ')

    captionid = np.zeros((num_step,), dtype=np.int32)
    caption_mask = np.zeros((num_step,), dtype=np.float32)
    caption_mask[0] = 1.
    for j, word in enumerate(caption):
      if j + 1 == num_step:
        break
      if caption[j] in word2wid:
        wid = word2wid[caption[j]]
      else:
        wid = 2
      captionid[j+1] = wid
      caption_mask[j+1] = 1.
    j += 1
    if j+1 < num_step:
      captionid[j+1] = 1
      caption_mask[j+1] = 1.

    ft_idxs.append(idx)
    captionids.append(captionid)
    caption_masks.append(caption_mask)

  ft_idxs = np.array(ft_idxs, dtype=np.int32)
  captionids = np.array(captionids, dtype=np.int32)
  caption_masks = np.array(caption_masks, dtype=np.float32)

  with open(out_file, 'w') as fout:
    cPickle.dump((ft_idxs, captionids, caption_masks), fout)


def prepare_sample_captions():
  # root_dir = '/mnt/data1/jiac/tgif' # neptune
  root_dir = '/home/jiac/data/tgif' # aws
  word_file = os.path.join(root_dir, 'aux', 'int2word.pkl')
  sample_file = os.path.join(root_dir, 'twosides', 'vevd_expr', 'i3d.512.512.lstm', 'pred', 'epoch-146.1000.-1.sample.trn.json')
  # sample_file = os.path.join(root_dir, 'twosides', 'vevd_expr', 'i3d.512.512.lstm', 'pred', 'epoch-146.100.-1.sample.trn.json')
  gt_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')
  out_file = os.path.join(root_dir, 'split', 'trn_id_caption_mask_sample.500.pkl')
  # out_file = os.path.join(root_dir, 'split', 'trn_id_caption_mask_sample.100.pkl')

  num_step = 30

  with open(word_file) as f:
    data = cPickle.load(f)
  word2wid = {}
  for i, word in enumerate(data):
    word2wid[word] = i

  with open(gt_file) as f:
    vid2captions = cPickle.load(f)

  cider = model.fast_cider.CiderScorer()
  cider.init_refs(vid2captions)

  int2str = framework.util.caption.utility.CaptionInt2str(word_file)

  captionids = []
  caption_masks = []
  ciders = []
  vids = []
  with open(sample_file) as f:
    data = json.load(f)
  print 'load complete'
  cnt = 0
  for key in data:
    vid = int(key)
    for score, caption in data[key][:500]:
      captionid, caption_mask = caption2captionid_mask(caption, num_step, word2wid)
      score, scores = cider.compute_cider([caption], [vid])
      captionids.append(captionid)
      caption_masks.append(caption_mask)
      ciders.append(score)
      vids.append(vid)
    cnt += 1
    if cnt % 100 == 0:
      print cnt

  captionids = np.array(captionids, dtype=np.int32)
  caption_masks = np.array(caption_masks, dtype=np.float32)
  ciders = np.array(ciders, dtype=np.float32)
  vids = np.array(vids, dtype=np.int32)

  with open(out_file, 'w') as fout:
    cPickle.dump([vids, captionids, caption_masks, ciders], fout)


def check_kapathy_mao_split():
  root_dir = '/home/jiac/data/caption' # earth
  mao_tst_split_file = os.path.join(root_dir, 'mao', 'ms_coco_train_val_test_img_list', 'ms_coco_test_list_mRNN.txt')
  kapathy_trn_split_file = os.path.join(root_dir, 'kapathy_split', 'trn_names.npy')

  mao_tst_names = set()
  with open(mao_tst_split_file) as f:
    for line in f:
      line = line.strip()
      mao_tst_names.add(line)

  kapathy_trn_names = np.load(kapathy_trn_split_file)

  cnt_intersect = 0
  for name in kapathy_trn_names:
    if name in mao_tst_names:
      cnt_intersect += 1

  print len(mao_tst_names), cnt_intersect


def prepare_mao_split_ft():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  mao_split_files = [
    os.path.join(root_dir, 'mao_split', 'ms_coco_train_val_test_img_list', 'ms_coco_train_original_list_mRNN.txt'),
    os.path.join(root_dir, 'mao_split', 'ms_coco_train_val_test_img_list', 'ms_coco_train_from_val_list_mRNN.txt'),
    os.path.join(root_dir, 'mao_split', 'ms_coco_train_val_test_img_list', 'ms_coco_val_list_mRNN.txt'),
    os.path.join(root_dir, 'mao_split', 'ms_coco_train_val_test_img_list', 'ms_coco_test_list_mRNN.txt'),
  ]
  split_files = [
    os.path.join(root_dir, 'split', 'trn_names.npy'),
    os.path.join(root_dir, 'split', 'val_names.npy'),
    os.path.join(root_dir, 'split', 'tst_names.npy'),
  ]
  # ft_dir = os.path.join(root_dir, 'mp_feature', 'tf_resnet101')
  # mao_ft_dir = os.path.join(root_dir, 'mao_mp_feature', 'tf_resnet101')
  ft_dir = os.path.join(root_dir, 'mp_feature', 'resnet200')
  mao_ft_dir = os.path.join(root_dir, 'mao_mp_feature', 'resnet200')

  dim_ft = 2048

  # mao map
  name2mao_idx = {}
  num_mao = []

  idx = 0
  for mao_split_file in mao_split_files[:2]:
    with open(mao_split_file) as f:
      for line in f:
        name = line.strip()
        name2mao_idx[name] = (0, idx)
        idx += 1
  num_mao.append(idx)
  for i in range(2, 4):
    mao_split_file = mao_split_files[i]
    with open(mao_split_file) as f:
      for j, line in enumerate(f):
        name = line.strip()
        name2mao_idx[name] = (i-1, j)
    num_mao.append(j+1)

  # kapathy
  split_names = []
  for split_file in split_files:
    names = np.load(split_file)
    split_names.append(names)

  mao_fts = [np.zeros((num, dim_ft), dtype=np.float32) for num in num_mao]

  splits = ['trn', 'val', 'tst']
  for s, split in enumerate(splits):
    ft_file = os.path.join(ft_dir, '%s_ft.npy'%split)
    fts = np.load(ft_file)
    for i, ft in enumerate(fts):
      name = split_names[s][i]
      mao_split, idx = name2mao_idx[name]
      mao_fts[mao_split][idx] = ft

  for s in range(3):
    out_file = os.path.join(mao_ft_dir, '%s_ft.npy'%splits[s])
    np.save(out_file, mao_fts[s])


def prepare_mao_split_caption():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  mao_split_files = [
    os.path.join(root_dir, 'mao_split', 'ms_coco_train_val_test_img_list', 'ms_coco_train_original_list_mRNN.txt'),
    os.path.join(root_dir, 'mao_split', 'ms_coco_train_val_test_img_list', 'ms_coco_train_from_val_list_mRNN.txt'),
    os.path.join(root_dir, 'mao_split', 'ms_coco_train_val_test_img_list', 'ms_coco_val_list_mRNN.txt'),
    os.path.join(root_dir, 'mao_split', 'ms_coco_train_val_test_img_list', 'ms_coco_test_list_mRNN.txt'),
  ]
  split_files = [
    os.path.join(root_dir, 'split', 'trn_names.npy'),
    os.path.join(root_dir, 'split', 'val_names.npy'),
    os.path.join(root_dir, 'split', 'tst_names.npy'),
  ]
  vid_files = [
    os.path.join(root_dir, 'split', 'trn_videoids.npy'),
    os.path.join(root_dir, 'split', 'val_videoids.npy'),
    os.path.join(root_dir, 'split', 'tst_videoids.npy'),
  ]
  gt_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')
  word_file = os.path.join(root_dir, 'aux', 'int2word.pkl')
  out_files = [
    os.path.join(root_dir, 'mao_split', 'trn_id_caption_mask.pkl'),
    os.path.join(root_dir, 'mao_split', 'val_id_caption_mask.pkl'),
    os.path.join(root_dir, 'mao_split', 'tst_id_caption_mask.pkl'),
  ]
  out_vid_files = [
    os.path.join(root_dir, 'mao_split', 'trn_videoids.npy'),
    os.path.join(root_dir, 'mao_split', 'val_videoids.npy'),
    os.path.join(root_dir, 'mao_split', 'tst_videoids.npy'),
  ]

  num_step = 20

  with open(gt_file) as f:
    vid2captions = cPickle.load(f)

  with open(word_file) as f:
    words = cPickle.load(f)
  word2wid = {}
  for i, word in enumerate(words):
    word2wid[word] = i

  name2vid = {}
  for split_file, vid_file in zip(split_files, vid_files):
    names = np.load(split_file)
    vids = np.load(vid_file)
    for name, vid in zip(names, vids):
      name2vid[name] = vid

  ft_idxs = []
  captionids = []
  caption_masks = []
  vids = []
  idx = 0
  for i in range(2):
    mao_split_file = mao_split_files[i]
    with open(mao_split_file) as f:
      for line in f:
        name = line.strip()
        vid = name2vid[name]
        captions = vid2captions[vid]
        for caption in captions:
          captionid, caption_mask = caption2captionid_mask(caption, num_step, word2wid)
          ft_idxs.append(idx)
          captionids.append(captionid)
          caption_masks.append(caption_mask)
        vids.append(vid)
        idx += 1
  ft_idxs = np.array(ft_idxs, dtype=np.int32)
  captionids = np.array(captionids, dtype=np.int32)
  caption_masks = np.array(caption_masks, dtype=np.float32)
  with open(out_files[0], 'w') as fout:
    cPickle.dump([ft_idxs, captionids, caption_masks], fout)
  np.save(out_vid_files[0], vids)

  for s in range(1, 3):
    ft_idxs = []
    captionids = []
    caption_masks = []
    vids = []
    idx = 0
    with open(mao_split_files[s+1]) as f:
      for line in f:
        name = line.strip()
        vid = name2vid[name]
        captions = vid2captions[vid]
        for caption in captions:
          captionid, caption_mask = caption2captionid_mask(caption, num_step, word2wid)
          ft_idxs.append(idx)
          captionids.append(captionid)
          caption_masks.append(caption_mask)
        vids.append(vid)
        idx += 1
    ft_idxs = np.array(ft_idxs, dtype=np.int32)
    captionids = np.array(captionids, dtype=np.int32)
    caption_masks = np.array(caption_masks, dtype=np.float32)
    with open(out_files[s], 'w') as fout:
      cPickle.dump([ft_idxs, captionids, caption_masks], fout)
    np.save(out_vid_files[s], vids)


def mao_quotient_space():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  trn_split_file = os.path.join(root_dir, 'split', 'trn_videoids.npy')
  trn_caption_file = os.path.join(root_dir, 'split', 'trn_id_caption_mask.pkl')
  mao_trn_split_file = os.path.join(root_dir, 'split', 'trn_videoids.npy')
  mao_caption_file = os.path.join(root_dir, 'split', 'trn_id_caption_mask.pkl')
  quotient_file = os.path.join(root_dir, 'nli', 'retrieve.quotient.json')
  mao_quotient_file = os.path.join(root_dir, 'nli', 'mao.retrieve.quotient.json')

  vids = np.load(trn_split_file)
  mao_vids = np.load(mao_trn_split_file)
  mao_vid2ft_idx = {}
  for i, mao_vid in enumerate(mao_vids):
    mao_vid2ft_idx[mao_vid] = i

  with open(trn_caption_file) as f:
    data = cPickle.load(f)
    ft_idxs = data[0]
  vid2base_idx = {}
  caption_idx2vid = {}
  for i, ft_idx in enumerate(ft_idxs):
    vid = vids[ft_idx]
    if vid not in vid2base_idx:
      vid2base_idx[vid] = i
    caption_idx2vid[i] = vid

  with open(mao_caption_file) as f:
    data = cPickle.load(f)
    mao_ft_idxs = data[0]
  mao_vid2base_idxs = {}
  for i, ft_idx in enumerate(mao_ft_idxs):
    vid = mao_vids[ft_idx]
    if vid not in mao_vid2base_idxs:
      mao_vid2base_idxs[vid] = i

  with open(quotient_file) as f:
    quotient_dict = json.load(f)
  mao_quotient_dict = {}
  for key in quotient_dict:
    caption_idxs = quotient_dict[key]
    key = int(key)
    vid = vids[ft_idxs[key]]
    if vid not in mao_vid2base_idxs:
      continue
    mao_ft_idx = mao_vid2ft_idx[vid]
    quotient = []
    for caption_idx in caption_idxs:
      vid = caption_idx2vid[caption_idx]
      base_idx = vid2base_idx[vid]
      delta = caption_idx - base_idx
      if vid not in mao_vid2base_idxs:
        continue
      mao_caption_idx = mao_vid2base_idxs[vid] + delta
      quotient.append(mao_caption_idx)
    mao_quotient_dict[mao_ft_idx] = quotient

  with open(mao_quotient_file, 'w') as fout:
    json.dump(mao_quotient_dict, fout, indent=2)


def prepare_df():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  word_file = os.path.join(root_dir, 'aux', 'int2word.pkl')
  caption_file = os.path.join(root_dir, 'mao_split', 'trn_id_caption_mask.pkl')
  out_file = os.path.join(root_dir, 'aux', 'df.trn.npy')

  with open(word_file) as f:
    words = cPickle.load(f)

  df = np.zeros((len(words),))
  with open(caption_file) as f:
    ft_idxs, captionids, caption_masks = cPickle.load(f)
  for captionid, caption_mask in zip(captionids, caption_masks):
    num_step = captionids.shape[1]
    for i in range(num_step):
      if caption_mask[i] == 1. and captionid[i] != 1:
        df[captionid[i]] += 1

  np.save(out_file, df)


def prepare_low_resource_data():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  split_file = os.path.join(root_dir, 'split', 'trn_videoids.npy')
  annotation_file = os.path.join(root_dir, 'split', 'trn_id_caption_mask.pkl')
  caption_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')

  # out_annotation_template = os.path.join(root_dir, 'split.%d', 'trn_id_caption_mask.pkl')
  out_caption_template = os.path.join(root_dir, 'aux.%d', 'human_caption_dict.pkl')

  vids = np.load(split_file)

  with open(annotation_file) as f:
    ft_idxs, captionids, caption_masks = cPickle.load(f)

  with open(caption_file) as f:
    vid2captions = cPickle.load(f)

  for num in range(1, 5):
    print num
    out_ft_idxs = []
    out_captionids = []
    out_caption_masks = []
    out_vid2captions = {}

    vid2hit = {}
    for ft_idx, captionid, caption_mask in zip(ft_idxs, captionids, caption_masks):
      vid = vids[ft_idx]
      if vid in vid2hit and vid2hit[vid] == num:
        continue
      if vid not in vid2hit:
        vid2hit[vid] = 0
      vid2hit[vid] += 1

      out_ft_idxs.append(ft_idx)
      out_captionids.append(captionid)
      out_caption_masks.append(caption_mask)
    out_ft_idxs = np.array(out_ft_idxs, dtype=np.int32)
    out_captionids = np.array(out_captionids, dtype=np.int32)
    out_caption_masks = np.array(out_caption_masks, dtype=np.float32)

    for vid in vids:
      out_vid2captions[vid] = vid2captions[vid][:num]
    for vid in vid2captions:
      if vid not in out_vid2captions:
        out_vid2captions[vid] = vid2captions[vid]

    # out_annotation_file = out_annotation_template%num
    # with open(out_annotation_file, 'w') as fout:
    #   cPickle.dump([out_ft_idxs, out_captionids, out_caption_masks], fout)

    out_caption_file = out_caption_template%num
    with open(out_caption_file, 'w') as fout:
      cPickle.dump(out_vid2captions, fout)


def prepare_low_resource_equal_annotation_data():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  split_file = os.path.join(root_dir, 'split', 'trn_videoids.npy')
  annotation_file = os.path.join(root_dir, 'split', 'trn_id_caption_mask.pkl')
  caption_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')

  out_annotation_file = os.path.join(root_dir, 'split.4.half', 'trn_id_caption_mask.pkl')
  out_caption_file = os.path.join(root_dir, 'aux.4.half', 'human_caption_dict.pkl')

  num = 4

  vids = np.load(split_file)

  with open(annotation_file) as f:
    ft_idxs, captionids, caption_masks = cPickle.load(f)

  with open(caption_file) as f:
    vid2captions = cPickle.load(f)

  out_ft_idxs = []
  out_captionids = []
  out_caption_masks = []
  out_vid2captions = {}

  vid2hit = {}
  for ft_idx, captionid, caption_mask in zip(ft_idxs, captionids, caption_masks):
    vid = vids[ft_idx]
    if vid in vid2hit and vid2hit[vid] == num:
      continue
    if vid not in vid2hit:
      vid2hit[vid] = 0
    vid2hit[vid] += 1

    out_ft_idxs.append(ft_idx)
    out_captionids.append(captionid)
    out_caption_masks.append(caption_mask)
  out_ft_idxs = np.array(out_ft_idxs, dtype=np.int32)
  out_captionids = np.array(out_captionids, dtype=np.int32)
  out_caption_masks = np.array(out_caption_masks, dtype=np.float32)
  
  total = out_ft_idxs.shape[0]
  half = total / 2
  half = half - half%2 # even number ensures alignment between vid2captions and captionids
  out_ft_idxs = out_ft_idxs[:half]
  out_captionids = out_captionids[:half]
  out_caption_masks = out_caption_masks[:half]

  half_vids = set()
  for ft_idx in out_ft_idxs:
    vid = vids[ft_idx]
    half_vids.add(vid)

  for vid in half_vids:
    out_vid2captions[vid] = vid2captions[vid][:num]
  for vid in vid2captions:
    if vid not in out_vid2captions:
      out_vid2captions[vid] = vid2captions[vid]

  with open(out_annotation_file, 'w') as fout:
    cPickle.dump([out_ft_idxs, out_captionids, out_caption_masks], fout)

  with open(out_caption_file, 'w') as fout:
    cPickle.dump(out_vid2captions, fout)


def prepare_low_resource_quotient_file():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  split_file = os.path.join(root_dir, 'split', 'trn_videoids.npy')
  annotation_template = os.path.join(root_dir, 'split.%d', 'trn_id_caption_mask.pkl')
  out_template = os.path.join(root_dir, 'split.%d', 'quotient.json')

  for num in range(1, 5):
    annotation_file = annotation_template%num
    with open(annotation_file) as f:
      ft_idxs, captionids, caption_masks = cPickle.load(f)

    ft_idx2idxs = {}
    for i, ft_idx in enumerate(ft_idxs):
      ft_idx = int(ft_idx)
      if ft_idx not in ft_idx2idxs:
        ft_idx2idxs[ft_idx] = []
      ft_idx2idxs[ft_idx].append(i)

    out_file = out_template%num
    with open(out_file, 'w') as fout:
      json.dump(ft_idx2idxs, fout, indent=2)


if __name__ == '__main__':
  # prepare_data_for_retrieval()
  # prepare_sample_captions()
  # check_kapathy_mao_split()
  # prepare_mao_split_ft()
  # prepare_mao_split_caption()
  # mao_quotient_space()
  # prepare_df()

  # prepare_low_resource_data()
  prepare_low_resource_equal_annotation_data()
  # prepare_low_resource_quotient_file()
