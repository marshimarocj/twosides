import os
import json
import subprocess
import requests
import itertools
import sys
import cPickle
sys.path.append('../')

import numpy as np

from bleu.bleu import Bleu
from cider.cider import Cider
from rouge.rouge import Rouge
from meteor.meteor import Meteor
from model.fast_cider import CiderScorer


'''func
'''
def get_res_gts_dict(res_file, gts_file):
  human_caption = cPickle.load(file(gts_file))
  data = json.load(file(res_file))

  res, gts = {}, {}
  for key, value in data.iteritems():
    gts[key] = human_caption[int(key)]
    res[key] = [value]

  return res, gts


def eval_generation(predict_file, groundtruth_file):
  res, gts = get_res_gts_dict(predict_file, groundtruth_file)

  bleu_scorer = Bleu(4)
  meteor_scorer = Meteor()
  rouge_scorer = Rouge()
  cider_scorer = Cider()
  # closest score
  res_bleu, _ = bleu_scorer.compute_score(gts, res)
  # metero handles the multi references (don't know the details yet)
  res_meteor, _ = meteor_scorer.compute_score(gts, res)
  meteor_scorer.meteor_p.kill()
  # average
  res_rouge, _ = rouge_scorer.compute_score(gts, res)
  # average
  res_cider, _ = cider_scorer.compute_score(gts, res)

  out = {
    'bleu': res_bleu, 
    'meteor': res_meteor,
    'rouge': res_rouge,
    'cider': res_cider
  }

  return out


def eval_retrieval(predict_file):
  pred_idxs = np.load(predict_file)

  r1, r5, r10 = 0., 0., 0.
  for i, pred_idx in enumerate(pred_idxs):
    r1 += np.sum(pred_idx[:1] == i)
    r5 += np.sum(pred_idx[:5] == i)
    r10 += np.sum(pred_idx[:10] == i)
  num = pred_idxs.shape[0]

  return {
    'r1': r1 / num * 100,
    'r5': r5 / num * 100,
    'r10': r10 / num * 100,
  }


def eval_retrieval_mscoco(predict_file, split_file):
  pred_idxs = np.load(predict_file)

  with open(split_file) as f:
    ft_idxs, captionids, caption_masks = cPickle.load(f)

  r1, r5, r10 = 0., 0., 0.
  for i, pred_idx in enumerate(pred_idxs):
    r1 += np.max(ft_idxs[pred_idx[:1]] == i)
    r5 += np.max(ft_idxs[pred_idx[:5]] == i)
    r10 += np.max(ft_idxs[pred_idx[:10]] == i)
  num = pred_idxs.shape[0]

  return {
    'r1': r1 / num * 100,
    'r5': r5 / num * 100,
    'r10': r10 / num * 100,
  }


def select_generation(logdir, lower=-1, upper=-1):
  names = os.listdir(logdir)
  bleu4s = []
  ciders = []
  epochs = []
  for name in names:
    if 'val_metrics' not in name:
      continue
    val_file = os.path.join(logdir, name)
    with open(val_file) as f:
      data = json.load(f)
    epoch = data['epoch']
    if (lower != -1 and epoch < lower) or (upper != -1 and epoch >= upper):
      continue
    bleu4s.append(data['bleu4'])
    ciders.append(data['cider'])
    epochs.append(data['epoch'])

  best_epochs = set()
  
  idx = np.argmax(bleu4s)
  epoch = epochs[int(idx)]
  best_epochs.add(epoch)

  idx = np.argmax(ciders)
  epoch = epochs[int(idx)]
  best_epochs.add(epoch)

  return best_epochs


def select_retrieval(logdir, lower=-1, upper=-1):
  names = os.listdir(logdir)
  r5s = []
  r10s = []
  epochs = []
  for name in names:
    if 'val_metrics' not in name:
      continue
    val_file = os.path.join(logdir, name)
    with open(val_file) as f:
      data = json.load(f)
    epoch = data['epoch']
    if (lower != -1 and epoch < lower) or (upper != -1 and epoch >= upper):
      continue
    r5s.append(data['r5'])
    r10s.append(data['r10'])
    epochs.append(data['epoch'])

  best_epochs = set()

  idx = np.argmax(r5s)
  epoch = epochs[int(idx)]
  best_epochs.add(epoch)

  idx = np.argmax(r10s)
  epoch = epochs[int(idx)]
  best_epochs.add(epoch)

  return best_epochs


def predict(python_file, model_cfg_file, path_cfg_file, best_epochs, gpuid, **kwargs):
  with open('eval.sh', 'w') as fout:
    fout.write('#!/bin/sh\n')
    fout.write('export CUDA_VISIBLE_DEVICES=%d\n'%gpuid)
    fout.write('cd ../driver\n')
    for best_epoch in best_epochs:
      cmd = [
        'python', python_file,
        model_cfg_file, path_cfg_file,
        '--best_epoch', str(best_epoch),
        '--is_train', '0',
      ]
      for key in kwargs:
        cmd += ['--' + key, str(kwargs[key])]
      fout.write(' '.join(cmd) + '\n')

  os.system('chmod 777 eval.sh')
  subprocess.call(['./eval.sh'])


'''expr
'''
def pred_eval_generation():
  # root_dir = '/data1/jiac/tgif' # uranus
  # root_dir = '/data1/jiac/MSCOCO' # uranus
  # root_dir = '/mnt/data1/jiac/tgif' # neptune
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  # root_dir = '/hdd/mscoco' # my-aws
  # root_dir = '/data1/jiac/mscoco' # mercurial
  # root_dir = '/jiac/data/mscoco' # gpu9
  # root_dir = '/qjin/data/MSCOCO' # sun
  gt_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')

  # model_name = 'twosides/qvevd_expr/i3d.512.512.0.5.16.lstm.lifted'
  # model_name = 'twosides/qvevd_expr/i3d.512.512.0.5.16.lstm'
  # python_file = 'qvevd.py'

  # # model_name = 'twosides/vevd_expr/tf_resnet101.512.512.lstm.mao'
  # model_name = 'twosides/vevd_expr/resnet200.512.512.lstm.mao'
  # python_file = 'vevd.py'

  # # model_name = 'twosides/qvevd_score_expr/i3d.512.512.0.5.16.0.5.lstm'
  # model_name = 'twosides/qvevd_score_expr/i3d.512.512.0.5.16.0.5.lstm.lifted'
  # python_file = 'qvevd_score.py'

  # # model_name = 'twosides/qvevd_score_sample_expr/i3d.512.512.0.25_0.25_0.25.16.1.0.5.lstm'
  # model_name = 'twosides/qvevd_score_sample_expr/i3d.512.512.0.25_0.25_0.25.16.1.0.5.1e-5.lstm'
  # python_file = 'qvevd_score_sample.py'

  # model_name = 'twosides/vevd_expr/i3d.512.512.lstm'
  # python_file = 'vevd.py'

  # model_name = 'twosides/self_critique/i3d.512.512.1.0.1.lstm'
  # python_file = 'self_critique.py'

  # # model_name = 'twosides/qvevd_score_sample_reward_expr/i3d.512.512.0.33_0.33.16.1.0.5.lstm'
  # model_name = 'twosides/qvevd_score_sample_reward_expr/i3d.512.512.0.1_0.9.16.1.0.5.lstm'
  # python_file = 'qvevd_score_sample_reward.py'

  # # # model_name = 'twosides/qvevd_reward_margin_expr/i3d.512.512.1.0.0.5.16.5.0.1.lstm'
  # # model_name = 'twosides/qvevd_reward_margin_expr/i3d.512.512.1.0.0.5.16.3.5.0.1.lstm'
  # # model_name = 'twosides/qvevd_reward_margin_expr/tf_resnet101.512.512.1.0.0.5.16.3.5.0.1.lstm'
  # # model_name = 'twosides/qvevd_reward_margin_expr/resnet200.512.512.1.0.0.5.16.3.5.0.1.lstm'
  # model_name = 'twosides/qvevd_reward_margin_expr/resnet200.512.512.1.0.0.5.16.3.5.0.1.lstm.sc'
  # python_file = 'qvevd_reward_margin.py'

  # model_name = 'low_resource/vevd_expr/tf_resnet101_450.512.512.lstm.1'
  # model_name = 'low_resource/vevd_expr/tf_resnet101_450.512.512.lstm.2'
  # model_name = 'low_resource/vevd_expr/tf_resnet101_450.512.512.lstm.3'
  # model_name = 'low_resource/vevd_expr/tf_resnet101_450.512.512.lstm.4'
  # model_name = 'low_resource/vevd_expr/tf_resnet101_450.512.512.lstm.4.half'
  # model_name = 'low_resource/vevd_expr/tf_resnet101_450.512.512.lstm'
  # python_file = 'vevd.py'

  # # model_name = 'low_resource/vevd_reward_margin_expr/tf_resnet101_450.512.512.1.0.0.5.16.3.5.0.1.lstm.sc.1'
  # model_name = 'low_resource/vevd_reward_margin_expr/tf_resnet101_450.512.512.1.0.0.5.16.3.5.0.1.lstm.sc.2'
  # # model_name = 'low_resource/vevd_reward_margin_expr/tf_resnet101_450.512.512.1.0.0.5.16.3.5.0.1.lstm.sc.3'
  # model_name = 'low_resource/vevd_reward_margin_expr/tf_resnet101_450.512.512.1.0.0.5.16.3.5.0.1.lstm.sc.4'
  # model_name = 'low_resource/vevd_reward_margin_expr/tf_resnet101_450.512.512.1.0.0.5.16.3.5.0.1.lstm'
  # python_file = 'qvevd_reward_margin.py'

  # model_name = 'low_resource/vevd_sc_expr/tf_resnet101_450.512.512.1.0.1.lstm.2'

  # model_name = 'low_resource/vevd_reward_margin_expr/tf_resnet101_450.512.512.1.0.0.5.1.1.1.0.1.lstm.1'
  # model_name = 'low_resource/vevd_reward_margin_expr/tf_resnet101_450.512.512.1.0.0.5.1.1.1.0.1.lstm.1'
  # python_file = 'qvevd_reward_margin_fast.py'

  # model_name = 'low_resource/vevd_gpc_expr/tf_resnet101_450.512.512.5.5.1.0.1.lstm.1'
  # model_name = 'low_resource/vevd_gpc_expr/tf_resnet101_450.512.512.5.5.1.0.0.lstm.1'
  # python_file = 'qvevd_gpc.py'

  # model_name = 'low_resource/vevd_gpc_sample_expr/tf_resnet101_450.512.512.5.5.1.0.0.lstm.1'
  # python_file = 'qvevd_gpc_sample.py'

  # model_name = 'low_resource/vevd_gpc_all_expr/tf_resnet101_450.512.512.0.1.4.5.1.lstm.1'
  # model_name = 'low_resource/vevd_gpc_all_expr/tf_resnet101_450.512.512.4.1.0.5.1.lstm.1'
  # model_name = 'low_resource/vevd_gpc_all_expr/tf_resnet101_450.512.512.2.1.2.5.1.lstm.1'
  # model_name = 'low_resource/vevd_gpc_all_expr/tf_resnet101_450.512.512.2.1.2.5.1.0.1.lstm.5'
  model_name = 'low_resource/vevd_gpc_all_expr/tf_resnet101_450.512.512.2.1.2.5.1.0.0.lstm.5'
  python_file = 'qvevd_gpc_all.py'

  logdir = os.path.join(root_dir, model_name, 'log')
  preddir = os.path.join(root_dir, model_name, 'pred')
  model_cfg_file = os.path.join(root_dir, model_name + '.model.json')
  path_cfg_file = os.path.join(root_dir, model_name + '.path.json')

  gpuid = 1

  best_epochs = select_generation(logdir, lower=0, upper=200)
  # best_epochs = [18, 19]
  print best_epochs
  # best_epochs = select_retrieval(logdir)
  # best_epochs = [166, 178]
  # best_epochs = [147, 176]
  # best_epochs = [149]
  # best_epochs = [146]

  with open('eval.%d.txt'%gpuid, 'w') as fout:
    predict(python_file, model_cfg_file, path_cfg_file, best_epochs, gpuid) 
    #   # lifted=1, tst_task='generation')
      # tst_task='generation')
      # tst_task='generation', strategy='sample', num_sample_in_tst=100, sample_topk=5)
      # tst_task='generation', strategy='sample', num_sample_in_tst=1000, sample_topk=10)

    for best_epoch in best_epochs:
      predict_file = os.path.join(preddir, 'epoch-%d.1.5.beam.json'%best_epoch)
      # predict_file = os.path.join(preddir, 'epoch-%d.1.5.sample.json'%best_epoch)

      out = eval_generation(predict_file, gt_file)
      content = '%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f'%(
        out['bleu'][0]*100, out['bleu'][1]*100, out['bleu'][2]*100, out['bleu'][3]*100,
        out['meteor']*100, out['rouge']*100, out['cider']*100)
      print best_epoch
      print content
      fout.write(str(best_epoch) + '\t' + content + '\n')


def pred_eval_retrieval():
  # root_dir = '/data1/jiac/tgif' # uranus
  root_dir = '/mnt/data1/jiac/tgif' # neptune
  # root_dir = '/mnt/data1/jiac/tgif' # mercurial
  # root_dir = '/data1/jiac/mscoco' # mercurial
  # root_dir = '/jiac/data/tgif'
  gt_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')

  # # model_name = 'twosides/qvevd_expr/i3d.512.512.0.5.16.lstm.lifted'
  # # model_name = 'twosides/qvevd_expr/i3d.512.512.0.5.16.lstm'
  # # model_name = 'twosides/qvevd_expr/i3d.512.512.1.0.16.lstm'
  # # model_name = 'twosides/qvevd_expr/tf_resnet101.512.512.1.0.16.lstm'
  # model_name = 'twosides/qvevd_expr/i3d.512.512.1.0.16.lstm.lifted'
  # python_file = 'qvevd.py'

  # model_name = 'twosides/qvevd_score_expr/i3d.512.512.0.5.16.0.5.lstm'
  # model_name = 'twosides/qvevd_score_expr/i3d.512.512.0.5.16.0.5.lstm.lifted'
  # model_name = 'twosides/qvevd_score_expr/i3d.512.512.1.0.16.0.5.lstm'
  # model_name = 'twosides/qvevd_score_expr/i3d.512.512.1.0.16.0.5.lstm.lifted'
  model_name = 'twosides/qvevd_score_expr/i3d.512.512.1.0.16.0.5.lstm.lifted.bleu'
  python_file = 'qvevd_score.py'

  # # model_name = 'twosides/qvevd_score_sample_expr/i3d.512.512.0.25_0.25_0.25.16.1.0.5.lstm'
  # model_name = 'twosides/qvevd_score_sample_expr/i3d.512.512.0.25_0.25_0.25.16.1.0.5.1e-5.lstm'
  # python_file = 'qvevd_score_sample.py'

  # # model_name = 'twosides/qvevd_score_sample_fix_expr/i3d.512.512.0.25_0.25_0.25.16.1.0.5.1e-5.100.lstm'
  # model_name = 'twosides/qvevd_score_sample_fix_expr/i3d.512.512.0.25_0.25_0.25.16.5.0.5.1e-5.100.lstm'
  # python_file = 'qvevd_score_sample_fix.py'

  # model_name = 'twosides/self_critique/i3d.512.512.1.0.1.lstm'
  # python_file = 'self_critique.py'

  # # model_name = 'twosides/qvevd_score_sample_reward_expr/i3d.512.512.0.33_0.33.16.1.0.5.lstm'
  # model_name = 'twosides/qvevd_score_sample_reward_expr/i3d.512.512.0.1_0.9.16.1.0.5.lstm'
  # python_file = 'qvevd_score_sample_reward.py'

  logdir = os.path.join(root_dir, model_name, 'log')
  preddir = os.path.join(root_dir, model_name, 'pred')
  model_cfg_file = os.path.join(root_dir, model_name + '.model.json')
  path_cfg_file = os.path.join(root_dir, model_name + '.path.json')

  gpuid = 3

  # best_epochs = select_generation(logdir)
  best_epochs = select_retrieval(logdir)
  # best_epochs = [184, 193]

  with open('eval.%d.txt'%gpuid, 'w') as fout:
    predict(python_file, model_cfg_file, path_cfg_file, best_epochs, gpuid,
      tst_task='retrieval')
    # predict(python_file, model_cfg_file, path_cfg_file, best_epochs, gpuid)

    for best_epoch in best_epochs:
      predict_file = os.path.join(preddir, 'epoch-%d.npy'%best_epoch)

      out = eval_retrieval(predict_file)
      content = '%.2f\t%.2f\t%.2f'%(out['r1'], out['r5'], out['r10'])
      print best_epoch
      print content
      fout.write(str(best_epoch) + '\t' + content + '\n')


def pred_eval_retrieval_mscoco():
  root_dir = '/data1/jiac/mscoco' # mercurial
  gt_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')
  split_file = os.path.join(root_dir, 'mao_split', 'tst_retrieve_id_caption_mask.pkl')

  # model_name = 'twosides/qvevd_score_expr/resnet200.512.512.1.0.16.0.5.lstm.lifted'
  model_name = 'twosides/qvevd_score_expr/resnet200.512.512.1.0.16.0.5.lstm.lifted.bleu'
  python_file = 'qvevd_score_mscoco.py'

  # model_name = 'twosides/qvevd_expr/resnet200.512.512.1.0.16.lstm.lifted'
  # python_file = 'qvevd_mscoco.py'

  logdir = os.path.join(root_dir, model_name, 'log')
  preddir = os.path.join(root_dir, model_name, 'pred')
  model_cfg_file = os.path.join(root_dir, model_name + '.model.json')
  path_cfg_file = os.path.join(root_dir, model_name + '.path.json')

  gpuid = 1

  best_epochs = select_retrieval(logdir)

  with open('eval.%d.txt'%gpuid, 'w') as fout:
    predict(python_file, model_cfg_file, path_cfg_file, best_epochs, gpuid,
      tst_task='retrieval')

    for best_epoch in best_epochs:
      predict_file = os.path.join(preddir, 'epoch-%d.npy'%best_epoch)

      out = eval_retrieval_mscoco(predict_file, split_file)
      content = '%.2f\t%.2f\t%.2f'%(out['r1'], out['r5'], out['r10'])
      print best_epoch
      print content
      fout.write(str(best_epoch) + '\t' + content + '\n')


def eval_cider_detail():
  root_dir = '/data1/jiac/tgif' # uranus
  gt_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')
  pred_file = os.path.join(root_dir, 'twosides/qvevd_expr/i3d.512.512.0.5.16.lstm.lifted', 'pred', 'epoch-176.1.5.sample.debug.json')

  with open(pred_file) as f:
    vid2captions = json.load(f)

  gts = {}
  with open(gt_file) as f:
    data = cPickle.load(f)
  for vid in vid2captions:
    gts[vid] = data[int(vid)]

  cider_scorer = CiderScorer()
  cider_scorer.init_refs(gts)

  select_vids = ['9252', '65502', '7263']
  for vid in select_vids:
    data = vid2captions[vid]
    captions = [d['caption'] for d in data]
    log_probs = [d['norm_log_prob'] for d in data]
    vids = [vid for _ in captions]
    _, scores = cider_scorer.compute_cider(captions, vids)

    log_prob_idx = np.argmax(log_probs)
    score_idx = np.argmax(scores)
    print vid
    print log_probs[log_prob_idx], scores[log_prob_idx], captions[log_prob_idx]
    print log_probs[score_idx], scores[score_idx], captions[score_idx]


if __name__ == '__main__':
  pred_eval_generation()
  # pred_eval_retrieval()
  # pred_eval_retrieval_mscoco()
  # eval_cider_detail()
