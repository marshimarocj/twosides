import os
import json


'''func
'''


'''expr
'''
def collect_cider_log():
  root_dir = '/data1/jiac/MSCOCO' # uranus
  # expr_name = os.path.join(root_dir, 'low_resource', 'vevd_sc_expr', 'tf_resnet101_450.512.512.1.0.1.lstm')
  expr_name = os.path.join(root_dir, 'low_resource', 'vevd_reward_margin_expr', 'tf_resnet101_450.512.512.1.0.0.5.16.3.5.0.1.lstm')
  log_dir = os.path.join(expr_name, 'log')
  out_file = os.path.join(log_dir, 'progress.json')

  epochs = []
  ciders = []
  for i in range(50):
    log_file = os.path.join(log_dir, 'val_metrics.%d.json'%i)
    if not os.path.exists(log_file):
      continue
    with open(log_file) as f:
      data = json.load(f)
      cider = data['cider']
    ciders.append(cider)
    epochs.append(i)

  out = zip(epochs, ciders)
  with open(out_file, 'w') as fout:
    json.dump(out, fout, indent=2)


if __name__ == '__main__':
  collect_cider_log()
