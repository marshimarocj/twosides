import os
import json
import cPickle

import numpy as np

from bleu.bleu import Bleu
from cider.cider import Cider
from rouge.rouge import Rouge
from meteor.meteor import Meteor


'''func
'''
def get_res_gts_dict(res_file, gts_file):
  human_caption = cPickle.load(file(gts_file))
  data = json.load(file(res_file))

  res, gts = {}, {}
  for key, value in data.iteritems():
    gts[key] = human_caption[int(key)]
    res[key] = [value]

  return res, gts


'''expr
'''
def eval_mscoco():
  root_dir = '/home/jiac/data/low_resource'
  gt_file = '/home/jiac/data/mscoco/aux/human_caption_dict.pkl'

  pred_file = os.path.join(root_dir, 'mscoco', 'xe_2', 'epoch-9.1.5.beam.json')
  out_file = os.path.join(root_dir, 'mscoco', 'xe_2', 'epoch-9.1.5.beam.detail.json')

  # pred_file = os.path.join(root_dir, 'mscoco', 'sc_2', 'epoch-46.1.5.beam.json')
  # out_file = os.path.join(root_dir, 'mscoco', 'sc_2', 'epoch-46.1.5.beam.detail.json')

  # pred_file = os.path.join(root_dir, 'mscoco', 'sc_4_half', 'epoch-43.1.5.beam.json')
  # out_file = os.path.join(root_dir, 'mscoco', 'sc_4_half', 'epoch-43.1.5.beam.detail.json')

  # pred_file = os.path.join(root_dir, 'mscoco', 'mpc_2', 'epoch-41.1.5.beam.json')
  # out_file = os.path.join(root_dir, 'mscoco', 'mpc_2', 'epoch-41.1.5.beam.detail.json')

  meteor_scorer = Meteor()
  cider_scorer = Cider()

  res, gts = get_res_gts_dict(pred_file, gt_file)

  # _, res_meteors = meteor_scorer.compute_score(gts, res)
  # meteor_scorer.meteor_p.kill()
  _, res_ciders = cider_scorer.compute_score(gts, res)

  accuracy = {}
  keys = gts.keys()
  for key, c in zip(keys, res_ciders):
    accuracy[key] = {
      # 'meteor': float(m),
      'cider': float(c),
    }

  with open(out_file, 'w') as fout:
    json.dump(accuracy, fout, indent=2)


def eval_tgif():
  root_dir = '/home/jiac/data/low_resource/tgif'
  gt_file = '/home/jiac/data/tgif/aux/human_caption_dict.pkl'

  # pred_file = os.path.join(root_dir, 'xe', 'epoch-146.1.5.beam.json')
  # out_file = os.path.join(root_dir, 'xe', 'epoch-146.1.5.beam.detail.json')

  # pred_file = os.path.join(root_dir, 'sc', 'epoch-184.1.5.json')
  # out_file = os.path.join(root_dir, 'sc', 'epoch-184.1.5.detail.json')

  pred_file = os.path.join(root_dir, 'mpc', 'epoch-179.1.5.beam.json')
  out_file = os.path.join(root_dir, 'mpc', 'epoch-179.1.5.beam.detail.json')

  cider_scorer = Cider()

  res, gts = get_res_gts_dict(pred_file, gt_file)

  _, res_ciders = cider_scorer.compute_score(gts, res)

  accuracy = {}
  keys = gts.keys()
  for key, c in zip(keys, res_ciders):
    accuracy[key] = {
      'cider': float(c),
    }

  with open(out_file, 'w') as fout:
    json.dump(accuracy, fout, indent=2)


def filter_mscoco():
  root_dir = '/home/jiac/data/low_resource/mscoco'
  name_file = '/home/jiac/data/mscoco/split/tst_names.npy'
  id_file = '/home/jiac/data/mscoco/split/tst_videoids.npy'
  gt_file = '/home/jiac/data/mscoco/aux/human_caption_dict.pkl'
  out_file = os.path.join(root_dir, 'case_study.json')

  expr_eval_files = [
    os.path.join(root_dir, 'xe_2', 'epoch-9.1.5.beam.detail.json'),
    os.path.join(root_dir, 'sc_2', 'epoch-46.1.5.beam.detail.json'),
    os.path.join(root_dir, 'sc_4_half', 'epoch-43.1.5.beam.detail.json'),
    os.path.join(root_dir, 'mpc_2', 'epoch-41.1.5.beam.detail.json'),
  ]
  expr_caption_files = [
    os.path.join(root_dir, 'xe_2', 'epoch-9.1.5.beam.json'),
    os.path.join(root_dir, 'sc_2', 'epoch-46.1.5.beam.json'),
    os.path.join(root_dir, 'sc_4_half', 'epoch-43.1.5.beam.json'),
    os.path.join(root_dir, 'mpc_2', 'epoch-41.1.5.beam.json'),
  ]

  names = np.load(name_file)
  ids = np.load(id_file)

  id2name = {}
  for id, name in zip(ids, names):
    id2name[id] = name

  id2scores = []
  for expr_eval_file in expr_eval_files:
    with open(expr_eval_file) as f:
      data = json.load(f)
    id2scores.append(data)

  id2captions = []
  for expr_caption_file in expr_caption_files:
    with open(expr_caption_file) as f:
      data = json.load(f)
    id2captions.append(data)

  with open(gt_file) as f:
    id2gt_captions = cPickle.load(f)

  id2diff = {}
  for id in id2scores[0]:
    max_score = 0.
    for i in range(3):
      score = id2scores[i][id]['cider']
      max_score = max(score, max_score)
    score = id2scores[3][id]['cider']
    diff = score - max_score
    id2diff[id] = diff

  id_diff = id2diff.items()
  id_diff = sorted(id_diff, key=lambda x: x[1], reverse=True)

  out = []
  for id, diff in id_diff:
    name = id2name[int(id)]
    results = {
      'xe_2': [id2scores[0][id], id2captions[0][id]], 
      'sc_2': [id2scores[1][id], id2captions[1][id]],
      'sc_4_half': [id2scores[2][id], id2captions[2][id]],
      'mpc_2': [id2scores[3][id], id2captions[3][id]],
      'gts': id2gt_captions[int(id)],
    }
    out.append({
      'name': name,
      'results': results,
    })

  with open(out_file, 'w') as fout:
    json.dump(out, fout, indent=2)


def filter_tgif():
  root_dir = '/home/jiac/data/low_resource/tgif'
  id2name_file = '/home/jiac/data/tgif/aux/int2video.npy'
  gt_file = '/home/jiac/data/tgif/aux/human_caption_dict.pkl'
  out_file = os.path.join(root_dir, 'case_study.json')

  expr_eval_files = [
    os.path.join(root_dir, 'xe', 'epoch-146.1.5.beam.detail.json'),
    os.path.join(root_dir, 'sc', 'epoch-184.1.5.detail.json'),
    os.path.join(root_dir, 'mpc', 'epoch-179.1.5.beam.detail.json'),
  ]
  expr_caption_files = [
    os.path.join(root_dir, 'xe', 'epoch-146.1.5.beam.json'),
    os.path.join(root_dir, 'sc', 'epoch-184.1.5.json'),
    os.path.join(root_dir, 'mpc', 'epoch-179.1.5.beam.json'),
  ]

  names = np.load(id2name_file)

  id2scores = []
  for expr_eval_file in expr_eval_files:
    with open(expr_eval_file) as f:
      data = json.load(f)
    id2scores.append(data)

  id2captions = []
  for expr_caption_file in expr_caption_files:
    with open(expr_caption_file) as f:
      data = json.load(f)
    id2captions.append(data)

  with open(gt_file) as f:
    id2gt_captions = cPickle.load(f)

  id2diff = {}
  for id in id2scores[0]:
    max_score = 0.
    for i in range(2):
      score = id2scores[i][id]['cider']
      max_score = max(score, max_score)
    score = id2scores[2][id]['cider']
    diff = score - max_score
    id2diff[id] = diff

  id_diff = id2diff.items()
  id_diff = sorted(id_diff, key=lambda x: x[1], reverse=True)

  out = []
  for id, diff in id_diff:
    name = names[int(id)]
    results = {
      'xe': [id2scores[0][id], id2captions[0][id]], 
      'sc': [id2scores[1][id], id2captions[1][id]],
      'mpc': [id2scores[2][id], id2captions[2][id]],
      'gts': id2gt_captions[int(id)],
    }
    out.append({
      'name': name,
      'results': results,
    })

  with open(out_file, 'w') as fout:
    json.dump(out, fout, indent=2)


if __name__ == '__main__':
  # eval_mscoco()
  # eval_tgif()
  # filter_mscoco()
  filter_tgif()
