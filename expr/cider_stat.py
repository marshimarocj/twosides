import os
import json
import cPickle
import random

import numpy as np
from cider.cider import Cider




'''func
'''


'''expr
'''
def eval_mean_std_sample():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  gt_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')
  pred_file = os.path.join(root_dir, 'vevd_expr', 'tf_resnet101.512.512.lstm', 'pred', 'epoch-33.5.-1.sample.tst.json')

  with open(gt_file) as f:
    human_caption = cPickle.load(f)
  with open(pred_file) as f:
    data = json.load(f)

  pred, gts = {}, {}
  for key in data:
    gts[key] = human_caption[int(key)]
    val = data[key]
    pred[key] = [val[0][1]]

  cider_scorer = Cider()

  _, scores = cider_scorer.compute_score(gts, pred)

  # print np.mean(scores), np.std(scores) # 44.6, 44.5

  # num_bin = 3
  # bins = [0] * num_bin
  # for score in scores:
  #   bin = int(score / (1./num_bin))
  #   bin = min(bin, num_bin-1)
  #   bins[bin] += 1
  # total = sum(bins)
  # for i in range(num_bin):
  #   bins[i] /= float(total)
  # print bins 
  # [0.4928, 0.2754, 0.2318]

  bounds = [.2, .7]
  bins = [0] * 3
  for score in scores:
    for i in range(2):
      if score < bounds[i]:
        bins[i] += 1
        break
    else:
      bins[-1] += 1
  total = sum(bins)
  for i in range(3):
    bins[i] /= float(total)
  print bins # [0.3552, 0.4766, 0.1682]


def eval_mean_std_beam():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  gt_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')
  pred_file = os.path.join(root_dir, 'vevd_expr', 'tf_resnet101.512.512.lstm', 'pred', 'epoch-33.1.json')

  with open(gt_file) as f:
    human_caption = cPickle.load(f)
  with open(pred_file) as f:
    data = json.load(f)

  pred, gts = {}, {}
  for key in data:
    gts[key] = human_caption[int(key)]
    val = data[key]
    pred[key] = [val]

  cider_scorer = Cider()

  _, scores = cider_scorer.compute_score(gts, pred)

  # print np.mean(scores), np.std(scores) # 91.5, 71.5

  # num_bin = 3
  # bins = [0] * num_bin
  # for score in scores:
  #   bin = int(score / (1./num_bin))
  #   bin = min(bin, num_bin-1)
  #   bins[bin] += 1
  # total = sum(bins)
  # for i in range(num_bin):
  #   bins[i] /= float(total)
  # print bins
  # # [0.2148, 0.2156, 0.5696]

  bounds = [.2, .7]
  bins = [0] * 3
  for score in scores:
    for i in range(2):
      if score < bounds[i]:
        bins[i] += 1
        break
    else:
      bins[-1] += 1
  total = sum(bins)
  for i in range(3):
    bins[i] /= float(total)
  print bins


def eval_mean_std_gt():
  root_dir = '/mnt/data1/jiac/mscoco' # neptune
  gt_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')
  pred_file = os.path.join(root_dir, 'vevd_expr', 'tf_resnet101.512.512.lstm', 'pred', 'epoch-33.1.json')

  with open(gt_file) as f:
    human_caption = cPickle.load(f)
  with open(pred_file) as f:
    data = json.load(f)
  keys = data.keys()

  pred = human_caption.values()
  random.shuffle(pred)

  preds, gts = {}, {}
  for i, key in enumerate(keys):
    gts[key] = human_caption[int(key)]
    preds[key] = [pred[i][0]]

  cider_scorer = Cider()

  _, scores = cider_scorer.compute_score(gts, preds)

  # num_bin = 3
  # bins = [0] * num_bin
  # for score in scores:
  #   bin = int(score / (1./num_bin))
  #   bin = min(bin, num_bin-1)
  #   bins[bin] += 1
  # total = sum(bins)
  # for i in range(num_bin):
  #   bins[i] /= float(total)
  # print bins # [0.988, 0.0092, 0.0028]

  bounds = [.2, .7]
  bins = [0] * 3
  for score in scores:
    for i in range(2):
      if score < bounds[i]:
        bins[i] += 1
        break
    else:
      bins[-1] += 1
  total = sum(bins)
  for i in range(3):
    bins[i] /= float(total)
  print bins


if __name__ == '__main__':
  eval_mean_std_sample()
  eval_mean_std_beam()
  eval_mean_std_gt()
