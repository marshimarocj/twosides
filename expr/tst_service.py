import json
import base64
import requests
import ast
import cPickle
import time
import sys
import os
sys.path.append('../')

import numpy as np

import framework.util.caption.utility
import model.base
import model.qvevd_reward_margin_fast
import driver.qvevd_reward_margin_fast


'''func
'''


'''expr
'''
def profile_eval():
  root_dir = '/jiac/hdd/mscoco' # aws
  pred_file = os.path.join(root_dir, 'diversity_expr/tf_resnet101.512.512.1.0.0.2.5.2_4.lstm/pred/epoch-21.1.5.beam.json')
  gt_file = os.path.join(root_dir, 'aux', 'human_caption_dict.pkl')

  with open(pred_file) as f:
    data = json.load(f)
  vid2prediction = {}
  for key in data:
    vid2prediction[int(key)] = data[key]

  with open(gt_file) as f:
    vid2gt = cPickle.load(f)

  eval_data = []
  hyp_map = {}
  ref_map = {}
  idx = 0
  for vid in vid2prediction:
    pred = vid2prediction[vid]
    gt = vid2gt[vid]
    eval_data.append({
      'pred': pred,
      'gt': gt,
      'vid': vid,
      'id': idx
    })
    hyp_map[vid] = [pred]
    ref_map[vid] = [gt]
    idx += 1
    if idx == 1000:
      break

  start = time.time()
  server_url = 'http://aladdin1.inf.cs.cmu.edu:8888/bleu'
  r = requests.post(server_url, json=eval_data)
  data = json.loads(r.text)
  end = time.time()
  print 'bleu:', end-start

  start = time.time()
  server_url = 'http://18.216.242.151:8080/meteor_server/EvalMeteor'
  final_eval = {'hyp_map': hyp_map, 'ref_map': ref_map}
  json_str = json.dumps(final_eval)
  base64_encode = base64.b64encode(json_str)
  base64_ret = requests.post(server_url, {'eval_data': base64_encode}).text
  # print base64_ret
  ret_json_str = base64.b64decode(base64_ret)
  ret_dict = ast.literal_eval(ret_json_str)
  end = time.time()
  print 'meteor:', end-start

  start = time.time()
  server_url = 'http://aladdin1.inf.cs.cmu.edu:8888/rouge'
  r = requests.post(server_url, json=eval_data)
  data = json.loads(r.text)
  end = time.time()
  print 'rouge:', end-start

  start = time.time()
  server_url = 'http://aladdin1.inf.cs.cmu.edu:8888/cider'
  r = requests.post(server_url, json=eval_data)
  data = json.loads(r.text)
  end = time.time()
  print 'cider', end-start


def tst_bcmr_eval():
  word_file = '/jiac/hdd/mscoco/aux/int2word.pkl' # aws

  with open(word_file) as f:
    words = cPickle.load(f)
  word2wid = {}
  for i, word in enumerate(words):
    word2wid[word] = i

  hyps = [
    ["this is a dog"],
    ["this is a cat"],
    ["does cat eat dog"]
  ]

  out_wids = np.ones((3, 10), dtype=np.int32)
  for i in range(3):
    caption = hyps[i][0].split(' ')
    out_wids[i][0] = 0
    for j, word in enumerate(caption):
      if word in word2wid:
        wid = word2wid[word]
      else:
        wid = 2
      out_wids[i][j+1] = wid
  out_wids = np.expand_dims(out_wids, 0)
  vids = [0]

  refs = ["this is a white dog","there is a white and black dog over there","i do not like dogs"]
  vid2gt_captions = {}
  vid2gt_captions[0] = refs

  int2str = framework.util.caption.utility.CaptionInt2str(word_file)

  out_scores = model.base.eval_BCMR_in_rollout_fast(out_wids, vids, int2str, vid2gt_captions)

  print out_scores


def tst_trn_reader():
  root_dir = '/jiac/hdd/mscoco' # aws
  path_cfg_file = os.path.join(root_dir, 'twosides', 'qvevd_reward_margin_expr', 'tf_resnet101.512.512.1.0.0.5.16.3.5.0.1.lstm.bcmr.path.json')

  path_cfg = driver.qvevd_reward_margin_fast.gen_dir_struct_info(path_cfg_file)

  trn_reader = model.qvevd_reward_margin_fast.TrnReader(
    path_cfg.trn_ftfiles, path_cfg.trn_videoid_file, path_cfg.quotient_file, 
    path_cfg.trn_annotation_file, path_cfg.groundtruth_file, path_cfg.word_file, 
    16, metric='bcmr')
  start = time.time()
  for data in trn_reader.yield_trn_batch(64):
    end = time.time()
    print end - start
    start = end


if __name__ == '__main__':
  # profile_eval()
  # tst_bcmr_eval()
  tst_trn_reader()
