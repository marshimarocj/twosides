import argparse
import sys
import os
import datetime
import cPickle
sys.path.append('../')

import numpy as np

import model.qvevd_score_sample_fix
import base


def build_parser():
  parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

  parser.add_argument('model_cfg_file')
  parser.add_argument('path_cfg_file')
  parser.add_argument('--is_train', dest='is_train', type=int, default=True)
  parser.add_argument('--memory_fraction', dest='memory_fraction', type=float, default=1.0)
  # only in tst
  parser.add_argument('--best_epoch', dest='best_epoch', type=int, default=True)

  return parser


def gen_dir_struct_info(path_cfg_file):
  path_cfg = model.qvevd_score_sample_fix.PathCfg()
  return base.gen_dir_struct_info(path_cfg, path_cfg_file)


def load_and_fill_model_cfg(model_cfg_file, path_cfg):
  model_cfg = model.qvevd_score_sample_fix.ModelConfig()
  model_cfg.load(model_cfg_file)

  # auto fill params
  words = cPickle.load(open(path_cfg.word_file))
  model_cfg.subcfgs[model.qvevd_score_sample_fix.VD].num_words = len(words)

  return model_cfg


if __name__ == '__main__':
  parser = build_parser()
  opts = parser.parse_args()

  path_cfg = gen_dir_struct_info(opts.path_cfg_file)
  model_cfg = load_and_fill_model_cfg(opts.model_cfg_file, path_cfg)

  if opts.is_train:
    m = model.qvevd_score_sample_fix.Model(model_cfg)

    with open(os.path.join(path_cfg.log_dir, 'cfg.pkl'), 'w') as fout:
      cPickle.dump(model_cfg, fout)
      cPickle.dump(path_cfg, fout)
      cPickle.dump(opts, fout)

    trntst = model.qvevd_score_sample_fix.TrnTst(model_cfg, path_cfg, m)

    trn_reader = model.qvevd_score_sample_fix.TrnReader(
      path_cfg.trn_ftfiles, path_cfg.trn_videoid_file, path_cfg.quotient_file, 
      path_cfg.trn_annotation_file, path_cfg.groundtruth_file, path_cfg.trn_sample_file, path_cfg.word_file, model_cfg)
    val_reader = model.qvevd_score_sample_fix.ValReader(
      path_cfg.val_ftfiles, path_cfg.val_videoid_file,
      path_cfg.val_retrieve_file, path_cfg.groundtruth_file)
    if path_cfg.model_file != '':
      trntst.train(trn_reader, val_reader, memory_fraction=opts.memory_fraction, resume=True)
    else:
      trntst.train(trn_reader, val_reader, memory_fraction=opts.memory_fraction)
  else:
    path_cfg.model_file = os.path.join(path_cfg.model_dir, 'epoch-%d'%opts.best_epoch)
    path_cfg.log_file = None

    m = model.qvevd_score_sample_fix.Model(model_cfg)

    path_cfg.predict_file = os.path.join(path_cfg.output_dir, 'pred', 'epoch-%d.npy'%opts.best_epoch)
    trntst = model.qvevd_score_sample_fix.TrnTst(model_cfg, path_cfg, m)
    tst_reader = model.qvevd_score_sample_fix.TstRetrievalReader(
      path_cfg.tst_ftfiles, path_cfg.tst_retrieve_file)
    trntst.test(tst_reader, memory_fraction=opts.memory_fraction)
