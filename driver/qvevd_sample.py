import argparse
import sys
import os
import datetime
import cPickle
sys.path.append('../')

import numpy as np

import model.qvevd
import base


def build_parser():
  parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)

  parser.add_argument('model_cfg_file')
  parser.add_argument('path_cfg_file')
  parser.add_argument('--memory_fraction', dest='memory_fraction', type=float, default=1.0)
  # only in tst
  parser.add_argument('--best_epoch', dest='best_epoch', type=int, default=True)
  parser.add_argument('--num_sample_in_tst', dest='num_sample_in_tst', type=int, default=100)
  parser.add_argument('--sample_topk', dest='sample_topk', type=int, default=10)
  parser.add_argument('--split', dest='split', default='tst')

  return parser


def gen_dir_struct_info(path_cfg_file):
  path_cfg = model.qvevd.PathCfg()
  return base.gen_dir_struct_info(path_cfg, path_cfg_file)


def load_and_fill_model_cfg(model_cfg_file, path_cfg):
  model_cfg = model.qvevd.ModelConfig()
  model_cfg.load(model_cfg_file)

  # auto fill params
  words = cPickle.load(open(path_cfg.word_file))
  model_cfg.subcfgs[model.qvevd.VD].num_words = len(words)

  return model_cfg


if __name__ == '__main__':
  parser = build_parser()
  opts = parser.parse_args()

  path_cfg = gen_dir_struct_info(opts.path_cfg_file)
  model_cfg = load_and_fill_model_cfg(opts.model_cfg_file, path_cfg)

  path_cfg.model_file = os.path.join(path_cfg.model_dir, 'epoch-%d'%opts.best_epoch)
  path_cfg.log_file = None
  model_cfg.tst_task = 'generation'
  model_cfg.strategy = 'sample'
  model_cfg.num_sample_in_tst = opts.num_sample_in_tst
  model_cfg.sample_topk = opts.sample_topk

  m = model.qvevd.Model(model_cfg)

  path_cfg.predict_file = os.path.join(path_cfg.output_dir, 'pred', 
    'epoch-%d.%d.%d.sample.%s.json'%(opts.best_epoch, opts.num_sample_in_tst, opts.sample_topk, opts.split))
  if opts.split == 'trn':
    path_cfg.tst_ftfiles = path_cfg.trn_ftfiles
    path_cfg.tst_videoid_file = path_cfg.trn_videoid_file
  trntst = model.qvevd.SampleTst(model_cfg, path_cfg, m)
  tst_reader = model.qvevd.TstGenerationReader(
    path_cfg.tst_ftfiles, path_cfg.tst_videoid_file)
  trntst.test(tst_reader, memory_fraction=opts.memory_fraction)
