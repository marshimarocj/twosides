import json
import base64
# import requests
import ast
import threading
# from Queue import Queue

import numpy as np
import tensorflow as tf

from bleu.bleu import Bleu
from cider.cider import Cider
from rouge.rouge import Rouge

import framework.model.trntst
import framework.util.caption.utility
from framework.model.module import Mode

VD = 'decoder'

def tst_generation_ops(model, ft_embed, decoder):
  if model._config.strategy == 'beam':
    batch_size = tf.shape(ft_embed)[0]
    init_wid = tf.zeros((batch_size,), dtype=tf.int32)

    vd_inputs = {
      decoder.InKey.FT: ft_embed,
      decoder.InKey.INIT_WID: init_wid,
    }
    out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.TST, 
      task='generation', strategy=model._config.strategy)
    return {
      model.OutKey.OUT_WID: out_ops[decoder.OutKey.OUT_WID],
      model.OutKey.BEAM_CUM_LOG_PROB: out_ops[decoder.OutKey.BEAM_CUM_LOG_PROB],
      model.OutKey.BEAM_PRE: out_ops[decoder.OutKey.BEAM_PRE],
      model.OutKey.BEAM_END: out_ops[decoder.OutKey.BEAM_END],
    }
  elif model._config.strategy == 'sample':
    batch_size = tf.shape(ft_embed)[0]
    init_wid = tf.zeros((batch_size,), dtype=tf.int32)

    vd_inputs = {
      decoder.InKey.FT: ft_embed,
      decoder.InKey.INIT_WID: init_wid,
    }
    out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.TST, 
      task='generation', strategy=model._config.strategy, 
      topk=model._config.sample_topk, num_sample=model._config.num_sample_in_tst)
    out_wids = out_ops[decoder.OutKey.OUT_WID]
    return {
      model.OutKey.OUT_WID: out_ops[decoder.OutKey.OUT_WID], # (None, num_sample, num_step)
      model.OutKey.LOG_PROB: out_ops[decoder.OutKey.LOG_PROB], # (None, num_sample, num_step)
    }


def tst_retrieval_ops(model, ft_embed, captionid, caption_mask, decoder):
  batch_size = tf.shape(ft_embed)[0]
  init_wid = tf.zeros((batch_size,), dtype=tf.int32)

  vd_inputs = {
    decoder.InKey.FT: ft_embed,
    decoder.InKey.CAPTIONID: captionid,
    decoder.InKey.INIT_WID: init_wid,
  }
  decoder.is_trn = False
  out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.TST, task='retrieval')

  log_prob = out_ops[decoder.OutKey.LOG_PROB]
  norm_log_prob = tf.reduce_sum(log_prob*caption_mask[:, 1:], axis=1) / \
    tf.reduce_sum(caption_mask[:, 1:], axis=1) # (None,)
  return {
    model.OutKey.LOG_PROB: norm_log_prob
  }


class TrnTst(framework.model.trntst.TrnTst):
  def __init__(self, model_cfg, path_cfg, model, gen_sent_mode=1):
    framework.model.trntst.TrnTst.__init__(self, model_cfg, path_cfg, model)

    # caption int to string
    self.int2str = framework.util.caption.utility.CaptionInt2str(path_cfg.word_file)

    self.gen_sent_mode = gen_sent_mode

  def feed_data_and_run_loss_op_in_val(self, data, sess):
    pass


def output_by_sent_mode(sent_pool, videoid, videoid2caption,
    sent_pool_size, gen_sent_mode, int2str):
  if gen_sent_mode == 1:
    if len(sent_pool) == 0:
      videoid2caption[videoid] = ''
    else:
      captionid = np.expand_dims(sent_pool[0][1], 0)
      videoid2caption[videoid] = int2str(captionid)[0]
  elif gen_sent_mode == 2:
    videoid2caption[videoid] = []
    for k in xrange(sent_pool_size):
      captionid = np.expand_dims(sent_pool[k][1], 0)
      out = (
        float(sent_pool[k][0]),
        int2str(captionid)[0],
      )
      videoid2caption[videoid].append(out)


def predict_generation_task(trntst, sess, tst_reader, predict_file, 
    search_strategy, sent_pool_size):
  videoid2caption = {}
  base = 0
  op_dict = trntst.model.op_in_tst()
  for data in tst_reader.yield_tst_batch(trntst.model_cfg.tst_batch_size):
    feed_dict = {
      trntst.model.inputs[trntst.model.InKey.FT]: data['fts'],
      trntst.model.inputs[trntst.model.InKey.IS_TRN]: False,
    }
    if search_strategy == 'greedy':
      sent_pool = sess.run(
        op_dict[trntst.model.OutKey.OUT_WID], feed_dict=feed_dict)
      sent_pool = np.array(sent_pool)

      for k, sent in enumerate(sent_pool):
        videoid = tst_reader.videoids[base + k]
        videoid2caption[videoid] = trntst.int2str(np.expand_dims(sent, 0))[0]

      base += sent_pool.shape[0]
    elif search_strategy == 'beam':
      wordids, cum_log_probs, pres, ends = sess.run(
        [
          op_dict[trntst.model.OutKey.OUT_WID],
          op_dict[trntst.model.OutKey.BEAM_CUM_LOG_PROB],
          op_dict[trntst.model.OutKey.BEAM_PRE],
          op_dict[trntst.model.OutKey.BEAM_END],
        ], feed_dict=feed_dict)
      sent_pool = framework.util.caption.utility.beamsearch_recover_captions(
        wordids, cum_log_probs, pres, ends, sent_pool_size)

      for b in xrange(len(sent_pool)):
        videoid = str(tst_reader.videoids[b+base])
        output_by_sent_mode(sent_pool[b], videoid, videoid2caption,
          trntst.model_cfg.subcfgs[VD].sent_pool_size, trntst.gen_sent_mode, trntst.int2str)

      base += len(sent_pool)
    elif search_strategy == 'sample':
      out_wids, log_probs = sess.run(
        [op_dict[trntst.model.OutKey.OUT_WID], op_dict[trntst.model.OutKey.LOG_PROB]],
        feed_dict=feed_dict)
      captionids, caption_masks = gen_captionid_masks_from_wids(out_wids)
      caption_masks = caption_masks[:, :, 1:]
      norm_log_prob = np.sum(log_probs * caption_masks, axis=-1) / np.sum(caption_masks, axis=-1) # (None, num_sample)
      idxs = np.argmax(norm_log_prob, axis=1)

      for i in range(captionids.shape[0]):
        videoid = str(tst_reader.videoids[i+base])
        caption = trntst.int2str(captionids[i][idxs[i]:idxs[i]+1])[0]
        videoid2caption[videoid] = caption

      base += captionids.shape[0]

  json.dump(videoid2caption, open(predict_file, 'w'))


def predict_retrieval_task(trntst, sess, tst_reader, predict_file, 
    topk):
  op_dict = trntst.model.op_in_tst()
  batch_size = trntst.model_cfg.tst_batch_size
  num_video = tst_reader.fts.shape[0]
  num_caption = tst_reader.captionids.shape[0]

  topk_idxs = []
  idx = 0
  for data in tst_reader.yield_tst_batch(batch_size):
    ft = data['ft']
    captionids = data['captionids']
    caption_masks = data['caption_masks']

    num = captionids.shape[0]
    log_probs = []
    fts = np.repeat(np.expand_dims(ft, 0), batch_size, axis=0)
    for i in range(0, num, batch_size):
      start = i
      end = min(i+batch_size, num)

      _fts = fts[:end-start]
      _captionids = captionids[start:end]
      _caption_masks = caption_masks[start:end]
      feed_dict = {
        trntst.model.inputs[trntst.model.InKey.FT]: _fts,
        trntst.model.inputs[trntst.model.InKey.CAPTIONID]: _captionids,
        trntst.model.inputs[trntst.model.InKey.CAPTION_MASK]: _caption_masks,
        trntst.model.inputs[trntst.model.InKey.IS_TRN]: False,
      }
      log_prob = sess.run(
        op_dict[trntst.model.OutKey.LOG_PROB], feed_dict=feed_dict)
      log_probs.append(log_prob)
    log_probs = np.concatenate(log_probs, axis=0)

    sort_idxs = np.argsort(-log_probs)
    topk_idxs.append(sort_idxs[:topk])
    idx += 1
  np.save(predict_file, topk_idxs)


def val_generation_task(trntst, sess, tst_reader, metrics):
  videoid2caption = {}
  base = 0
  op_dict = trntst.model.op_in_val(task='generation')

  batch_size = trntst.model_cfg.tst_batch_size
  for data in tst_reader.yield_tst_batch(batch_size, task='generation'):
    feed_dict = {
      trntst.model.inputs[trntst.model.InKey.FT]: data['fts'],
      trntst.model.inputs[trntst.model.InKey.IS_TRN]: False,
    }
    sent_pool = sess.run(
      op_dict[trntst.model.OutKey.OUT_WID], feed_dict=feed_dict)
    sent_pool = np.array(sent_pool)

    for k, sent in enumerate(sent_pool):
      videoid = tst_reader.videoids[base + k]
      videoid2caption[videoid] = trntst.int2str(np.expand_dims(sent, 0))
    base += batch_size

  bleu_scorer = Bleu(4)
  bleu_score, _ = bleu_scorer.compute_score(tst_reader.videoid2captions, videoid2caption)
  for i in range(4):
    metrics['bleu%d'%(i+1)] = bleu_score[i]

  cider_scorer = Cider()
  cider_score, _ = cider_scorer.compute_score(tst_reader.videoid2captions, videoid2caption)
  metrics['cider'] = cider_score


def val_generation_task_sample(trntst, sess, tst_reader, metrics):
  videoid2caption = {}
  base = 0
  op_dict = trntst.model.op_in_val(task='generation')

  batch_size = trntst.model_cfg.tst_batch_size
  for data in tst_reader.yield_tst_batch(batch_size, task='generation'):
    feed_dict = {
      trntst.model.inputs[trntst.model.InKey.FT]: data['fts'],
      trntst.model.inputs[trntst.model.InKey.IS_TRN]: False,
    }
    out_wids, log_probs = sess.run(
      [op_dict[trntst.model.OutKey.ROLL_OUT_WID], op_dict[trntst.model.OutKey.RLOG_PROB]], 
      feed_dict=feed_dict)
    captionids, caption_masks = gen_captionid_masks_from_wids(out_wids)
    norm_log_prob = np.sum(log_probs * caption_masks, axis=2) / np.sum(caption_masks, axis=2) # (None, num_sample)
    idxs = np.argmax(norm_log_prob, axis=1)

    for i in range(captionids.shape[0]):
      videoid = tst_reader.videoids[base + i]
      videoid2caption[videoid] = trntst.int2str(captionids[i][idxs[i]:idxs[i]+1])
    base += captionids.shape[0]

  bleu_scorer = Bleu(4)
  bleu_score, _ = bleu_scorer.compute_score(tst_reader.videoid2captions, videoid2caption)
  for i in range(4):
    metrics['sample_bleu%d'%(i+1)] = bleu_score[i]

  cider_scorer = Cider()
  cider_score, _ = cider_scorer.compute_score(tst_reader.videoid2captions, videoid2caption)
  metrics['sample_cider'] = cider_score


def val_retrieval_task(trntst, sess, tst_reader, metrics):
  op_dict = trntst.model.op_in_val(task='retrieval')

  batch_size = trntst.model_cfg.tst_batch_size
  idx = 0
  r1, r5, r10 = 0., 0., 0.
  for data in tst_reader.yield_tst_batch(batch_size, task='retrieval'):
    ft = data['ft']
    captionids = data['captionids']
    caption_masks = data['caption_masks']

    num = captionids.shape[0]
    log_probs = []
    fts = np.repeat(np.expand_dims(ft, 0), batch_size, axis=0)
    for i in range(0, num, batch_size):
      start = i
      end = min(i+batch_size, num)

      _fts = fts[:end-start]
      _captionids = captionids[start:end]
      _caption_masks = caption_masks[start:end]
      feed_dict = {
        trntst.model.inputs[trntst.model.InKey.FT]: _fts,
        trntst.model.inputs[trntst.model.InKey.CAPTIONID]: _captionids,
        trntst.model.inputs[trntst.model.InKey.CAPTION_MASK]: _caption_masks,
        trntst.model.inputs[trntst.model.InKey.IS_TRN]: False,
      }
      log_prob = sess.run(
        op_dict[trntst.model.OutKey.LOG_PROB], feed_dict=feed_dict)
      log_probs.append(log_prob)
    log_probs = np.concatenate(log_probs, axis=0)

    sort_idxs = np.argsort(-log_probs)
    r1 += np.sum(sort_idxs[:1] == idx)
    r5 += np.sum(sort_idxs[:5] == idx)
    r10 += np.sum(sort_idxs[:10] == idx)

    idx += 1
  r1 /= idx
  r5 /= idx
  r10 /= idx
  metrics['r1'] = r1
  metrics['r5'] = r5
  metrics['r10'] = r10


def gen_captionid_masks_from_wids(out_wids):
  batch_size, num, num_step = out_wids.shape
  bos = np.zeros((batch_size, num, 1), dtype=np.int32)
  caption_ids = np.concatenate([bos, out_wids], axis=2)
  caption_masks = np.zeros(caption_ids.shape, dtype=np.float32)
  caption_masks[:, :, 0] = 1.
  for i in range(1, num_step+1):
    caption_masks[:, :, i] = caption_masks[:, :, i-1] * (caption_ids[:, :, i-1] != 1)

  return caption_ids, caption_masks


def eval_cider_in_rollout(out_wids, vids, int2str, cider):
  batch_size, num, num_step = out_wids.shape
  out_scores = []
  for i in range(batch_size):
    pred_captions = []
    for j in range(num):
      pred_caption = int2str(np.expand_dims(out_wids[i, j], 0))[0]
      pred_captions.append(pred_caption)
    _vids = [vids[i]]*num
    score, scores = cider.compute_cider(pred_captions, _vids)
    out_scores.append(scores)

  out_scores = np.array(out_scores, dtype=np.float32)
  return out_scores


def eval_BCMR_in_rollout(out_wids, vids, int2str, cider, vid2gt_captions):
  batch_size, num, num_step = out_wids.shape
  out_scores = []
  bleu_scorer = Bleu(2)
  rouge_scorer = Rouge()

  hyp_maps = []
  for j in range(num):
    hyp_maps.append({})
  for i in range(batch_size):
    vid = vids[i]
    pred_captions = []
    refs = {vid: vid2gt_captions[vid]}

    bleu_scores = []
    rouge_scores = []
    for j in range(num):
      pred_caption = int2str(np.expand_dims(out_wids[i, j], 0))[0]
      pred_captions.append(pred_caption)
      pred = {vid: [pred_caption]}

      bleu_score, _ = bleu_scorer.compute_score(refs, pred)
      rouge_score, _ = rouge_scorer.compute_score(refs, pred)

      hyp_maps[j]['%d_%d'%(vid, j)] = [pred_caption]

      bleu_scores.append(bleu_score)
      rouge_scores.append(rouge_score)
    _vids = [vids[i]]*num
    _, cider_scores = cider.compute_cider(pred_captions, _vids)

    out_score = []
    for j in range(num):
      bcmr_score = 0.5*bleu_scores[j][0] + 0.5*bleu_scores[j][1] + \
        2.0 * rouge_scores[j] + 1.0 * cider_scores[j]
      out_score.append(bcmr_score)
    out_scores.append(out_score)
  out_scores = np.array(out_scores, dtype=np.float32)

  q = Queue()
  vid2idx = {}
  for j in range(num):
    ref_map = {}
    for i, vid in enumerate(vids):
      ref_map['%d_%d'%(vid, j)] = [vid2gt_captions[vid]]
      vid2idx[vid] = i
    worker = threading.Thread(target=meteor_scorer, args=(hyp_maps[j], ref_map, q))
    worker.start()
  for _ in range(num):
    data = q.get()
    for key in data:
     val = data[key][0]
     fields = key.split('_')
     vid = int(fields[0])
     j = int(fields[1])
     out_scores[vid2idx[vid], j] += 5.0*val

  return out_scores


def eval_BCMR_in_rollout_fast(out_wids, vids, int2str, vid2gt_captions, chunk_size=200):
  def parse_id(id):
    fields = id.split('_')
    i = int(fields[0])
    j = int(fields[1])
    return i, j

  batch_size, num, num_step = out_wids.shape

  eval_data = []
  hyp_map = {}
  ref_map = {}
  for i in range(batch_size):
    vid = vids[i]

    for j in range(num):
      pred_caption = int2str(np.expand_dims(out_wids[i, j], 0))[0]

      key = '%d_%d'%(i, j)
      hyp_map[key] = [pred_caption]
      ref_map[key] = [vid2gt_captions[vid]]
      eval_data.append({
        'pred': pred_caption, 
        'gt': vid2gt_captions[vid], 
        'vid': vid,
        'id': key,
      })

  q = Queue()
  num_thread = 0
  total = len(eval_data)

  for i in range(0, total, chunk_size):
    worker = threading.Thread(
      target=bleu_scorer, args=(eval_data[i*chunk_size:(i+1)*chunk_size], q))
    worker.start()

    worker = threading.Thread(
      target=rouge_scorer, args=(eval_data[i*chunk_size:(i+1)*chunk_size], q))
    worker.start()

    worker = threading.Thread(target=cider_scorer, args=(eval_data[i*chunk_size:(i+1)*chunk_size], q))
    worker.start()

    num_thread += 3

  hyp_items = hyp_map.items()
  ref_items = ref_map.items()
  for i in range(0, total, chunk_size):
    _hyp_map = dict(hyp_items[i*chunk_size:(i+1)*chunk_size])
    _ref_map = dict(ref_items[i*chunk_size:(i+1)*chunk_size])
    worker = threading.Thread(
      target=meteor_scorer, args=(_hyp_map, _ref_map, q))
    worker.start()
    num_thread += 1

  out_scores = np.zeros((batch_size, num), dtype=np.float32)
  for _ in range(num_thread):
    data = q.get()
    service = data['service']
    data = data['data']
    if service == 'bleu':
      for d in data:
        id = d['id']
        score = d['score']
        i, j = parse_id(id)
        out_scores[i, j] += 0.5 * score[0] + 0.5 * score[1] + score[2] + score[3]
    elif service == 'meteor':
      for key in data:
        val = data[key][0]
        i, j = parse_id(key)
        out_scores[i, j] += 5.0 * val
    elif service == 'cider':
      for d in data:
        id = d['id']
        score = d['score']
        i, j = parse_id(id)
        out_scores[i, j] += score
    elif service == 'rouge':
      for d in data:
        id = d['id']
        score = d['score']
        i, j = parse_id(id)
        out_scores[i, j] += 2.0 * score

  return out_scores


def meteor_scorer(hyp_map, ref_map, q):
  server_url = 'http://18.216.242.151:8080/meteor_server/EvalMeteor'

  final_eval = {'hyp_map': hyp_map, 'ref_map': ref_map}
  json_str = json.dumps(final_eval)
  base64_encode = base64.b64encode(json_str)

  base64_ret = requests.post(server_url, {'eval_data': base64_encode}).text

  ret_json_str = base64.b64decode(base64_ret)
  ret_dict = ast.literal_eval(ret_json_str)

  q.put({
    'service': 'meteor',
    'data': ret_dict,
  })


def bleu_scorer(data, q):
  server_url = 'http://aladdin1.inf.cs.cmu.edu:8888/bleu'

  r = requests.post(server_url, json=data)
  data = json.loads(r.text)
  q.put(data)


def rouge_scorer(data, q):
  server_url = 'http://aladdin1.inf.cs.cmu.edu:8888/rouge'

  r = requests.post(server_url, json=data)
  data = json.loads(r.text)
  q.put(data)


def cider_scorer(data, q):
  server_url = 'http://aladdin1.inf.cs.cmu.edu:8888/cider'

  r = requests.post(server_url, json=data)
  data = json.loads(r.text)
  q.put(data)
