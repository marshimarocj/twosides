import enum
import sys
import os
import cPickle
import json
import base64
import requests
import ast
import threading
from Queue import Queue
import random
sys.path.append('../')

import numpy as np
import tensorflow as tf

# from bleu import bleu
# from cider import cider
from bleu.bleu import Bleu
from cider.cider import Cider
from rouge.rouge import Rouge

import framework.model.module
import framework.model.trntst
import framework.model.data
from framework.model.module import Mode
import framework.util.caption.utility
import base
import encoder.dnn
import decoder.rnn
import qvevd_score_sample
import fast_cider

VE = 'encoder'
VD = 'decoder'
CELL = decoder.rnn.CELL


class ModelConfig(framework.model.module.ModelConfig):
  def __init__(self):
    framework.model.module.ModelConfig.__init__(self)

    self.subcfgs[VE] = encoder.dnn.Config()
    self.subcfgs[VD] = decoder.rnn.Config()
    self.alpha = 1.
    self.reward_alpha = .5
    self.margin = .1
    self.num_neg = 16
    self.num_sample = 1
    self.num_hard_neg = 3

    self.tst_task = 'generation'
    self.topk = 10
    self.strategy = 'beam'
    self.sample_topk = -1
    self.num_sample_in_tst = 100

    self.metric = 'cider'

  def _assert(self):
    assert self.subcfgs[VE].dim_output == self.subcfgs[VD].subcfgs[CELL].dim_hidden


def gen_cfg(**kwargs):
  cfg = ModelConfig()
  cfg.val_iter = -1
  cfg.val_loss = False
  cfg.monitor_iter = 100
  cfg.trn_batch_size = 64
  cfg.tst_batch_size = 64
  cfg.base_lr = 1e-5
  cfg.num_epoch = kwargs['num_epoch']
  cfg.alpha = kwargs['alpha']
  cfg.reward_alpha = kwargs['reward_alpha']
  cfg.num_neg = kwargs['num_neg']
  cfg.num_sample = kwargs['num_sample']
  cfg.num_hard_neg = kwargs['num_hard_neg']
  cfg.max_margin = kwargs['margin']
  cfg.tst_task = kwargs['tst_task']
  cfg.topk = kwargs['topk']

  enc = cfg.subcfgs[VE]
  enc.dim_fts = kwargs['dim_fts']
  enc.dim_output = kwargs['dim_hidden']
  enc.keepin_prob = kwargs['content_keepin_prob']

  dec = cfg.subcfgs[VD]
  dec.num_step = kwargs['num_step']
  dec.dim_input = kwargs['dim_input']
  dec.dim_hidden = kwargs['dim_hidden']

  cell = dec.subcfgs[CELL]
  cell.dim_hidden = kwargs['dim_hidden']
  cell.dim_input = kwargs['dim_input']
  cell.keepout_prob = kwargs['cell_keepout_prob']
  cell.keepin_prob = kwargs['cell_keepin_prob']

  return cfg


class Model(framework.model.module.AbstractPGModel):
  name_scope = 'vemd.Model'

  class InKey(enum.Enum):
    FT = 'fts'
    CAPTIONID = 'captionids'
    CAPTION_MASK = 'caption_masks'
    ROLL_CAPTIONID = 'sample_captionids'
    ROLL_CAPTION_MASK = 'sample_caption_masks'
    PN_REWARD = 'pn_reward'
    PS_CIDER = 'ps_cider'
    PN_CIDER = 'pn_cider'
    IS_TRN = 'is_training'

  class OutKey(enum.Enum):
    LOGIT = 'logit'
    OUT_WID = 'out_wid'
    LOG_PROB = 'log_prob'
    ROLL_LOG_PROB = 'roll_log_prob'
    BEAM_CUM_LOG_PROB = 'beam_cum_log_prob'
    BEAM_PRE = 'beam_pre'
    BEAM_END = 'beam_end'
    BASELINE_OUT_WID = 'greedy_out_wid'

  def _set_submods(self):
    return {
      VE: encoder.dnn.Encoder(self._config.subcfgs[VE]),
      VD: decoder.rnn.Decoder(self._config.subcfgs[VD]),
    }

  def _add_input_in_mode(self, mode):
    if mode == framework.model.module.Mode.TRN_VAL:
      with tf.variable_scope(self.name_scope):
        fts = tf.placeholder(
          tf.float32, shape=(None, sum(self._config.subcfgs[VE].dim_fts)), name=self.InKey.FT.value)
        is_training = tf.placeholder(
          tf.bool, shape=(), name=self.InKey.IS_TRN.value)
        # trn only
        captionids = tf.placeholder(
          tf.int32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.CAPTIONID.value)
        caption_masks = tf.placeholder(
          tf.float32, shape=(None, self.config.subcfgs[VD].num_step), name=self.InKey.CAPTION_MASK.value)
        rollout_captionids = tf.placeholder(
          tf.int32, shape=(None, self._config.num_sample, self._config.subcfgs[VD].num_step), name=self.InKey.ROLL_CAPTIONID.value)
        rollout_caption_masks = tf.placeholder(
          tf.float32, shape=(None, self._config.num_sample, self.config.subcfgs[VD].num_step), name=self.InKey.ROLL_CAPTION_MASK.value)
        pn_reward = tf.placeholder(
          tf.float32, shape=(None, self._config.num_sample), name=self.InKey.PN_REWARD.value) # (num_pos*sample)
        ps_cider = tf.placeholder(
          tf.float32, shape=(None, self._config.num_sample), name=self.InKey.PS_CIDER.value)
        pn_cider = tf.placeholder(
          tf.float32, shape=(None, self._config.num_neg), name=self.InKey.PN_CIDER.value)

      return {
        self.InKey.FT: fts,
        self.InKey.CAPTIONID: captionids,
        self.InKey.CAPTION_MASK: caption_masks,
        self.InKey.ROLL_CAPTIONID: rollout_captionids,
        self.InKey.ROLL_CAPTION_MASK: rollout_caption_masks,
        self.InKey.PN_REWARD: pn_reward,
        self.InKey.PS_CIDER: ps_cider,
        self.InKey.PN_CIDER: pn_cider,
        self.InKey.IS_TRN: is_training,
      }
    else:
      with tf.variable_scope(self.name_scope):
        fts = tf.placeholder(
          tf.float32, shape=(None, sum(self._config.subcfgs[VE].dim_fts)), name=self.InKey.FT.value)
        captionids = tf.placeholder(
          tf.int32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.CAPTIONID.value)
        caption_masks = tf.placeholder(
          tf.float32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.CAPTION_MASK.value)
        is_training = tf.placeholder(
          tf.bool, shape=(), name=self.InKey.IS_TRN.value)

      return {
        self.InKey.FT: fts,
        self.InKey.CAPTIONID: captionids,
        self.InKey.CAPTION_MASK: caption_masks,
        self.InKey.IS_TRN: is_training,
      }

  def _build_parameter_graph(self):
    pass

  def get_out_ops_in_mode(self, in_ops, mode, **kwargs):
    encoder = self.submods[VE]
    decoder = self.submods[VD]

    out_ops = encoder.get_out_ops_in_mode({
      encoder.InKey.FT: in_ops[self.InKey.FT],
      encoder.InKey.IS_TRN: in_ops[self.InKey.IS_TRN],
    }, mode)
    ft_embed = out_ops[encoder.OutKey.EMBED] # (None, dim_output)

    def trn_val():
      batch_size = tf.shape(ft_embed)[0]

      # groundtruth
      captionids = in_ops[self.InKey.CAPTIONID]
      caption_masks = in_ops[self.InKey.CAPTION_MASK]
      init_wid = tf.zeros((batch_size,), dtype=tf.int32)

      vd_inputs = {
        decoder.InKey.FT: ft_embed,
        decoder.InKey.CAPTIONID: captionids,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.TRN, is_trn=True)
      logit = out_ops[decoder.OutKey.LOGIT]

      out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.TRN, is_trn=False)
      log_prob = out_ops[decoder.OutKey.LOG_PROB]
      norm_log_prob = tf.reduce_sum(log_prob*caption_masks[:, 1:], axis=1) / \
        tf.reduce_sum(caption_masks[:, 1:], axis=1) # (num_pos,)

      # val
      vd_inputs = {
        decoder.InKey.FT: ft_embed,
        decoder.InKey.INIT_WID: init_wid,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.VAL, search_strategy='greedy')
      out_wid = out_ops[decoder.OutKey.OUT_WID]

      # sample
      num_step = self._config.subcfgs[VD].num_step
      expanded_ft_embed = tf.tile(tf.expand_dims(ft_embed, 1), [1, self._config.num_sample, 1]) # (None, num_sample, dim_output)
      expanded_ft_embed = tf.reshape(expanded_ft_embed, (-1, self._config.subcfgs[VE].dim_output)) # (None*num_sample, dim_output)
      sample_captionid = in_ops[self.InKey.ROLL_CAPTIONID] # (None, num_sample, num_step)
      sample_captionid = tf.reshape(sample_captionid, (-1, num_step)) # (None*num_sample, num_step)

      vd_inputs = {
        decoder.InKey.FT: expanded_ft_embed,
        decoder.InKey.CAPTIONID: sample_captionid,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.TRN, is_trn=False)

      rollout_log_prob = out_ops[decoder.OutKey.LOG_PROB] # (None*num_sample, num_step)
      rollout_log_prob = tf.reshape(rollout_log_prob, (-1, self._config.num_sample, num_step-1))

      return {
        self.OutKey.OUT_WID: out_wid,
        self.OutKey.LOGIT: logit,
        self.OutKey.LOG_PROB: norm_log_prob,
        self.OutKey.ROLL_LOG_PROB: rollout_log_prob,
      }

    def rollout():
      batch_size = tf.shape(ft_embed)[0]
      init_wid = tf.zeros((batch_size,), dtype=tf.int32)

      vd_inputs = {
        decoder.InKey.FT: ft_embed,
        decoder.InKey.INIT_WID: init_wid,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode,
        search_strategy='greedy')
      baseline_out_wid = out_ops[decoder.OutKey.OUT_WID]

      out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode, 
        search_strategy='sample', num_sample=self._config.num_sample, topk=-1)
      roll_out_wid = out_ops[decoder.OutKey.OUT_WID]
      return {
        self.OutKey.BASELINE_OUT_WID: baseline_out_wid,
        self.OutKey.OUT_WID: roll_out_wid,
      }

    def tst():
      if self._config.tst_task == 'generation':
        return base.tst_generation_ops(self, ft_embed, decoder)
      elif self._config.tst_task == 'retrieval':
        return base.tst_retrieval_ops(
          self, ft_embed, in_ops[self.InKey.CAPTIONID], in_ops[self.InKey.CAPTION_MASK], decoder)

    delegate = {
      framework.model.module.Mode.TRN_VAL: trn_val,
      framework.model.module.Mode.ROLLOUT: rollout,
      framework.model.module.Mode.TST: tst,
    }
    return delegate[mode]()

  def _add_loss(self):
    with tf.variable_scope(self.name_scope):
      logits = self._outputs[self.OutKey.LOGIT] # (None*num_step, num_words)
      captionids = self._inputs[self.InKey.CAPTIONID]
      caption_masks = self._inputs[self.InKey.CAPTION_MASK]
      xentropy_loss = framework.util.expanded_op.cross_entropy_loss_on_rnn_logits(
        captionids, caption_masks, logits)
      if self._config.alpha == 1.:
        loss_op = 0.
      else:
        self.op2monitor['xentropy_loss'] = xentropy_loss
        loss_op = (1. - self._config.alpha) * xentropy_loss

      ps_ciders = self._inputs[self.InKey.PS_CIDER]
      pn_ciders = self._inputs[self.InKey.PN_CIDER] # (None, num_neg)
      pn_cider = tf.nn.top_k(pn_ciders, k=self._config.num_hard_neg)
      margin_rewards = tf.expand_dims(ps_ciders, 2) - tf.expand_dims(pn_ciders, 1) - self._config.margin
      margin_rewards = tf.reduce_mean(margin_rewards, axis=2) # (None, num_sample)

      reward = (1.0 - self._config.reward_alpha) * self._inputs[self.InKey.PN_REWARD] + self._config.reward_alpha * margin_rewards
      sample_log_prob = self._outputs[self.OutKey.ROLL_LOG_PROB]
      sample_caption_mask = self._inputs[self.InKey.ROLL_CAPTION_MASK]
      sample_caption_mask = sample_caption_mask[:, :, 1:]
      surrogate_loss = -tf.expand_dims(reward, axis=2) * sample_log_prob * sample_caption_mask
      surrogate_loss = tf.reduce_sum(surrogate_loss) / tf.reduce_sum(sample_caption_mask)
      if self._config.alpha != 0.:
        self.op2monitor['surrogate_loss'] = surrogate_loss
        self.op2monitor['reward'] = tf.reduce_mean(reward)
        loss_op += self._config.alpha * surrogate_loss

      self.op2monitor['loss'] = loss_op

    return loss_op

  def op_in_rollout(self, **kwargs):
    return {
      self.OutKey.BASELINE_OUT_WID: self._rollout_outputs[self.OutKey.BASELINE_OUT_WID],
      self.OutKey.OUT_WID: self._rollout_outputs[self.OutKey.OUT_WID],
    }

  def op_in_val(self, **kwargs):
    assert 'task' in kwargs

    if kwargs['task'] == 'generation':
      return {
        self.OutKey.OUT_WID: self._outputs[self.OutKey.OUT_WID],
      }
    elif kwargs['task'] == 'retrieval':
      return {
        self.OutKey.LOG_PROB: self._outputs[self.OutKey.LOG_PROB],
      }

  def op_in_tst(self, **kwargs):
    if self._config.tst_task == 'generation':
      if self._config.strategy == 'beam':
        return {
          self.OutKey.OUT_WID: self._outputs[self.OutKey.OUT_WID],
          self.OutKey.BEAM_CUM_LOG_PROB: self._outputs[self.OutKey.BEAM_CUM_LOG_PROB],
          self.OutKey.BEAM_PRE: self._outputs[self.OutKey.BEAM_PRE],
          self.OutKey.BEAM_END: self._outputs[self.OutKey.BEAM_END],
        }
      elif self._config.strategy == 'sample':
        return {
          self.OutKey.OUT_WID: self._outputs[self.OutKey.OUT_WID],
          self.OutKey.LOG_PROB: self._outputs[self.OutKey.ROLL_LOG_PROB],
        }
    elif self._config.tst_task == 'retrieval':
      return {
        self.OutKey.LOG_PROB: self._outputs[self.OutKey.LOG_PROB],
      }


PathCfg = qvevd_score_sample.PathCfg


class TrnTst(framework.model.trntst.PGTrnTst):
  def __init__(self, model_cfg, path_cfg, model, gen_sent_mode=1):
    framework.model.trntst.TrnTst.__init__(self, model_cfg, path_cfg, model)

    # caption int to string
    self.int2str = framework.util.caption.utility.CaptionInt2str(path_cfg.word_file)

    self.gen_sent_mode = gen_sent_mode

    with open(path_cfg.groundtruth_file) as f:
      self.vid2captions = cPickle.load(f)
    if self.model_cfg.metric == 'cider':
      self.cider = fast_cider.CiderScorer()
      self.cider.init_refs(self.vid2captions)

  def feed_data_and_rollout(self, data, sess):
    op_dict = self.model.op_in_rollout()

    feed_dict = self._construct_feed_dict_in_rollout(data)
    baseline_out_wids, roll_out_wids = sess.run(
      [op_dict[self.model.OutKey.BASELINE_OUT_WID], op_dict[self.model.OutKey.OUT_WID]], feed_dict=feed_dict)

    rollout_captionids, rollout_caption_masks = base.gen_captionid_masks_from_wids(roll_out_wids[:, :, :-1])

    vids = data['vids']
    if self.model_cfg.metric == 'cider':
      baseline_ciders = base.eval_cider_in_rollout(baseline_out_wids, vids, self.int2str, self.cider) # (None, 1)
      rollout_ciders = base.eval_cider_in_rollout(roll_out_wids, vids, self.int2str, self.cider) # (None, num_sample)
    elif self.model_cfg.metric == 'bcmr':
      baseline_ciders = base.eval_BCMR_in_rollout_fast(baseline_out_wids, vids, self.int2str, self.vid2captions) # (None, 1)
      rollout_ciders = base.eval_BCMR_in_rollout_fast(roll_out_wids, vids, self.int2str, self.vid2captions) # (None, num_sample)
    pos_rewards = rollout_ciders - baseline_ciders # (None, num_sample)

    data['pn_rewards'] = pos_rewards
    data['ps_ciders'] = rollout_ciders
    data['rollout_captionids'] = rollout_captionids
    data['rollout_caption_masks'] = rollout_caption_masks

    return data

  def _construct_feed_dict_in_rollout(self, data):
    fts = data['fts']
    batch_size = fts.shape[0]

    return {
      self.model.rollout_inputs[self.model.InKey.FT]: fts,
      self.model.rollout_inputs[self.model.InKey.IS_TRN]: False,
    }

  def _construct_feed_dict_in_trn(self, data):
    return {
      self.model.inputs[self.model.InKey.FT]: data['fts'],
      self.model.inputs[self.model.InKey.CAPTIONID]: data['captionids'],
      self.model.inputs[self.model.InKey.CAPTION_MASK]: data['caption_masks'],
      self.model.inputs[self.model.InKey.PN_REWARD]: data['pn_rewards'],
      self.model.inputs[self.model.InKey.PS_CIDER]: data['ps_ciders'],
      self.model.inputs[self.model.InKey.PN_CIDER]: data['neg_ciders'],
      self.model.inputs[self.model.InKey.ROLL_CAPTIONID]: data['rollout_captionids'],
      self.model.inputs[self.model.InKey.ROLL_CAPTION_MASK]: data['rollout_caption_masks'],
      self.model.inputs[self.model.InKey.IS_TRN]: True,
    }

  def feed_data_and_run_loss_op_in_val(self, data, sess):
    pass

  def predict_and_eval_in_val(self, sess, tst_reader, metrics):
    base.val_generation_task(self, sess, tst_reader, metrics)

  def predict_in_tst(self, sess, tst_reader, predict_file):
    if self.model_cfg.tst_task == 'generation':
      base.predict_generation_task(self, sess, tst_reader, predict_file,
        self.model_cfg.strategy, self.model_cfg.subcfgs[VD].sent_pool_size)
    elif self.model_cfg.tst_task == 'retrieval':
      base.predict_retrieval_task(self, sess, tst_reader, predict_file, 
        self.model_cfg.topk)


class TrnReader(framework.model.data.Reader):
  def __init__(self, 
      ft_files, videoid_file, quotient_file, annotation_file, captionstr_file, word_file, num_neg,
      metric='cider'):
    self.fts = np.empty(0)
    self.ft_idxs = np.empty(0)
    self.captionids = np.empty(0)
    self.caption_masks = np.empty(0)
    self.quotient_dict = {}
    self.videoid2captions = {}
    self.num_neg = num_neg
    self.metric = metric

    self.shuffled_idxs = []
    self.num_caption = 0

    fts = []
    for ft_file in ft_files:
      ft = np.load(ft_file)
      fts.append(ft)
    self.fts = np.concatenate(tuple(fts), axis=1)
    self.fts = self.fts.astype(np.float32)

    self.videoids = np.load(open(videoid_file))

    data = cPickle.load(file(annotation_file))
    self.ft_idxs = data[0]
    self.captionids = data[1]
    self.caption_masks = data[2]
    self.num_caption = self.ft_idxs.shape[0]

    with open(quotient_file) as f:
      data = json.load(f)
      for key in data:
        self.quotient_dict[key] = set(data[key])

    videoid2captions = cPickle.load(open(captionstr_file))
    for videoid in self.videoids:
      self.videoid2captions[videoid] = videoid2captions[videoid]

    if self.metric == 'cider':
      self.cider = fast_cider.CiderScorer()
      self.cider.init_refs(self.videoid2captions)
    self.int2str = framework.util.caption.utility.CaptionInt2str(word_file)

    self.shuffled_idxs = range(self.num_caption)
    random.shuffle(self.shuffled_idxs)

  def num_record(self):
    return self.num_caption

  def yield_trn_batch(self, batch_size):
    for i in range(0, self.num_caption, batch_size):
      start = i
      end = min(i + batch_size, self.num_caption)
      num = end-start
      idxs = self.shuffled_idxs[start:end]

      fts = self.fts[self.ft_idxs[idxs]]
      captionids = self.captionids[idxs]
      caption_masks = self.caption_masks[idxs]
      vids = self.videoids[self.ft_idxs[idxs]]

      quotient_set = set()
      for idx in idxs:
        quotient_set.add(idx)
        if idx in self.quotient_dict:
          quotient_set = quotient_set | self.quotient_dict[idx]

      idxs = range(self.num_caption)
      random.shuffle(idxs)
      neg_idxs = []
      for idx in idxs:
        if idx not in quotient_set:
          neg_idxs.append(idx)
        if len(neg_idxs) == self.num_neg:
          break

      neg_captionids = self.captionids[neg_idxs]
      neg_caption_masks = self.caption_masks[neg_idxs]
      neg_captions = self.int2str(neg_captionids)

      if self.metric == 'cider':
        neg_ciders = []
        for i in range(self.num_neg):
          score, scores = self.cider.compute_cider([neg_captions[i]]*(end-start), vids)
          neg_ciders.append(scores)
        neg_ciders = np.array(neg_ciders, dtype=np.float32).T # (num_pos, num_neg)
      elif self.metric == 'bcmr':
        neg_ciders = calc_BCMR_for_neg(self, neg_captions, vids)

      yield {
        'fts': fts,
        'captionids': captionids,
        'caption_masks': caption_masks,
        'neg_sample_captionids': neg_captionids,
        'neg_sample_caption_masks': neg_caption_masks,
        'neg_ciders': neg_ciders,
        'vids': vids,
      }


ValReader = qvevd_score_sample.ValReader
TstGenerationReader = qvevd_score_sample.TstGenerationReader
TstRetrievalReader = qvevd_score_sample.TstRetrievalReader


def calc_BCMR_for_neg(reader, neg_captions, vids, chunk_size=200):
  def parse_id(id):
    fields = id.split('_')
    i = int(fields[0])
    j = int(fields[1])
    return i, j

  num_pos = vids.shape[0]
  num_neg = reader.num_neg

  eval_data = []
  hyp_map = {}
  ref_map = {}
  for j, neg_caption in enumerate(neg_captions):
    for i, vid in enumerate(vids):
      key = '%d_%d'%(i, j)
      hyp_map[key] = [neg_caption]
      ref_map[key] = [reader.videoid2captions[vid]]
      eval_data.append({
        'pred': neg_caption,
        'gt': reader.videoid2captions[vid],
        'vid': vid,
        'id': key,
      })

  q = Queue()
  num_thread = 0
  num = len(eval_data)

  for i in range(0, num, chunk_size):
    worker = threading.Thread(
      target=base.bleu_scorer, args=(eval_data[i*chunk_size:(i+1)*chunk_size], q))
    worker.start()

    worker = threading.Thread(
      target=base.rouge_scorer, args=(eval_data[i*chunk_size:(i+1)*chunk_size], q))
    worker.start()

    worker = threading.Thread(
      target=base.cider_scorer, args=(eval_data[i*chunk_size:(i+1)*chunk_size], q))
    worker.start()

    num_thread += 3

  hyp_items = hyp_map.items()
  ref_items = ref_map.items()
  for i in range(0, num, chunk_size):
    _hyp_map = dict(hyp_items[i*chunk_size:(i+1)*chunk_size])
    _ref_map = dict(ref_items[i*chunk_size:(i+1)*chunk_size])
    worker = threading.Thread(
      target=base.meteor_scorer, args=(_hyp_map, _ref_map, q))
    worker.start()
    num_thread += 1

  out_scores = np.zeros((num_pos, num_neg), dtype=np.float32)
  for _ in range(num_thread):
    data = q.get()
    service = data['service']
    data = data['data']
    if service == 'bleu':
      for d in data:
        id = d['id']
        score = d['score']
        i, j = parse_id(id)
        out_scores[i, j] += 0.5 * score[0] + 0.5 * score[1] + score[2] + score[3]
    elif service == 'meteor':
      for key in data:
        val = data[key][0]
        i, j = parse_id(key)
        out_scores[i, j] += 5.0 * val
    elif service == 'cider':
      for d in data:
        id = d['id']
        score = d['score']
        i, j = parse_id(id)
        out_scores[i, j] += score
    elif service == 'rouge':
      for d in data:
        id = d['id']
        score = d['score']
        i, j = parse_id(id)
        out_scores[i, j] += 2.0 * score

  return out_scores

  # neg_ciders = []
  # for j in range(num_neg):
  #   score, scores = reader.cider.compute_cider([neg_captions[j]]*num_pos, vids)
  #   neg_ciders.append(scores)
  # neg_ciders = np.array(neg_ciders, dtype=np.float32).T # (num_pos, num_neg)

  # hyp_maps = []
  # for j in range(num_neg):
  #   hyp_maps.append({})
  # for j, neg_caption in enumerate(neg_captions):
  #   res, gts = {}, {}
  #   for i, vid in enumerate(vids):
  #     res[i] = [neg_caption]
  #     gts[i] = reader.videoid2captions[vid]
  #     hyp_maps[j]['%d_%d'%(vid, j)] = [neg_caption]

  #   _, bleu_scores = reader.bleu_scorer.compute_score(gts, res)
  #   for i in range(num_pos):
  #     neg_ciders[i, j] += 0.5*bleu_scores[0][i] + 0.5*bleu_scores[1][i]

  #   _, rouge_scores = reader.rouge_scorer.compute_score(gts, res)
  #   neg_ciders[:, j] += 2.0 * rouge_scores

  # q = Queue()
  # vid2idx = {}
  # for j in range(num_neg):
  #   ref_map = {}
  #   for i, vid in enumerate(vids):
  #     ref_map['%d_%d'%(vid, j)] = [reader.videoid2captions[vid]]
  #     vid2idx[vid] = i
  #   worker = threading.Thread(target=meteor_scorer, args=(hyp_maps[j], ref_map, q))
  #   worker.start()
  # for _ in range(num_neg):
  #   data = q.get()
  #   for key in data:
  #    val = data[key][0]
  #    fields = key.split('_')
  #    vid = int(fields[0])
  #    j = int(fields[1])
  #    neg_ciders[vid2idx[vid], j] += 5.0*val

  # return neg_ciders
