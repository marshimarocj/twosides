import enum
import sys
import os
import cPickle
import json
import random
sys.path.append('../')

import numpy as np
import tensorflow as tf

from bleu import bleu
from cider import cider

import framework.model.module
import framework.model.trntst
import framework.model.data
from framework.model.module import Mode
import framework.util.caption.utility
import base
import encoder.dnn
import decoder.rnn
import qvevd_score_sample
import fast_cider

VE = 'encoder'
VD = 'decoder'
CELL = decoder.rnn.CELL


class ModelConfig(framework.model.module.ModelConfig):
  def __init__(self):
    framework.model.module.ModelConfig.__init__(self)

    self.subcfgs[VE] = encoder.dnn.Config()
    self.subcfgs[VD] = decoder.rnn.Config()
    self.alphas = [.33, .33]
    self.max_margin = .5
    self.num_neg = 16
    self.num_sample = 1

    self.tst_task = 'generation'
    self.topk = 10
    self.sn_delta_threshold = .3
    self.strategy = 'beam'
    self.sample_topk = -1
    self.num_sample_in_tst = 100

  def _assert(self):
    assert self.subcfgs[VE].dim_output == self.subcfgs[VD].subcfgs[CELL].dim_hidden


def gen_cfg(**kwargs):
  cfg = ModelConfig()
  cfg.val_iter = -1
  cfg.val_loss = False
  cfg.monitor_iter = 100
  cfg.trn_batch_size = 32
  cfg.tst_batch_size = 64
  cfg.base_lr = 1e-5
  cfg.num_epoch = kwargs['num_epoch']
  cfg.alphas = kwargs['alphas']
  cfg.num_neg = kwargs['num_neg']
  cfg.num_sample = kwargs['num_sample']
  cfg.max_margin = kwargs['max_margin']
  cfg.tst_task = kwargs['tst_task']
  cfg.topk = kwargs['topk']
  cfg.sn_delta_threshold = kwargs['sn_delta_threshold']

  enc = cfg.subcfgs[VE]
  enc.dim_fts = kwargs['dim_fts']
  enc.dim_output = kwargs['dim_hidden']
  enc.keepin_prob = kwargs['content_keepin_prob']

  dec = cfg.subcfgs[VD]
  dec.num_step = kwargs['num_step']
  dec.dim_input = kwargs['dim_input']
  dec.dim_hidden = kwargs['dim_hidden']

  cell = dec.subcfgs[CELL]
  cell.dim_hidden = kwargs['dim_hidden']
  cell.dim_input = kwargs['dim_input']
  cell.keepout_prob = kwargs['cell_keepout_prob']
  cell.keepin_prob = kwargs['cell_keepin_prob']

  return cfg


class Model(framework.model.module.AbstractPGModel):
  name_scope = 'vemd.Model'

  class InKey(enum.Enum):
    FT = 'fts'
    CAPTIONID = 'captionids'
    CAPTION_MASK = 'caption_masks'
    NS_CAPTIONID = 'neg_sample_captionids'
    NS_CAPTION_MASK = 'neg_sample_caption_masks'
    NUM_POS = 'num_pos'
    DELTA = 'delta'
    SDELTA = 'sample_delta'
    SCONSTRAINT = 'sample_constraint'
    REWARD = 'reward'
    IS_TRN = 'is_training'

  class OutKey(enum.Enum):
    LOGIT = 'logit'
    OUT_WID = 'out_wid'
    LOG_PROB = 'log_prob'
    NLOG_PROB = 'neg_log_prob'
    SLOG_PROB = 'sample_log_prob'
    BEAM_CUM_LOG_PROB = 'beam_cum_log_prob'
    BEAM_PRE = 'beam_pre'
    BEAM_END = 'beam_end'
    BASELINE_OUT_WID = 'greedy_out_wid'
    ROLL_OUT_WID = 'sample_out_wid'
    RLOG_PROB = 'sample_log_prob_in_val'

  def _set_submods(self):
    return {
      VE: encoder.dnn.Encoder(self._config.subcfgs[VE]),
      VD: decoder.rnn.Decoder(self._config.subcfgs[VD]),
    }

  def _add_input_in_mode(self, mode):
    if mode == framework.model.module.Mode.TRN_VAL:
      with tf.variable_scope(self.name_scope):
        fts = tf.placeholder(
          tf.float32, shape=(None, sum(self._config.subcfgs[VE].dim_fts)), name=self.InKey.FT.value)
        is_training = tf.placeholder(
          tf.bool, shape=(), name=self.InKey.IS_TRN.value)
        # trn only
        captionids = tf.placeholder(
          tf.int32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.CAPTIONID.value)
        caption_masks = tf.placeholder(
          tf.float32, shape=(None, self.config.subcfgs[VD].num_step), name=self.InKey.CAPTION_MASK.value)
        neg_sample_captionids = tf.placeholder(
          tf.int32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.NS_CAPTIONID.value)
        neg_sample_caption_masks = tf.placeholder(
          tf.float32, shape=(None, self.config.subcfgs[VD].num_step), name=self.InKey.NS_CAPTION_MASK.value)
        deltas = tf.placeholder(
          tf.float32, shape=(None, self._config.num_neg), name=self.InKey.DELTA.value)
        sample_deltas = tf.placeholder(
          tf.float32, shape=(None, None), name=self.InKey.SDELTA.value) # (num_pos*num_sample, 1 + num_pos*num_neg)
        sample_constraints = tf.placeholder(
          tf.float32, shape=(None, None), name=self.InKey.SCONSTRAINT.value) # (num_pos*num_sample, 1 + num_pos*num_neg)
        reward = tf.placeholder(
          tf.float32, shape=(None,), name=self.InKey.REWARD.value) # (num_pos*sample)

      return {
        self.InKey.FT: fts,
        self.InKey.CAPTIONID: captionids,
        self.InKey.CAPTION_MASK: caption_masks,
        self.InKey.NS_CAPTIONID: neg_sample_captionids,
        self.InKey.NS_CAPTION_MASK: neg_sample_caption_masks,
        self.InKey.DELTA: deltas,
        self.InKey.SDELTA: sample_deltas,
        self.InKey.SCONSTRAINT: sample_constraints,
        self.InKey.REWARD: reward,
        self.InKey.IS_TRN: is_training,
      }
    else:
      with tf.variable_scope(self.name_scope):
        fts = tf.placeholder(
          tf.float32, shape=(None, sum(self._config.subcfgs[VE].dim_fts)), name=self.InKey.FT.value)
        captionids = tf.placeholder(
          tf.int32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.CAPTIONID.value)
        caption_masks = tf.placeholder(
          tf.float32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.CAPTION_MASK.value)
        is_training = tf.placeholder(
          tf.bool, shape=(), name=self.InKey.IS_TRN.value)

      return {
        self.InKey.FT: fts,
        self.InKey.CAPTIONID: captionids,
        self.InKey.CAPTION_MASK: caption_masks,
        self.InKey.IS_TRN: is_training,
      }

  def _build_parameter_graph(self):
    pass

  def get_out_ops_in_mode(self, in_ops, mode, **kwargs):
    encoder = self.submods[VE]
    decoder = self.submods[VD]

    out_ops = encoder.get_out_ops_in_mode({
      encoder.InKey.FT: in_ops[self.InKey.FT],
      encoder.InKey.IS_TRN: in_ops[self.InKey.IS_TRN],
    }, mode)
    ft_embed = out_ops[encoder.OutKey.EMBED] # (None, dim_output)

    def trn_val():
      num_pos = tf.shape(ft_embed)[0]

      # pos
      pos_captionids = in_ops[self.InKey.CAPTIONID]
      pos_caption_masks = in_ops[self.InKey.CAPTION_MASK]
      init_wid = tf.zeros((num_pos,), dtype=tf.int32)

      vd_inputs = {
        decoder.InKey.FT: ft_embed,
        decoder.InKey.INIT_WID: init_wid,
        decoder.InKey.CAPTIONID: pos_captionids,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.TRN, is_trn=True)
      logit = out_ops[decoder.OutKey.LOGIT]

      out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.TRN, is_trn=False)
      log_prob = out_ops[decoder.OutKey.LOG_PROB]
      norm_log_prob = tf.reduce_sum(log_prob*pos_caption_masks[:, 1:], axis=1) / \
        tf.reduce_sum(pos_caption_masks[:, 1:], axis=1) # (num_pos,)

      # val
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.VAL, search_strategy='greedy')
      out_wid = out_ops[decoder.OutKey.OUT_WID]

      out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.VAL, 
        search_strategy='sample', num_sample=5, topk=5)
      roll_out_wid = out_ops[decoder.OutKey.OUT_WID]
      roll_log_prob = out_ops[decoder.OutKey.LOG_PROB]

      # neg
      captionids = in_ops[self.InKey.NS_CAPTIONID]
      caption_masks = in_ops[self.InKey.NS_CAPTION_MASK]
      expanded_ft_embed = tf.tile(tf.expand_dims(ft_embed, 1), [1, self._config.num_neg, 1]) # (num_pos, num_neg, dim_output)
      expanded_ft_embed = tf.reshape(expanded_ft_embed, (-1, self._config.subcfgs[VE].dim_output)) # (num_pos*num_neg, dim_output)
      neg_captionid = captionids[:self._config.num_neg]
      neg_captionid = tf.tile(tf.expand_dims(neg_captionid, 0), [num_pos, 1, 1]) # (num_pos, num_neg, num_step)
      neg_captionid = tf.reshape(neg_captionid, (-1, self._config.subcfgs[VD].num_step))
      neg_caption_masks = caption_masks[:self._config.num_neg]
      neg_caption_masks = tf.tile(tf.expand_dims(neg_caption_masks, 0), [num_pos, 1, 1]) # (num_pos, num_neg, num_step)
      neg_caption_masks = tf.reshape(neg_caption_masks, (-1, self._config.subcfgs[VD].num_step))

      vd_inputs = {
        decoder.InKey.FT: expanded_ft_embed,
        decoder.InKey.CAPTIONID: neg_captionid,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.TRN, is_trn=False)

      neg_log_prob = out_ops[decoder.OutKey.LOG_PROB]
      norm_neg_log_prob = tf.reduce_sum(neg_log_prob * neg_caption_masks[:, 1:], axis=1) / \
        tf.reduce_sum(neg_caption_masks[:, 1:], axis=1) # (num_pos*num_neg,)
      norm_neg_log_prob = tf.reshape(norm_neg_log_prob, (-1, self._config.num_neg)) # (num_pos, num_neg)

      # sample
      expanded_ft_embed = tf.tile(tf.expand_dims(ft_embed, 1), [1, self._config.num_sample, 1]) # (num_pos, num_sample, dim_output)
      expanded_ft_embed = tf.reshape(expanded_ft_embed, (-1, self._config.subcfgs[VE].dim_output)) # (num_pos*num_sample, dim_output)
      sample_captionid = captionids[self._config.num_neg:] # (num_pos*num_sample, num_step)
      sample_caption_masks = caption_masks[self._config.num_neg:]
      init_wid = tf.zeros((num_pos*self._config.num_sample,), dtype=tf.int32)

      vd_inputs = {
        decoder.InKey.FT: expanded_ft_embed,
        decoder.InKey.CAPTIONID: sample_captionid,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.TRN, is_trn=False)

      sample_log_prob = out_ops[decoder.OutKey.LOG_PROB]
      norm_sample_log_prob = tf.reduce_sum(sample_log_prob * sample_caption_masks[:, 1:], axis=1) / \
        tf.reduce_sum(sample_caption_masks[:, 1:], axis=1) # (num_pos*num_sample,)
      norm_sample_log_prob = tf.reshape(norm_sample_log_prob, (-1, self._config.num_sample)) # (num_pos, num_sample)

      return {
        self.OutKey.OUT_WID: out_wid,
        self.OutKey.ROLL_OUT_WID: roll_out_wid,
        self.OutKey.RLOG_PROB: roll_log_prob,
        self.OutKey.LOGIT: logit,
        self.OutKey.LOG_PROB: norm_log_prob, # (num_pos,)
        self.OutKey.NLOG_PROB: norm_neg_log_prob, # (num_pos, num_neg)
        self.OutKey.SLOG_PROB: norm_sample_log_prob, # (num_pos, num_sample)
      }

    def rollout():
      batch_size = tf.shape(ft_embed)[0]
      init_wid = tf.zeros((batch_size,), dtype=tf.int32)

      vd_inputs = {
        decoder.InKey.FT: ft_embed,
        decoder.InKey.INIT_WID: init_wid,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode,
        search_strategy='greedy')
      baseline_out_wid = out_ops[decoder.OutKey.OUT_WID]

      out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode, 
        search_strategy='sample', num_sample=self._config.num_sample, topk=-1)
      roll_out_wid = out_ops[decoder.OutKey.OUT_WID]
      return {
        self.OutKey.BASELINE_OUT_WID: baseline_out_wid,
        self.OutKey.ROLL_OUT_WID: roll_out_wid,
      }

    def tst():
      if self._config.tst_task == 'generation':
        return base.tst_generation_ops(self, ft_embed, decoder)
      elif self._config.tst_task == 'retrieval':
        return base.tst_retrieval_ops(
          self, ft_embed, in_ops[self.InKey.CAPTIONID], in_ops[self.InKey.CAPTION_MASK], decoder)

    delegate = {
      framework.model.module.Mode.TRN_VAL: trn_val,
      framework.model.module.Mode.ROLLOUT: rollout,
      framework.model.module.Mode.TST: tst,
    }
    return delegate[mode]()

  def _add_loss(self):
    with tf.variable_scope(self.name_scope):
      logits = self._outputs[self.OutKey.LOGIT] # (None*num_step, num_words)
      captionids = self._inputs[self.InKey.CAPTIONID]
      caption_masks = self._inputs[self.InKey.CAPTION_MASK]
      xentropy_loss = framework.util.expanded_op.cross_entropy_loss_on_rnn_logits(
        captionids, caption_masks, logits)
      self.op2monitor['xentropy_loss'] = xentropy_loss
      loss_op = (1. - sum(self._config.alphas)) * xentropy_loss

      log_prob = self._outputs[self.OutKey.LOG_PROB]
      log_prob = tf.expand_dims(log_prob, 1) # (None, 1)
      neg_log_prob = self._outputs[self.OutKey.NLOG_PROB] # (None, num_neg)

      deltas = self._inputs[self.InKey.DELTA]
      max_margin = self._config.max_margin * tf.ones_like(deltas, dtype=tf.float32)
      margin = tf.minimum(deltas, max_margin)
      margin_loss = margin + neg_log_prob - log_prob
      margin_loss = tf.maximum(tf.zeros_like(margin_loss), margin_loss)
      margin_loss = tf.reduce_mean(margin_loss)
      loss_op += self._config.alphas[0] * margin_loss
      margin_total = margin_loss

      sample_deltas = self._inputs[self.InKey.SDELTA] # (num_pos*num_sample, 1 + num_pos*num_neg)
      sample_constraints = self._inputs[self.InKey.SCONSTRAINT] # (num_pos*num_sample, 1 + num_pos*num_neg)
      sample_log_prob = self._outputs[self.OutKey.SLOG_PROB] # (num_pos, num_sample)

      ps_deltas = sample_deltas[:, 0]
      ps_deltas = tf.reshape(ps_deltas, (-1, self._config.num_sample))
      ps_constraints = sample_constraints[:, 0]
      ps_constraints = tf.reshape(ps_constraints, (-1, self._config.num_sample))
      margin_loss = ps_deltas + sample_log_prob - log_prob
      margin_loss = tf.maximum(tf.zeros_like(margin_loss), margin_loss)
      margin_loss = tf.reduce_sum(margin_loss * ps_constraints) / tf.maximum(tf.reduce_sum(ps_constraints), 1.)
      loss_op += self._config.alphas[0] * margin_loss
      margin_total += margin_loss

      sn_deltas = sample_deltas[:, 1:] # (num_pos*num_sample, num_pos*num_neg)
      sn_constraints = sample_constraints[:, 1:]
      sample_log_prob = tf.reshape(sample_log_prob, (-1, 1)) # (num_pos*num_sample, 1)
      neg_log_prob = tf.reshape(neg_log_prob, (1, -1)) # (1, num_pos*num_neg)
      margin_loss = sn_deltas + neg_log_prob - sample_log_prob
      margin_loss = tf.maximum(tf.zeros_like(margin_loss), margin_loss)
      margin_loss = tf.reduce_sum(margin_loss * sn_constraints) / tf.maximum(tf.reduce_sum(sn_constraints), 1.)
      loss_op += self._config.alphas[0] * margin_loss
      margin_total += margin_loss
      self.op2monitor['margin_loss'] = margin_total

      reward = self._inputs[self.InKey.REWARD]
      sample_log_prob = self._outputs[self.OutKey.SLOG_PROB]
      sample_log_prob = tf.reshape(sample_log_prob, (-1,))
      roll_mask = self.inputs[self.InKey.NS_CAPTION_MASK][self._config.num_neg:, 1:]
      surrogate_loss = -tf.reduce_sum(reward * sample_log_prob * tf.reduce_sum(roll_mask, axis=1)) / tf.reduce_sum(roll_mask)
      self.op2monitor['surrogate_loss'] = surrogate_loss
      self.op2monitor['reward'] = tf.reduce_mean(reward)
      loss_op += self._config.alphas[1] * surrogate_loss

      self.op2monitor['loss'] = loss_op

    return loss_op

  def op_in_rollout(self, **kwargs):
    return {
      self.OutKey.BASELINE_OUT_WID: self._rollout_outputs[self.OutKey.BASELINE_OUT_WID],
      self.OutKey.ROLL_OUT_WID: self._rollout_outputs[self.OutKey.ROLL_OUT_WID],
    }

  def op_in_val(self, **kwargs):
    assert 'task' in kwargs

    if kwargs['task'] == 'generation':
      return {
        self.OutKey.OUT_WID: self._outputs[self.OutKey.OUT_WID],
        self.OutKey.ROLL_OUT_WID: self._outputs[self.OutKey.ROLL_OUT_WID],
        self.OutKey.RLOG_PROB: self._outputs[self.OutKey.RLOG_PROB],
      }
    elif kwargs['task'] == 'retrieval':
      return {
        self.OutKey.LOG_PROB: self._outputs[self.OutKey.LOG_PROB],
      }

  def op_in_tst(self):
    if self._config.tst_task == 'generation':
      if self._config.strategy == 'beam':
        return {
          self.OutKey.OUT_WID: self._outputs[self.OutKey.OUT_WID],
          self.OutKey.BEAM_CUM_LOG_PROB: self._outputs[self.OutKey.BEAM_CUM_LOG_PROB],
          self.OutKey.BEAM_PRE: self._outputs[self.OutKey.BEAM_PRE],
          self.OutKey.BEAM_END: self._outputs[self.OutKey.BEAM_END],
        }
      elif self._config.strategy == 'sample':
        return {
          self.OutKey.OUT_WID: self._outputs[self.OutKey.OUT_WID],
          self.OutKey.LOG_PROB: self._outputs[self.OutKey.LOG_PROB],
        }
    elif self._config.tst_task == 'retrieval':
      return {
        self.OutKey.LOG_PROB: self._outputs[self.OutKey.LOG_PROB],
      }


PathCfg = qvevd_score_sample.PathCfg


class TrnTst(framework.model.trntst.PGTrnTst):
  def __init__(self, model_cfg, path_cfg, model, gen_sent_mode=1):
    framework.model.trntst.TrnTst.__init__(self, model_cfg, path_cfg, model)

    # caption int to string
    self.int2str = framework.util.caption.utility.CaptionInt2str(path_cfg.word_file)

    self.gen_sent_mode = gen_sent_mode

    self.cider = fast_cider.CiderScorer()
    with open(path_cfg.groundtruth_file) as f:
      vid2captions = cPickle.load(f)
    self.cider.init_refs(vid2captions)

  def feed_data_and_rollout(self, data, sess):
    op_dict = self.model.op_in_rollout()

    feed_dict = self._construct_feed_dict_in_rollout(data)
    baseline_out_wids, roll_out_wids = sess.run(
      [op_dict[self.model.OutKey.BASELINE_OUT_WID], op_dict[self.model.OutKey.ROLL_OUT_WID]], feed_dict=feed_dict)

    vids = data['vids']
    baseline_ciders = base.eval_cider_in_rollout(baseline_out_wids, vids, self.int2str, self.cider) # (None, 1)
    rollout_ciders = base.eval_cider_in_rollout(roll_out_wids, vids, self.int2str, self.cider) # (None, num_sample)
    rewards = rollout_ciders - baseline_ciders
    rewards = rewards.reshape((-1,)) # (None*num_sample,)

    rollout_captionids, rollout_caption_masks = base.gen_captionid_masks_from_wids(roll_out_wids)
    rollout_captionids = rollout_captionids.reshape((-1, rollout_captionids.shape[-1]))
    rollout_caption_masks = rollout_caption_masks.reshape((-1, rollout_caption_masks.shape[-1]))

    rollout_ciders = np.expand_dims(rollout_ciders.reshape((-1,)), 1) # (num_pos*num_sample, 1)
    neg_ciders = np.expand_dims(data['neg_ciders'].reshape((-1,)), 0) # (1, num_pos*num_neg)

    sp_deltas = 1.0 - rollout_ciders
    sp_constraints = sp_deltas > 0
    sn_deltas = rollout_ciders - neg_ciders 
    sn_constraints = sn_deltas > self.model_cfg.sn_delta_threshold
    deltas = np.concatenate([sp_deltas, sn_deltas], axis=1) # (num_pos*num_sample, 1+num_pos*num_neg)
    constraints = np.concatenate([sp_constraints, sn_constraints], axis=1)
    constraints = np.array(constraints, dtype=np.float32)

    data['neg_sample_captionids'] = np.concatenate([data['neg_sample_captionids'], rollout_captionids], axis=0)
    data['neg_sample_caption_masks'] = np.concatenate([data['neg_sample_caption_masks'], rollout_caption_masks], axis=0)
    data['sample_deltas'] = deltas
    data['sample_constraints'] = constraints
    data['rewards'] = rewards

    return data

  def _construct_feed_dict_in_rollout(self, data):
    fts = data['fts']
    batch_size = fts.shape[0]

    return {
      self.model.rollout_inputs[self.model.InKey.FT]: fts,
      self.model.rollout_inputs[self.model.InKey.IS_TRN]: False,
    }

  def _construct_feed_dict_in_trn(self, data):
    return {
      self.model.inputs[self.model.InKey.FT]: data['fts'],
      self.model.inputs[self.model.InKey.CAPTIONID]: data['captionids'],
      self.model.inputs[self.model.InKey.CAPTION_MASK]: data['caption_masks'],
      self.model.inputs[self.model.InKey.NS_CAPTIONID]: data['neg_sample_captionids'],
      self.model.inputs[self.model.InKey.NS_CAPTION_MASK]: data['neg_sample_caption_masks'],
      self.model.inputs[self.model.InKey.DELTA]: data['deltas'],
      self.model.inputs[self.model.InKey.SDELTA]: data['sample_deltas'],
      self.model.inputs[self.model.InKey.SCONSTRAINT]: data['sample_constraints'],
      self.model.inputs[self.model.InKey.REWARD]: data['rewards'],
      self.model.inputs[self.model.InKey.IS_TRN]: True,
    }

  def feed_data_and_run_loss_op_in_val(self, data, sess):
    pass

  def predict_and_eval_in_val(self, sess, tst_reader, metrics):
    base.val_generation_task(self, sess, tst_reader, metrics)
    base.val_generation_task_sample(self, sess, tst_reader, metrics)
    base.val_retrieval_task(self, sess, tst_reader, metrics)

  def predict_in_tst(self, sess, tst_reader, predict_file):
    if self.model_cfg.tst_task == 'generation':
      base.predict_generation_task(self, sess, tst_reader, predict_file,
        self.model_cfg.strategy, self.model_cfg.subcfgs[VD].sent_pool_size)
    elif self.model_cfg.tst_task == 'retrieval':
      base.predict_retrieval_task(self, sess, tst_reader, predict_file, 
        self.model_cfg.topk)


TrnReader = qvevd_score_sample.TrnReader
ValReader = qvevd_score_sample.ValReader
TstGenerationReader = qvevd_score_sample.TstGenerationReader
TstRetrievalReader = qvevd_score_sample.TstRetrievalReader
