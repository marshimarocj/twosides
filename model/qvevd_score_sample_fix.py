import enum
import sys
import os
import cPickle
import json
import random
sys.path.append('../')

import numpy as np
import tensorflow as tf

from bleu import bleu
from cider import cider

import framework.model.module
import framework.model.trntst
import framework.model.data
import framework.util.caption.utility
import base
import encoder.dnn
import decoder.rnn
import qvevd
import fast_cider

VE = 'encoder'
VD = 'decoder'
CELL = decoder.rnn.CELL


class ModelConfig(framework.model.module.ModelConfig):
  def __init__(self):
    framework.model.module.ModelConfig.__init__(self)

    self.subcfgs[VE] = encoder.dnn.Config()
    self.subcfgs[VD] = decoder.rnn.Config()
    self.alphas = [.25, .25, .25]
    self.max_margin = .5
    self.num_neg = 16
    self.num_sample = 1
    self.topk = 10

    self.sn_delta_threshold = .3

  def _assert(self):
    assert self.subcfgs[VE].dim_output == self.subcfgs[VD].subcfgs[CELL].dim_hidden


def gen_cfg(**kwargs):
  cfg = ModelConfig()
  cfg.val_iter = -1
  cfg.val_loss = False
  cfg.monitor_iter = 100
  cfg.trn_batch_size = 32
  cfg.tst_batch_size = 64
  cfg.base_lr = 1e-4
  cfg.num_epoch = kwargs['num_epoch']
  cfg.alphas = kwargs['alphas']
  cfg.num_neg = kwargs['num_neg']
  cfg.num_sample = kwargs['num_sample']
  cfg.max_margin = kwargs['max_margin']
  cfg.sn_delta_threshold = kwargs['sn_delta_threshold']
  cfg.topk = kwargs['topk']

  enc = cfg.subcfgs[VE]
  enc.dim_fts = kwargs['dim_fts']
  enc.dim_output = kwargs['dim_hidden']
  enc.keepin_prob = kwargs['content_keepin_prob']

  dec = cfg.subcfgs[VD]
  dec.num_step = kwargs['num_step']
  dec.dim_input = kwargs['dim_input']
  dec.dim_hidden = kwargs['dim_hidden']

  cell = dec.subcfgs[CELL]
  cell.dim_hidden = kwargs['dim_hidden']
  cell.dim_input = kwargs['dim_input']
  cell.keepout_prob = kwargs['cell_keepout_prob']
  cell.keepin_prob = kwargs['cell_keepin_prob']

  return cfg


class Model(framework.model.module.AbstractModel):
  name_scope = 'vemd.Model'

  class InKey(enum.Enum):
    FT = 'fts'
    CAPTIONID = 'captionids'
    CAPTION_MASK = 'caption_masks'
    NS_CAPTIONID = 'neg_sample_captionids'
    NS_CAPTION_MASK = 'neg_sample_caption_masks'
    NUM_POS = 'num_pos'
    DELTA = 'delta'
    SDELTA = 'sample_delta'
    SCONSTRAINT = 'sample_constraint'
    IS_TRN = 'is_training'

  class OutKey(enum.Enum):
    LOG_PROB = 'log_prob'
    NLOG_PROB = 'neg_log_prob'
    SLOG_PROB = 'sample_log_prob'
    BEAM_CUM_LOG_PROB = 'beam_cum_log_prob'
    BEAM_PRE = 'beam_pre'
    BEAM_END = 'beam_end'

  def _set_submods(self):
    return {
      VE: encoder.dnn.Encoder(self._config.subcfgs[VE]),
      VD: decoder.rnn.Decoder(self._config.subcfgs[VD]),
    }

  def _add_input_in_mode(self, mode):
    if mode == framework.model.module.Mode.TRN_VAL:
      with tf.variable_scope(self.name_scope):
        fts = tf.placeholder(
          tf.float32, shape=(None, sum(self._config.subcfgs[VE].dim_fts)), name=self.InKey.FT.value)
        is_training = tf.placeholder(
          tf.bool, shape=(), name=self.InKey.IS_TRN.value)
        # trn only
        captionids = tf.placeholder(
          tf.int32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.CAPTIONID.value)
        caption_masks = tf.placeholder(
          tf.float32, shape=(None, self.config.subcfgs[VD].num_step), name=self.InKey.CAPTION_MASK.value)
        neg_sample_captionids = tf.placeholder(
          tf.int32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.NS_CAPTIONID.value)
        neg_sample_caption_masks = tf.placeholder(
          tf.float32, shape=(None, self.config.subcfgs[VD].num_step), name=self.InKey.NS_CAPTION_MASK.value)
        deltas = tf.placeholder(
          tf.float32, shape=(None, self._config.num_neg), name=self.InKey.DELTA.value)
        sample_deltas = tf.placeholder(
          tf.float32, shape=(None, None), name=self.InKey.SDELTA.value) # (num_pos*num_sample, 1 + num_pos*num_neg)
        sample_constraints = tf.placeholder(
          tf.float32, shape=(None, None), name=self.InKey.SCONSTRAINT.value) # (num_pos*num_smaple, 1 + num_pos*num_neg)

      return {
        self.InKey.FT: fts,
        self.InKey.CAPTIONID: captionids,
        self.InKey.CAPTION_MASK: caption_masks,
        self.InKey.NS_CAPTIONID: neg_sample_captionids,
        self.InKey.NS_CAPTION_MASK: neg_sample_caption_masks,
        self.InKey.DELTA: deltas,
        self.InKey.SDELTA: sample_deltas,
        self.InKey.SCONSTRAINT: sample_constraints,
        self.InKey.IS_TRN: is_training,
      }
    else:
      with tf.variable_scope(self.name_scope):
        fts = tf.placeholder(
          tf.float32, shape=(None, sum(self._config.subcfgs[VE].dim_fts)), name=self.InKey.FT.value)
        captionids = tf.placeholder(
          tf.int32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.CAPTIONID.value)
        caption_masks = tf.placeholder(
          tf.float32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.CAPTION_MASK.value)
        is_training = tf.placeholder(
          tf.bool, shape=(), name=self.InKey.IS_TRN.value)

      return {
        self.InKey.FT: fts,
        self.InKey.CAPTIONID: captionids,
        self.InKey.CAPTION_MASK: caption_masks,
        self.InKey.IS_TRN: is_training,
      }

  def _build_parameter_graph(self):
    pass

  def get_out_ops_in_mode(self, in_ops, mode, **kwargs):
    encoder = self.submods[VE]
    decoder = self.submods[VD]

    out_ops = encoder.get_out_ops_in_mode({
      encoder.InKey.FT: in_ops[self.InKey.FT],
      encoder.InKey.IS_TRN: in_ops[self.InKey.IS_TRN],
    }, mode)
    ft_embed = out_ops[encoder.OutKey.EMBED] # (None, dim_output)

    def trn_val():
      num_pos = tf.shape(ft_embed)[0]

      # pos
      pos_captionids = in_ops[self.InKey.CAPTIONID]
      pos_caption_masks = in_ops[self.InKey.CAPTION_MASK]
      init_wid = tf.zeros((num_pos,), dtype=tf.int32)

      vd_inputs = {
        decoder.InKey.FT: ft_embed,
        decoder.InKey.INIT_WID: init_wid,
        decoder.InKey.CAPTIONID: pos_captionids,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode, is_trn=False)
      log_prob = out_ops[decoder.OutKey.LOG_PROB]
      norm_log_prob = tf.reduce_sum(log_prob*pos_caption_masks[:, 1:], axis=1) / \
        tf.reduce_sum(pos_caption_masks[:, 1:], axis=1) # (num_pos,)

      # neg
      captionids = in_ops[self.InKey.NS_CAPTIONID]
      caption_masks = in_ops[self.InKey.NS_CAPTION_MASK]
      expanded_ft_embed = tf.tile(tf.expand_dims(ft_embed, 1), [1, self._config.num_neg, 1]) # (num_pos, num_neg, dim_output)
      expanded_ft_embed = tf.reshape(expanded_ft_embed, (-1, self._config.subcfgs[VE].dim_output)) # (num_pos*num_neg, dim_output)
      neg_captionid = captionids[:self._config.num_neg]
      neg_captionid = tf.tile(tf.expand_dims(neg_captionid, 0), [num_pos, 1, 1]) # (num_pos, num_neg, num_step)
      neg_captionid = tf.reshape(neg_captionid, (-1, self._config.subcfgs[VD].num_step))
      neg_caption_masks = caption_masks[:self._config.num_neg]
      neg_caption_masks = tf.tile(tf.expand_dims(neg_caption_masks, 0), [num_pos, 1, 1]) # (num_pos, num_neg, num_step)
      neg_caption_masks = tf.reshape(neg_caption_masks, (-1, self._config.subcfgs[VD].num_step))
      init_wid = tf.zeros((num_pos*self._config.num_neg,), dtype=tf.int32)

      vd_inputs = {
        decoder.InKey.FT: expanded_ft_embed,
        decoder.InKey.INIT_WID: init_wid,
        decoder.InKey.CAPTIONID: neg_captionid,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode, is_trn=False)

      neg_log_prob = out_ops[decoder.OutKey.LOG_PROB]
      norm_neg_log_prob = tf.reduce_sum(neg_log_prob * neg_caption_masks[:, 1:], axis=1) / \
        tf.reduce_sum(neg_caption_masks[:, 1:], axis=1) # (num_pos*num_neg,)
      norm_neg_log_prob = tf.reshape(norm_neg_log_prob, (-1, self._config.num_neg)) # (num_pos, num_neg)

      # sample
      expanded_ft_embed = tf.tile(tf.expand_dims(ft_embed, 1), [1, self._config.num_sample, 1]) # (num_pos, num_sample, dim_output)
      expanded_ft_embed = tf.reshape(expanded_ft_embed, (-1, self._config.subcfgs[VE].dim_output)) # (num_pos*num_sample, dim_output)
      sample_captionid = captionids[self._config.num_neg:] # (num_pos*num_sample, num_step)
      sample_caption_masks = caption_masks[self._config.num_neg:]
      init_wid = tf.zeros((num_pos*self._config.num_sample,), dtype=tf.int32)

      vd_inputs = {
        decoder.InKey.FT: expanded_ft_embed,
        decoder.InKey.INIT_WID: init_wid,
        decoder.InKey.CAPTIONID: sample_captionid,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode, is_trn=False)

      sample_log_prob = out_ops[decoder.OutKey.LOG_PROB]
      norm_sample_log_prob = tf.reduce_sum(sample_log_prob * sample_caption_masks[:, 1:], axis=1) / \
        tf.reduce_sum(sample_caption_masks[:, 1:], axis=1) # (num_pos*num_sample,)
      norm_sample_log_prob = tf.reshape(norm_sample_log_prob, (-1, self._config.num_sample)) # (num_pos, num_sample)

      return {
        # self.OutKey.OUT_WID: out_wid,
        # self.OutKey.LOGIT: logit,
        self.OutKey.LOG_PROB: norm_log_prob, # (num_pos,)
        self.OutKey.NLOG_PROB: norm_neg_log_prob, # (num_pos, num_neg)
        self.OutKey.SLOG_PROB: norm_sample_log_prob, # (num_pos, num_sample)
      }

    def tst():
      return base.tst_retrieval_ops(
        self, ft_embed, in_ops[self.InKey.CAPTIONID], in_ops[self.InKey.CAPTION_MASK], decoder)

    delegate = {
      framework.model.module.Mode.TRN_VAL: trn_val,
      framework.model.module.Mode.TST: tst,
    }
    return delegate[mode]()

  def _add_loss(self):
    with tf.variable_scope(self.name_scope):
      log_prob = self._outputs[self.OutKey.LOG_PROB]
      log_prob = tf.expand_dims(log_prob, 1) # (None, 1)
      neg_log_prob = self._outputs[self.OutKey.NLOG_PROB] # (None, num_neg)

      deltas = self._inputs[self.InKey.DELTA]
      max_margin = self._config.max_margin * tf.ones_like(deltas, dtype=tf.float32)
      margin = tf.minimum(deltas, max_margin)
      margin_loss = margin + neg_log_prob - log_prob
      margin_loss = tf.maximum(tf.zeros_like(margin_loss), margin_loss)
      margin_loss = tf.reduce_mean(margin_loss)
      self.op2monitor['pn_margin_loss'] = margin_loss
      loss_op = self._config.alphas[0] * margin_loss

      sample_deltas = self._inputs[self.InKey.SDELTA] # (num_pos*num_sample, 1 + num_pos*num_neg)
      sample_constraints = self._inputs[self.InKey.SCONSTRAINT] # (num_pos*num_sample, 1 + num_pos*num_neg)
      sample_log_prob = self._outputs[self.OutKey.SLOG_PROB] # (num_pos, num_sample)

      ps_deltas = sample_deltas[:, 0]
      ps_deltas = tf.reshape(ps_deltas, (-1, self._config.num_sample))
      ps_constraints = sample_constraints[:, 0]
      ps_constraints = tf.reshape(ps_constraints, (-1, self._config.num_sample))
      margin_loss = ps_deltas + sample_log_prob - log_prob
      margin_loss = tf.maximum(tf.zeros_like(margin_loss), margin_loss)
      margin_loss = tf.reduce_sum(margin_loss * ps_constraints) / tf.maximum(tf.reduce_sum(ps_constraints), 1.)
      self.op2monitor['ps_margin_loss'] = margin_loss
      loss_op += self._config.alphas[1] * margin_loss

      sn_deltas = sample_deltas[:, 1:] # (num_pos*num_sample, num_pos*num_neg)
      sn_constraints = sample_constraints[:, 1:]
      sample_log_prob = tf.reshape(sample_log_prob, (-1, 1)) # (num_pos*num_sample, 1)
      neg_log_prob = tf.reshape(neg_log_prob, (1, -1)) # (1, num_pos*num_neg)
      margin_loss = sn_deltas + neg_log_prob - sample_log_prob
      margin_loss = tf.maximum(tf.zeros_like(margin_loss), margin_loss)
      margin_loss = tf.reduce_sum(margin_loss * sn_constraints) / tf.maximum(tf.reduce_sum(sn_constraints), 1.)
      self.op2monitor['sn_margin_loss'] = margin_loss
      loss_op += self._config.alphas[2] * margin_loss

      self.op2monitor['loss'] = loss_op

    return loss_op

  def op_in_val(self, **kwargs):
    return {
      self.OutKey.LOG_PROB: self._outputs[self.OutKey.LOG_PROB],
    }

  def op_in_tst(self, **kwargs):
    return {
      self.OutKey.LOG_PROB: self._outputs[self.OutKey.LOG_PROB],
    }


class PathCfg(framework.model.trntst.PathCfg):
  def __init__(self):
    framework.model.trntst.PathCfg.__init__(self)
    # manually provided in the cfg file
    self.split_dir = ''
    self.annotation_dir = ''
    self.output_dir = ''
    self.trn_ftfiles = []
    self.val_ftfiles = []
    self.tst_ftfiles = []
    self.quotient_file = ''
    self.val_retrieve_file = ''
    self.tst_retrieve_file = ''
    self.trn_sample_file = ''

    # automatically generated paths
    self.trn_videoid_file = ''
    self.val_videoid_file = ''
    self.tst_videoid_file = ''
    self.trn_annotation_file = ''
    self.val_annotation_file = ''
    self.tst_annotation_file = ''
    self.groundtruth_file = ''
    self.word_file = ''


class TrnTst(framework.model.trntst.TrnTst):
  def __init__(self, model_cfg, path_cfg, model):
    framework.model.trntst.TrnTst.__init__(self, model_cfg, path_cfg, model)

    # caption int to string
    self.int2str = framework.util.caption.utility.CaptionInt2str(path_cfg.word_file)

  def _construct_feed_dict_in_trn(self, data):
    return {
      self.model.inputs[self.model.InKey.FT]: data['fts'],
      self.model.inputs[self.model.InKey.CAPTIONID]: data['captionids'],
      self.model.inputs[self.model.InKey.CAPTION_MASK]: data['caption_masks'],
      self.model.inputs[self.model.InKey.NS_CAPTIONID]: data['neg_sample_captionids'],
      self.model.inputs[self.model.InKey.NS_CAPTION_MASK]: data['neg_sample_caption_masks'],
      self.model.inputs[self.model.InKey.DELTA]: data['deltas'],
      self.model.inputs[self.model.InKey.SDELTA]: data['sample_deltas'],
      self.model.inputs[self.model.InKey.SCONSTRAINT]: data['sample_constraints'],
      self.model.inputs[self.model.InKey.IS_TRN]: True,
    }

  def feed_data_and_run_loss_op_in_val(self, data, sess):
    pass

  def predict_and_eval_in_val(self, sess, tst_reader, metrics):
    base.val_retrieval_task(self, sess, tst_reader, metrics)

  def predict_in_tst(self, sess, tst_reader, predict_file):
    base.predict_retrieval_task(self, sess, tst_reader, predict_file, 
      self.model_cfg.topk)


class TrnReader(framework.model.data.Reader):
  def __init__(self, 
      ft_files, videoid_file, quotient_file, annotation_file, captionstr_file, sample_file, word_file,
      model_cfg):
    self.fts = np.empty(0)
    self.ft_idxs = np.empty(0)
    self.captionids = np.empty(0)
    self.caption_masks = np.empty(0)
    self.quotient_dict = {}
    self.videoid2captions = {}
    self.videoid2sample_captions = {}
    self.num_neg = model_cfg.num_neg
    self.num_sample = model_cfg.num_sample
    self.sn_delta_threshold = model_cfg.sn_delta_threshold
    self.word2wid = {}
    self.sample_captionids = np.empty(0)
    self.sample_caption_masks = np.empty(0)
    self.sample_ciders = np.empty(0)
    self.vid2sample_idxs = {}

    self.shuffled_idxs = []
    self.num_caption = 0

    fts = []
    for ft_file in ft_files:
      ft = np.load(ft_file)
      fts.append(ft)
    self.fts = np.concatenate(fts, axis=1)
    self.fts = self.fts.astype(np.float32)

    self.videoids = np.load(open(videoid_file))

    data = cPickle.load(file(annotation_file))
    self.ft_idxs = data[0]
    self.captionids = data[1]
    self.caption_masks = data[2]
    self.num_caption = self.ft_idxs.shape[0]

    with open(quotient_file) as f:
      data = json.load(f)
      for key in data:
        self.quotient_dict[key] = set(data[key])

    with open(captionstr_file) as f:
      videoid2captions = cPickle.load(f)
    for videoid in self.videoids:
      self.videoid2captions[videoid] = videoid2captions[videoid]

    self.cider = fast_cider.CiderScorer()
    self.cider.init_refs(self.videoid2captions)

    with open(word_file) as f:
      data = cPickle.load(f)
    for i, word in enumerate(data):
      self.word2wid[word] = i

    self.int2str = framework.util.caption.utility.CaptionInt2str(word_file)

    with open(sample_file) as f:
      vids, self.sample_captionids, self.sample_caption_masks, self.sample_ciders = cPickle.load(f)
    num = vids.shape[0]
    for i in range(num):
      vid = vids[i]
      if vid not in self.vid2sample_idxs:
        self.vid2sample_idxs[vid] = []
      self.vid2sample_idxs[vid].append(i)
    print 'load complete'

    self.shuffled_idxs = range(self.num_caption)
    random.shuffle(self.shuffled_idxs)

  def num_record(self):
    return self.num_caption

  def yield_trn_batch(self, batch_size):
    for i in range(0, self.num_caption, batch_size):
      start = i
      end = min(i + batch_size, self.num_caption)
      idxs = self.shuffled_idxs[start:end]

      fts = self.fts[self.ft_idxs[idxs]]
      captionids = self.captionids[idxs]
      caption_masks = self.caption_masks[idxs]
      vids = self.videoids[self.ft_idxs[idxs]]

      # neg
      quotient_set = set()
      for idx in idxs:
        quotient_set.add(idx)
        if idx in self.quotient_dict:
          quotient_set = quotient_set | self.quotient_dict[idx]

      ridxs = range(self.num_caption)
      random.shuffle(ridxs)
      neg_idxs = []
      for idx in ridxs:
        if idx not in quotient_set:
          neg_idxs.append(idx)
        if len(neg_idxs) == self.num_neg:
          break

      neg_captionids = self.captionids[neg_idxs]
      neg_caption_masks = self.caption_masks[neg_idxs]
      neg_captions = self.int2str(neg_captionids)

      deltas = []
      neg_ciders = []
      for j in range(self.num_neg):
        score, scores = self.cider.compute_cider([neg_captions[j]]*(end-start), vids)
        neg_ciders.append(scores)
        scores = 1.0 - scores
        scores = np.maximum(scores, np.zeros(scores.shape))
        deltas.append(scores)
      neg_ciders = np.array(neg_ciders, dtype=np.float32).T # (num_pos, num_neg)
      deltas = np.array(deltas, dtype=np.float32).T # (num_pos, num_neg)

      # sample
      sample_idxs = []
      for vid in vids:
        idxs = self.vid2sample_idxs[vid]
        random.shuffle(idxs)
        sample_idxs += idxs[:self.num_sample]
      ciders = self.sample_ciders[sample_idxs]
      sample_captionids = self.sample_captionids[sample_idxs]
      sample_caption_masks = self.sample_caption_masks[sample_idxs]

      ciders = np.expand_dims(ciders, 1)
      neg_ciders = np.expand_dims(neg_ciders.reshape((-1,)), 0) # (1, num_pos*num_neg)
      sp_deltas = 1.0 - ciders
      sp_constraints = sp_deltas > 0
      sn_deltas = ciders - neg_ciders
      sn_constraints = sn_deltas > self.sn_delta_threshold
      sample_deltas = np.concatenate([sp_deltas, sn_deltas], axis=1) # (num_pos*num_sample, 1+num_pos*num_neg)
      constraints = np.concatenate([sp_constraints, sn_constraints], axis=1)
      constraints = np.array(constraints, dtype=np.float32)

      yield {
        'fts': fts,
        'captionids': captionids,
        'caption_masks': caption_masks,
        'neg_sample_captionids': np.concatenate([neg_captionids, sample_captionids], axis=0),
        'neg_sample_caption_masks': np.concatenate([neg_caption_masks, sample_caption_masks], axis=0),
        'deltas': deltas,
        'sample_deltas': sample_deltas,
        'sample_constraints': constraints,
        'vids': vids,
      }


def caption2captionid_mask(caption, num_step, word2wid):
  captionid = np.zeros((num_step,), dtype=np.int32)
  caption_mask = np.zeros((num_step,), dtype=np.float32)
  words = caption.split(' ')
  for i, word in enumerate(words):
    if i+1 == num_step:
      break
    if word in word2wid:
      wid = word2wid[word]
    else:
      wid = 2
    captionid[i+1] = wid
    caption_mask[i+1] = 1.
  i = i+1
  while i+1 < num_step:
    captionid[i+1] = 1
    caption_mask[i+1] = 1.

  return captionid, caption_mask


ValReader = qvevd.ValReader
TstRetrievalReader = qvevd.TstRetrievalReader
