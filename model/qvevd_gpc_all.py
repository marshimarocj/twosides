import enum
import sys
import os
import cPickle
import json
import random
sys.path.append('../')

import numpy as np
import tensorflow as tf

from bleu.bleu import Bleu
from cider.cider import Cider
from rouge.rouge import Rouge

import framework.model.module
import framework.model.trntst
import framework.model.data
from framework.model.module import Mode
import framework.util.caption.utility
import base
import encoder.dnn
import decoder.rnn
import qvevd_score_sample
import fast_cider


VE = 'encoder'
VD = 'decoder'
CELL = decoder.rnn.CELL


class ModelConfig(framework.model.module.ModelConfig):
  def __init__(self):
    framework.model.module.ModelConfig.__init__(self)

    self.subcfgs[VE] = encoder.dnn.Config()
    self.subcfgs[VD] = decoder.rnn.Config()
    self.num_gt_neg = 1
    self.num_greedy_neg = 1
    self.num_sample_neg = 1
    self.num_sample = 1
    self.num_hard_neg = 3
    self.margin = .0

    self.topk = 10
    self.strategy = 'beam'

  def _assert(self):
    assert self.subcfgs[VE].dim_output == self.subcfgs[VD].subcfgs[CELL].dim_hidden


def gen_cfg(**kwargs):
  cfg = ModelConfig()
  cfg.val_iter = -1
  cfg.val_loss = False
  cfg.monitor_iter = 100
  cfg.trn_batch_size = 64
  cfg.tst_batch_size = 64
  cfg.base_lr = 1e-5
  cfg.num_epoch = kwargs['num_epoch']
  cfg.num_gt_neg = kwargs['num_gt_neg']
  cfg.num_greedy_neg = kwargs['num_greedy_neg']
  cfg.num_sample_neg = kwargs['num_sample_neg']
  cfg.num_sample = kwargs['num_sample']
  cfg.num_hard_neg = kwargs['num_hard_neg']
  cfg.margin = kwargs['margin']
  cfg.topk = kwargs['topk']

  enc = cfg.subcfgs[VE]
  enc.dim_fts = kwargs['dim_fts']
  enc.dim_output = kwargs['dim_hidden']
  enc.keepin_prob = kwargs['content_keepin_prob']

  dec = cfg.subcfgs[VD]
  dec.num_step = kwargs['num_step']
  dec.dim_input = kwargs['dim_input']
  dec.dim_hidden = kwargs['dim_hidden']

  cell = dec.subcfgs[CELL]
  cell.dim_hidden = kwargs['dim_hidden']
  cell.dim_input = kwargs['dim_input']
  cell.keepout_prob = kwargs['cell_keepout_prob']
  cell.keepin_prob = kwargs['cell_keepin_prob']

  return cfg


class Model(framework.model.module.AbstractPGModel):
  name_scope = 'vemd.Model'

  class InKey(enum.Enum):
    FT = 'fts'
    ROLL_CAPTIONID = 'sample_captionids'
    ROLL_CAPTION_MASK = 'sample_caption_masks'
    PS_CIDER = 'ps_cider'
    PN_CIDER = 'pn_cider'
    IS_TRN = 'is_training'

  class OutKey(enum.Enum):
    LOGIT = 'logit'
    OUT_WID = 'out_wid'
    LOG_PROB = 'log_prob'
    ROLL_LOG_PROB = 'roll_log_prob'
    BEAM_CUM_LOG_PROB = 'beam_cum_log_prob'
    BEAM_PRE = 'beam_pre'
    BEAM_END = 'beam_end'
    GREEDY_OUT_WID = 'greedy_out_wid'

  def _set_submods(self):
    return {
      VE: encoder.dnn.Encoder(self._config.subcfgs[VE]),
      VD: decoder.rnn.Decoder(self._config.subcfgs[VD]),
    }

  def _add_input_in_mode(self, mode):
    if mode == framework.model.module.Mode.TRN_VAL:
      with tf.variable_scope(self.name_scope):
        fts = tf.placeholder(
          tf.float32, shape=(None, sum(self._config.subcfgs[VE].dim_fts)), name=self.InKey.FT.value)
        is_training = tf.placeholder(
          tf.bool, shape=(), name=self.InKey.IS_TRN.value)
        # trn only
        rollout_captionids = tf.placeholder(
          tf.int32, shape=(None, self._config.num_sample, self._config.subcfgs[VD].num_step), name=self.InKey.ROLL_CAPTIONID.value)
        rollout_caption_masks = tf.placeholder(
          tf.float32, shape=(None, self._config.num_sample, self.config.subcfgs[VD].num_step), name=self.InKey.ROLL_CAPTION_MASK.value)
        ps_cider = tf.placeholder(
          tf.float32, shape=(None, self._config.num_sample), name=self.InKey.PS_CIDER.value)
        pn_cider = tf.placeholder(
          tf.float32, shape=(None, self._config.num_gt_neg + self._config.num_greedy_neg + self._config.num_sample_neg), name=self.InKey.PN_CIDER.value)

      return {
        self.InKey.FT: fts,
        self.InKey.ROLL_CAPTIONID: rollout_captionids,
        self.InKey.ROLL_CAPTION_MASK: rollout_caption_masks,
        self.InKey.PS_CIDER: ps_cider,
        self.InKey.PN_CIDER: pn_cider,
        self.InKey.IS_TRN: is_training,
      }
    else:
      with tf.variable_scope(self.name_scope):
        fts = tf.placeholder(
          tf.float32, shape=(None, sum(self._config.subcfgs[VE].dim_fts)), name=self.InKey.FT.value)
        is_training = tf.placeholder(
          tf.bool, shape=(), name=self.InKey.IS_TRN.value)

      return {
        self.InKey.FT: fts,
        self.InKey.IS_TRN: is_training,
      }

  def _build_parameter_graph(self):
    pass

  def get_out_ops_in_mode(self, in_ops, mode, **kwargs):
    encoder = self.submods[VE]
    decoder = self.submods[VD]

    out_ops = encoder.get_out_ops_in_mode({
      encoder.InKey.FT: in_ops[self.InKey.FT],
      encoder.InKey.IS_TRN: in_ops[self.InKey.IS_TRN],
    }, mode)
    ft_embed = out_ops[encoder.OutKey.EMBED] # (None, dim_output)

    def trn_val():
      batch_size = tf.shape(ft_embed)[0]

      # val
      init_wid = tf.zeros((batch_size,), dtype=tf.int32)
      vd_inputs = {
        decoder.InKey.FT: ft_embed,
        decoder.InKey.INIT_WID: init_wid,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.VAL, search_strategy='greedy')
      out_wid = out_ops[decoder.OutKey.OUT_WID]

      # sample
      num_step = self._config.subcfgs[VD].num_step
      expanded_ft_embed = tf.tile(tf.expand_dims(ft_embed, 1), [1, self._config.num_sample, 1]) # (None, num_sample, dim_output)
      expanded_ft_embed = tf.reshape(expanded_ft_embed, (-1, self._config.subcfgs[VE].dim_output)) # (None*num_sample, dim_output)
      sample_captionid = in_ops[self.InKey.ROLL_CAPTIONID] # (None, num_sample, num_step)
      sample_captionid = tf.reshape(sample_captionid, (-1, num_step)) # (None*num_sample, num_step)

      vd_inputs = {
        decoder.InKey.FT: expanded_ft_embed,
        decoder.InKey.CAPTIONID: sample_captionid,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, Mode.TRN, is_trn=False)

      rollout_log_prob = out_ops[decoder.OutKey.LOG_PROB] # (None*num_sample, num_step)
      rollout_log_prob = tf.reshape(rollout_log_prob, (-1, self._config.num_sample, num_step-1))

      return {
        self.OutKey.OUT_WID: out_wid,
        self.OutKey.ROLL_LOG_PROB: rollout_log_prob,
      }

    def rollout():
      batch_size = tf.shape(ft_embed)[0]
      init_wid = tf.zeros((batch_size,), dtype=tf.int32)

      vd_inputs = {
        decoder.InKey.FT: ft_embed,
        decoder.InKey.INIT_WID: init_wid,
      }
      if self._config.num_greedy_neg > 0:
        out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode,
          search_strategy='greedy')
        greedy_out_wid = out_ops[decoder.OutKey.OUT_WID]

      out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode, 
        search_strategy='sample', num_sample=self._config.num_sample + self._config.num_sample_neg, topk=-1)
      roll_out_wid = out_ops[decoder.OutKey.OUT_WID]

      out = {
        self.OutKey.OUT_WID: roll_out_wid,
      }
      if self._config.num_greedy_neg > 0:
        out[self.OutKey.GREEDY_OUT_WID] = greedy_out_wid
      return out

    def tst():
      return base.tst_generation_ops(self, ft_embed, decoder)

    delegate = {
      framework.model.module.Mode.TRN_VAL: trn_val,
      framework.model.module.Mode.ROLLOUT: rollout,
      framework.model.module.Mode.TST: tst,
    }
    return delegate[mode]()

  def _add_loss(self):
    with tf.variable_scope(self.name_scope):
      ps_ciders = self._inputs[self.InKey.PS_CIDER]
      pn_ciders = self._inputs[self.InKey.PN_CIDER] # (None, num_neg)
      pn_cider = tf.nn.top_k(pn_ciders, k=self._config.num_hard_neg)
      margin_rewards = tf.expand_dims(ps_ciders, 2) - tf.expand_dims(pn_ciders, 1) - self._config.margin
      margin_rewards = tf.reduce_mean(margin_rewards, axis=2) # (None, num_sample)

      reward = margin_rewards
      sample_log_prob = self._outputs[self.OutKey.ROLL_LOG_PROB]
      sample_caption_mask = self._inputs[self.InKey.ROLL_CAPTION_MASK]
      sample_caption_mask = sample_caption_mask[:, :, 1:]
      surrogate_loss = -tf.expand_dims(reward, axis=2) * sample_log_prob * sample_caption_mask
      surrogate_loss = tf.reduce_sum(surrogate_loss) / tf.reduce_sum(sample_caption_mask)
      self.op2monitor['surrogate_loss'] = surrogate_loss
      self.op2monitor['reward'] = tf.reduce_mean(reward)
      loss_op = surrogate_loss

    return loss_op

  def op_in_rollout(self, **kwargs):
    return self._rollout_outputs

  def op_in_val(self, **kwargs):
    return {
      self.OutKey.OUT_WID: self._outputs[self.OutKey.OUT_WID],
    }

  def op_in_tst(self, **kwargs):
    if self._config.strategy == 'beam':
      return {
        self.OutKey.OUT_WID: self._outputs[self.OutKey.OUT_WID],
        self.OutKey.BEAM_CUM_LOG_PROB: self._outputs[self.OutKey.BEAM_CUM_LOG_PROB],
        self.OutKey.BEAM_PRE: self._outputs[self.OutKey.BEAM_PRE],
        self.OutKey.BEAM_END: self._outputs[self.OutKey.BEAM_END],
      }
    elif self._config.strategy == 'sample':
      return {
        self.OutKey.OUT_WID: self._outputs[self.OutKey.OUT_WID],
        self.OutKey.LOG_PROB: self._outputs[self.OutKey.ROLL_LOG_PROB],
      }


PathCfg = qvevd_score_sample.PathCfg


class TrnTst(framework.model.trntst.PGTrnTst):
  def __init__(self, model_cfg, path_cfg, model, gen_sent_mode=1):
    framework.model.trntst.TrnTst.__init__(self, model_cfg, path_cfg, model)

    # caption int to string
    self.int2str = framework.util.caption.utility.CaptionInt2str(path_cfg.word_file)

    self.gen_sent_mode = gen_sent_mode

    self.cider = fast_cider.CiderScorer()
    with open(path_cfg.groundtruth_file) as f:
      vid2captions = cPickle.load(f)
    self.cider.init_refs(vid2captions)

  def feed_data_and_rollout(self, data, sess):
    op_dict = self.model.op_in_rollout()

    feed_dict = self._construct_feed_dict_in_rollout(data)
    if self.model_cfg.num_greedy_neg > 0:
      greedy_out_wids, roll_out_wids = sess.run(
        [op_dict[self.model.OutKey.GREEDY_OUT_WID], op_dict[self.model.OutKey.OUT_WID]], feed_dict=feed_dict)
    else:
      roll_out_wids = sess.run(
        op_dict[self.model.OutKey.OUT_WID], feed_dict=feed_dict)

    rollout_captionids, rollout_caption_masks = base.gen_captionid_masks_from_wids(roll_out_wids[:, :, :-1])

    vids = data['vids'] 
    if self.model_cfg.num_greedy_neg > 0:
      greedy_ciders = base.eval_cider_in_rollout(greedy_out_wids, vids, self.int2str, self.cider) # (None, 1)
      # print greedy_out_wids.shape, greedy_ciders.shape
    rollout_ciders = base.eval_cider_in_rollout(roll_out_wids, vids, self.int2str, self.cider) # (None, num_sample + num_sample_neg)

    data['ps_ciders'] = rollout_ciders[:, :self.model_cfg.num_sample]

    if 'neg_ciders' in data:
      data['neg_ciders'] = np.concatenate([data['neg_ciders'], rollout_ciders[:, self.model_cfg.num_sample:]], 1)
    else:
      data['neg_ciders'] = rollout_ciders[:, self.model_cfg.num_sample:]
    if self.model_cfg.num_greedy_neg > 0:
      data['neg_ciders'] = np.concatenate([data['neg_ciders'], greedy_ciders], 1)

    data['rollout_captionids'] = rollout_captionids[:, :self.model_cfg.num_sample]
    data['rollout_caption_masks'] = rollout_caption_masks[:, :self.model_cfg.num_sample]

    return data

  def _construct_feed_dict_in_rollout(self, data):
    fts = data['fts']
    batch_size = fts.shape[0]

    return {
      self.model.rollout_inputs[self.model.InKey.FT]: fts,
      self.model.rollout_inputs[self.model.InKey.IS_TRN]: False,
    }

  def _construct_feed_dict_in_trn(self, data):
    return {
      self.model.inputs[self.model.InKey.FT]: data['fts'],
      self.model.inputs[self.model.InKey.PS_CIDER]: data['ps_ciders'],
      self.model.inputs[self.model.InKey.PN_CIDER]: data['neg_ciders'],
      self.model.inputs[self.model.InKey.ROLL_CAPTIONID]: data['rollout_captionids'],
      self.model.inputs[self.model.InKey.ROLL_CAPTION_MASK]: data['rollout_caption_masks'],
      self.model.inputs[self.model.InKey.IS_TRN]: True,
    }

  def feed_data_and_run_loss_op_in_val(self, data, sess):
    pass

  def predict_and_eval_in_val(self, sess, tst_reader, metrics):
    base.val_generation_task(self, sess, tst_reader, metrics)

  def predict_in_tst(self, sess, tst_reader, predict_file):
    base.predict_generation_task(self, sess, tst_reader, predict_file,
      self.model_cfg.strategy, self.model_cfg.subcfgs[VD].sent_pool_size)


class TrnReader(framework.model.data.Reader):
  def __init__(self, 
      ft_files, videoid_file, quotient_file, annotation_file, captionstr_file, word_file, num_neg,
      metric='cider'):
    self.fts = np.empty(0)
    self.ft_idxs = np.empty(0)
    self.captionids = np.empty(0)
    self.caption_masks = np.empty(0)
    # self.quotient_dict = {}
    self.videoid2captions = {}
    self.num_neg = num_neg
    self.metric = metric

    self.shuffled_idxs = []
    self.num_caption = 0

    fts = []
    for ft_file in ft_files:
      ft = np.load(ft_file)
      fts.append(ft)
    self.fts = np.concatenate(tuple(fts), axis=1)
    self.fts = self.fts.astype(np.float32)

    self.videoids = np.load(open(videoid_file))

    data = cPickle.load(file(annotation_file))
    self.ft_idxs = data[0]
    self.captionids = data[1]
    self.caption_masks = data[2]
    self.num_caption = self.ft_idxs.shape[0]

    # with open(quotient_file) as f:
    #   data = json.load(f)
    #   for key in data:
    #     self.quotient_dict[key] = set(data[key])

    videoid2captions = cPickle.load(open(captionstr_file))
    for videoid in self.videoids:
      self.videoid2captions[videoid] = videoid2captions[videoid]

    if self.metric == 'cider':
      self.cider = fast_cider.CiderScorer()
      self.cider.init_refs(self.videoid2captions)
    self.int2str = framework.util.caption.utility.CaptionInt2str(word_file)

    self.shuffled_idxs = range(self.num_caption)
    random.shuffle(self.shuffled_idxs)

  def num_record(self):
    return self.num_caption

  def yield_trn_batch(self, batch_size):
    for i in range(0, self.num_caption, batch_size):
      start = i
      end = min(i + batch_size, self.num_caption)
      num = end-start
      idxs = self.shuffled_idxs[start:end]

      fts = self.fts[self.ft_idxs[idxs]]
      captionids = self.captionids[idxs]
      caption_masks = self.caption_masks[idxs]
      vids = self.videoids[self.ft_idxs[idxs]]

      # quotient_set = set()
      # for idx in idxs:
      #   quotient_set.add(idx)
      #   if idx in self.quotient_dict:
      #     quotient_set = quotient_set | self.quotient_dict[idx]

      batch_data = {
        'fts': fts,
        'captionids': captionids,
        'caption_masks': caption_masks,
        'vids': vids,
      }

      if self.num_neg > 0:
        idxs = range(self.num_caption)
        random.shuffle(idxs)

        neg_idxs = []
        for idx in idxs:
          # if idx not in quotient_set:
          neg_idxs.append(idx)
          if len(neg_idxs) == self.num_neg:
            break

        neg_captionids = self.captionids[neg_idxs]
        neg_caption_masks = self.caption_masks[neg_idxs]
        neg_captions = self.int2str(neg_captionids)

        if self.metric == 'cider':
          neg_ciders = []
          for i in range(self.num_neg):
            score, scores = self.cider.compute_cider([neg_captions[i]]*(end-start), vids)
            neg_ciders.append(scores)
          neg_ciders = np.array(neg_ciders, dtype=np.float32).T # (num_pos, num_neg)
          batch_data.update({
            'neg_ciders': neg_ciders,
          })

      yield batch_data


ValReader = qvevd_score_sample.ValReader
TstGenerationReader = qvevd_score_sample.TstGenerationReader
