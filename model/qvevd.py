import enum
import sys
import os
import cPickle
import json
import random
sys.path.append('../')

import numpy as np
import tensorflow as tf

from bleu import bleu
from cider import cider

import framework.model.module
import framework.model.trntst
import framework.model.data
import framework.util.caption.utility
import base
import encoder.dnn
import decoder.rnn

VE = 'encoder'
VD = 'decoder'
CELL = decoder.rnn.CELL


class ModelConfig(framework.model.module.ModelConfig):
  def __init__(self):
    framework.model.module.ModelConfig.__init__(self)

    self.subcfgs[VE] = encoder.dnn.Config()
    self.subcfgs[VD] = decoder.rnn.Config()
    self.alpha = 1.
    self.num_neg = 64
    self.margin = 0.1

    self.tst_task = 'generation'
    self.topk = 10
    self.strategy = 'beam'
    self.sample_topk = -1
    self.num_sample_in_tst = 100

  def _assert(self):
    assert self.subcfgs[VE].dim_output == self.subcfgs[VD].subcfgs[CELL].dim_hidden


def gen_cfg(**kwargs):
  cfg = ModelConfig()
  cfg.val_iter = -1
  cfg.val_loss = False
  cfg.monitor_iter = 100
  cfg.trn_batch_size = 32
  cfg.tst_batch_size = 64
  cfg.base_lr = 1e-4
  cfg.num_epoch = kwargs['num_epoch']
  cfg.alpha = kwargs['alpha']
  cfg.num_neg = kwargs['num_neg']
  cfg.tst_task = kwargs['tst_task']
  cfg.topk = kwargs['topk']

  enc = cfg.subcfgs[VE]
  enc.dim_fts = kwargs['dim_fts']
  enc.dim_output = kwargs['dim_hidden']
  enc.keepin_prob = kwargs['content_keepin_prob']

  dec = cfg.subcfgs[VD]
  dec.num_step = kwargs['num_step']
  dec.dim_input = kwargs['dim_input']
  dec.dim_hidden = kwargs['dim_hidden']

  cell = dec.subcfgs[CELL]
  cell.dim_hidden = kwargs['dim_hidden']
  cell.dim_input = kwargs['dim_input']
  cell.keepout_prob = kwargs['cell_keepout_prob']
  cell.keepin_prob = kwargs['cell_keepin_prob']

  return cfg


class Model(framework.model.module.AbstractModel):
  name_scope = 'vemd.Model'

  class InKey(enum.Enum):
    FT = 'fts'
    CAPTIONID = 'captionids'
    CAPTION_MASK = 'caption_masks'
    NCAPTIONID = 'neg_captionids'
    NCAPTION_MASK = 'neg_caption_masks'
    IS_TRN = 'is_training'

  class OutKey(enum.Enum):
    LOGIT = 'logit'
    OUT_WID = 'out_wid'
    LOG_PROB = 'log_prob'
    NLOG_PROB = 'neg_log_prob'
    BEAM_CUM_LOG_PROB = 'beam_cum_log_prob'
    BEAM_PRE = 'beam_pre'
    BEAM_END = 'beam_end'

  def _set_submods(self):
    return {
      VE: encoder.dnn.Encoder(self._config.subcfgs[VE]),
      VD: decoder.rnn.Decoder(self._config.subcfgs[VD]),
    }

  def _add_input_in_mode(self, mode):
    if mode == framework.model.module.Mode.TRN_VAL:
      with tf.variable_scope(self.name_scope):
        fts = tf.placeholder(
          tf.float32, shape=(None, sum(self._config.subcfgs[VE].dim_fts)), name=self.InKey.FT.value)
        is_training = tf.placeholder(
          tf.bool, shape=(), name=self.InKey.IS_TRN.value)
        # trn only
        captionids = tf.placeholder(
          tf.int32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.CAPTIONID.value)
        caption_masks = tf.placeholder(
          tf.float32, shape=(None, self.config.subcfgs[VD].num_step), name=self.InKey.CAPTION_MASK.value)
        neg_captionids = tf.placeholder(
          tf.int32, shape=(self._config.num_neg, self._config.subcfgs[VD].num_step), name=self.InKey.NCAPTIONID.value)
        neg_caption_masks = tf.placeholder(
          tf.float32, shape=(self._config.num_neg, self.config.subcfgs[VD].num_step), name=self.InKey.NCAPTION_MASK.value)

        out = {
          self.InKey.FT: fts,
          self.InKey.IS_TRN: is_training,
          self.InKey.CAPTIONID: captionids,
          self.InKey.CAPTION_MASK: caption_masks,
          self.InKey.NCAPTIONID: neg_captionids,
          self.InKey.NCAPTION_MASK: neg_caption_masks,
        }
      return out
    else:
      with tf.variable_scope(self.name_scope):
        fts = tf.placeholder(
          tf.float32, shape=(None, sum(self._config.subcfgs[VE].dim_fts)), name=self.InKey.FT.value)
        captionids = tf.placeholder(
          tf.int32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.CAPTIONID.value)
        caption_masks = tf.placeholder(
          tf.float32, shape=(None, self._config.subcfgs[VD].num_step), name=self.InKey.CAPTION_MASK.value)
        is_training = tf.placeholder(
          tf.bool, shape=(), name=self.InKey.IS_TRN.value)

      return {
        self.InKey.FT: fts,
        self.InKey.CAPTIONID: captionids,
        self.InKey.CAPTION_MASK: caption_masks,
        self.InKey.IS_TRN: is_training,
      }

  def _build_parameter_graph(self):
    pass

  def get_out_ops_in_mode(self, in_ops, mode, **kwargs):
    encoder = self.submods[VE]
    decoder = self.submods[VD]

    out_ops = encoder.get_out_ops_in_mode({
      encoder.InKey.FT: in_ops[self.InKey.FT],
      encoder.InKey.IS_TRN: in_ops[self.InKey.IS_TRN],
    }, mode)
    ft_embed = out_ops[encoder.OutKey.EMBED] # (None, dim_output)

    def trn_val(ft_embed):
      batch_size = tf.shape(ft_embed)[0]

      # pos
      caption_masks = in_ops[self.InKey.CAPTION_MASK]
      init_wid = tf.zeros((batch_size,), dtype=tf.int32)

      vd_inputs = {
        decoder.InKey.FT: ft_embed,
        decoder.InKey.INIT_WID: init_wid,
        decoder.InKey.CAPTIONID: in_ops[self.InKey.CAPTIONID],
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode, is_trn=True)
      out_wid = out_ops[decoder.OutKey.OUT_WID]
      logit = out_ops[decoder.OutKey.LOGIT]

      out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode, is_trn=False)
      log_prob = out_ops[decoder.OutKey.LOG_PROB]
      norm_log_prob = tf.reduce_sum(log_prob*caption_masks[:, 1:], axis=1) / \
        tf.reduce_sum(caption_masks[:, 1:], axis=1) # (None,)

      # neg
      ft_embed = tf.tile(tf.expand_dims(ft_embed, 1), [1, self._config.num_neg, 1]) # (None, num_neg, dim_output)
      ft_embed = tf.reshape(ft_embed, (-1, self._config.subcfgs[VE].dim_output)) # (None*num_neg, dim_output)
      neg_captionid = in_ops[self.InKey.NCAPTIONID]
      neg_captionid = tf.tile(tf.expand_dims(neg_captionid, 0), [batch_size, 1, 1]) # (None, num_neg, num_step)
      neg_captionid = tf.reshape(neg_captionid, (-1, self._config.subcfgs[VD].num_step))
      neg_caption_masks = in_ops[self.InKey.NCAPTION_MASK]
      neg_caption_masks = tf.tile(tf.expand_dims(neg_caption_masks, 0), [batch_size, 1, 1]) # (None, num_neg, num_step)
      neg_caption_masks = tf.reshape(neg_caption_masks, (-1, self._config.subcfgs[VD].num_step))
      init_wid = tf.zeros((batch_size*self._config.num_neg,), dtype=tf.int32)

      vd_inputs = {
        decoder.InKey.FT: ft_embed,
        decoder.InKey.INIT_WID: init_wid,
        decoder.InKey.CAPTIONID: neg_captionid,
      }
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode, is_trn=False)

      neg_log_prob = out_ops[decoder.OutKey.LOG_PROB]
      norm_neg_log_prob = tf.reduce_sum(neg_log_prob * neg_caption_masks[:, 1:], axis=1) / \
        tf.reduce_sum(neg_caption_masks[:, 1:], axis=1) # (None*num_neg,)
      norm_neg_log_prob = tf.reshape(norm_neg_log_prob, (-1, self._config.num_neg)) # (None, num_neg)

      return {
        self.OutKey.OUT_WID: out_wid,
        self.OutKey.LOGIT: logit,
        self.OutKey.LOG_PROB: norm_log_prob,
        self.OutKey.NLOG_PROB: norm_neg_log_prob,
      }

    def tst_generation(ft_embed):
      if self._config.strategy == 'beam':
        batch_size = tf.shape(ft_embed)[0]
        init_wid = tf.zeros((batch_size,), dtype=tf.int32)

        vd_inputs = {
          decoder.InKey.FT: ft_embed,
          decoder.InKey.INIT_WID: init_wid,
        }
        out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode, 
          task='generation', strategy=self._config.strategy)
        return {
          self.OutKey.OUT_WID: out_ops[decoder.OutKey.OUT_WID],
          self.OutKey.BEAM_CUM_LOG_PROB: out_ops[decoder.OutKey.BEAM_CUM_LOG_PROB],
          self.OutKey.BEAM_PRE: out_ops[decoder.OutKey.BEAM_PRE],
          self.OutKey.BEAM_END: out_ops[decoder.OutKey.BEAM_END],
        }
      elif self._config.strategy == 'sample':
        batch_size = tf.shape(ft_embed)[0]
        init_wid = tf.zeros((batch_size,), dtype=tf.int32)

        vd_inputs = {
          decoder.InKey.FT: ft_embed,
          decoder.InKey.INIT_WID: init_wid,
        }
        out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode, 
          task='generation', strategy=self._config.strategy, topk=self._config.sample_topk, num_sample=self._config.num_sample_in_tst)
        out_wids = out_ops[decoder.OutKey.OUT_WID]
        return {
          self.OutKey.OUT_WID: out_ops[decoder.OutKey.OUT_WID], # (None, num_sample, num_step)
          self.OutKey.LOG_PROB: out_ops[decoder.OutKey.LOG_PROB], # (None, num_sample, num_step)
        }

    def tst_retrieval(ft_embed):
      batch_size = tf.shape(ft_embed)[0]
      init_wid = tf.zeros((batch_size,), dtype=tf.int32)

      vd_inputs = {
        decoder.InKey.FT: ft_embed,
        decoder.InKey.CAPTIONID: in_ops[self.InKey.CAPTIONID],
        decoder.InKey.INIT_WID: init_wid,
      }
      decoder.is_trn = False
      out_ops = decoder.get_out_ops_in_mode(vd_inputs, mode, task='retrieval')

      caption_masks = in_ops[self.InKey.CAPTION_MASK]
      log_prob = out_ops[decoder.OutKey.LOG_PROB]
      norm_log_prob = tf.reduce_sum(log_prob*caption_masks[:, 1:], axis=1) / \
        tf.reduce_sum(caption_masks[:, 1:], axis=1) # (None,)
      return {
        self.OutKey.LOG_PROB: norm_log_prob
      }

    def tst(ft_embed):
      if self._config.tst_task == 'generation':
        return tst_generation(ft_embed)
      elif self._config.tst_task == 'retrieval':
        return tst_retrieval(ft_embed)

    delegate = {
      framework.model.module.Mode.TRN_VAL: trn_val,
      framework.model.module.Mode.TST: tst,
    }
    return delegate[mode](ft_embed)

  def _add_loss(self):
    with tf.variable_scope(self.name_scope):
      logits = self._outputs[self.OutKey.LOGIT] # (None*num_step, num_words)
      xentropy_loss = framework.util.expanded_op.cross_entropy_loss_on_rnn_logits(
        self._inputs[self.InKey.CAPTIONID], self._inputs[self.InKey.CAPTION_MASK], logits)
      if self._config.alpha == 1.:
        loss_op = 0.
      else:
        self.op2monitor['xentropy_loss'] = xentropy_loss
        loss_op = (1. - self._config.alpha) * xentropy_loss

      log_prob = self._outputs[self.OutKey.LOG_PROB]
      log_prob = tf.expand_dims(log_prob, 1) # (None, 1)
      neg_log_prob = self._outputs[self.OutKey.NLOG_PROB] # (None, num_neg)

      margin_loss = self._config.margin + neg_log_prob - log_prob
      margin_loss = tf.maximum(tf.zeros_like(margin_loss), margin_loss)
      margin_loss = tf.reduce_mean(margin_loss)
      if self._config.alpha != 0.:
        self.op2monitor['margin_loss'] = margin_loss
        loss_op += self._config.alpha * margin_loss

      self.op2monitor['loss'] = loss_op

    return loss_op

  def op_in_val(self, **kwargs):
    assert 'task' in kwargs

    if kwargs['task'] == 'generation':
      return {
        self.OutKey.OUT_WID: self._outputs[self.OutKey.OUT_WID],
      }
    elif kwargs['task'] == 'retrieval':
      return {
        self.OutKey.LOG_PROB: self._outputs[self.OutKey.LOG_PROB],
      }

  def op_in_tst(self):
    if self._config.tst_task == 'generation':
      if self._config.strategy == 'beam':
        return {
          self.OutKey.OUT_WID: self._outputs[self.OutKey.OUT_WID],
          self.OutKey.BEAM_CUM_LOG_PROB: self._outputs[self.OutKey.BEAM_CUM_LOG_PROB],
          self.OutKey.BEAM_PRE: self._outputs[self.OutKey.BEAM_PRE],
          self.OutKey.BEAM_END: self._outputs[self.OutKey.BEAM_END],
        }
      elif self._config.strategy == 'sample':
        return {
          self.OutKey.OUT_WID: self._outputs[self.OutKey.OUT_WID],
          self.OutKey.LOG_PROB: self._outputs[self.OutKey.LOG_PROB],
        }
    elif self._config.tst_task == 'retrieval':
      return {
        self.OutKey.LOG_PROB: self._outputs[self.OutKey.LOG_PROB],
      }


class LiftedModel(Model):
  def _add_loss(self):
    with tf.variable_scope(self.name_scope):
      logits = self._outputs[self.OutKey.LOGIT] # (None*num_step, num_words)
      xentropy_loss = framework.util.expanded_op.cross_entropy_loss_on_rnn_logits(
        self._inputs[self.InKey.CAPTIONID], self._inputs[self.InKey.CAPTION_MASK], logits)
      self.op2monitor['xentropy_loss'] = xentropy_loss
      loss_op = (1. - self._config.alpha) * xentropy_loss

      log_prob = self._outputs[self.OutKey.LOG_PROB]
      neg_log_prob = self._outputs[self.OutKey.NLOG_PROB] # (None, num_neg)
      neg_log_prob = tf.reduce_logsumexp(100*neg_log_prob, axis=1) / 100.

      margin_loss = self._config.margin + neg_log_prob - log_prob
      margin_loss = tf.maximum(tf.zeros_like(margin_loss), margin_loss)
      margin_loss = tf.reduce_mean(margin_loss)
      self.op2monitor['margin_loss'] = margin_loss

      loss_op += self._config.alpha * margin_loss
      self.op2monitor['loss'] = loss_op

    return loss_op


class PathCfg(framework.model.trntst.PathCfg):
  def __init__(self):
    framework.model.trntst.PathCfg.__init__(self)
    # manually provided in the cfg file
    self.split_dir = ''
    self.annotation_dir = ''
    self.output_dir = ''
    self.trn_ftfiles = []
    self.val_ftfiles = []
    self.tst_ftfiles = []
    self.quotient_file = ''
    self.val_retrieve_file = ''
    self.tst_retrieve_file = ''

    # automatically generated paths
    self.trn_videoid_file = ''
    self.val_videoid_file = ''
    self.tst_videoid_file = ''
    self.trn_annotation_file = ''
    self.val_annotation_file = ''
    self.tst_annotation_file = ''
    self.groundtruth_file = ''
    self.word_file = ''


class TrnTst(base.TrnTst):
  def _construct_feed_dict_in_trn(self, data):
    fts = data['fts']
    captionids = data['captionids']
    caption_masks = data['caption_masks']
    neg_captionids = data['neg_captionids']
    neg_caption_masks = data['neg_caption_masks']

    return {
      self.model.inputs[self.model.InKey.FT]: fts,
      self.model.inputs[self.model.InKey.CAPTIONID]: captionids,
      self.model.inputs[self.model.InKey.CAPTION_MASK]: caption_masks,
      self.model.inputs[self.model.InKey.NCAPTIONID]: neg_captionids,
      self.model.inputs[self.model.InKey.NCAPTION_MASK]: neg_caption_masks,
      self.model.inputs[self.model.InKey.IS_TRN]: True,
    }

  def predict_and_eval_in_val(self, sess, tst_reader, metrics):
    # base.val_generation_task(self, sess, tst_reader, metrics)
    base.val_retrieval_task(self, sess, tst_reader, metrics)

  def predict_in_tst(self, sess, tst_reader, predict_file):
    if self.model_cfg.tst_task == 'generation':
      base.predict_generation_task(self, sess, tst_reader, predict_file,
        self.model_cfg.strategy, self.model_cfg.subcfgs[VD].sent_pool_size)
    elif self.model_cfg.tst_task == 'retrieval':
      base.predict_retrieval_task(self, sess, tst_reader, predict_file, 
        self.model_cfg.topk)


class DebugTst(TrnTst):
  def predict_in_tst(self, sess, tst_reader, predict_file):
    videoid2caption = {}
    cnt = 0
    op_dict = self.model.op_in_tst()
    for data in tst_reader.yield_tst_batch(self.model_cfg.tst_batch_size):
      feed_dict = {
        self.model.inputs[self.model.InKey.FT]: data['fts'],
        self.model.inputs[self.model.InKey.IS_TRN]: False,
      }
      out_wids, log_probs = sess.run(
        [op_dict[self.model.OutKey.OUT_WID], op_dict[self.model.OutKey.LOG_PROB]],
        feed_dict=feed_dict)
      captionids, caption_masks = base.gen_captionid_masks_from_wids(out_wids)
      caption_masks = caption_masks[:, :, 1:]
      norm_log_prob = np.sum(log_probs * caption_masks, axis=-1) / np.sum(caption_masks, axis=-1) # (None, num_sample)

      for i in range(captionids.shape[0]):
        videoid = str(tst_reader.videoids[i+cnt])
        captions = self.int2str(captionids[i])
        out = []
        for j in range(len(captions)):
          out.append({
            'caption': captions[j],
            'norm_log_prob': float(norm_log_prob[i, j]),
          })
        videoid2caption[videoid] = out

      cnt += captionids.shape[0]

    json.dump(videoid2caption, open(predict_file, 'w'), indent=2)


class SampleTst(TrnTst):
  def predict_in_tst(self, sess, tst_reader, predict_file):
    videoid2caption = {}
    cnt = 0
    op_dict = self.model.op_in_tst()
    for data in tst_reader.yield_tst_batch(self.model_cfg.tst_batch_size):
      feed_dict = {
        self.model.inputs[self.model.InKey.FT]: data['fts'],
        self.model.inputs[self.model.InKey.IS_TRN]: False,
      }
      out_wids, log_probs = sess.run(
        [op_dict[self.model.OutKey.OUT_WID], op_dict[self.model.OutKey.LOG_PROB]],
        feed_dict=feed_dict)
      captionids, caption_masks = base.gen_captionid_masks_from_wids(out_wids)
      caption_masks = caption_masks[:, :, 1:]
      norm_log_prob = np.sum(log_probs * caption_masks, axis=-1) / np.sum(caption_masks, axis=-1) # (None, num_sample)

      for i in range(captionids.shape[0]):
        videoid = str(tst_reader.videoids[i+cnt])
        captions = self.int2str(captionids[i])
        out = []
        for j in range(len(captions)):
          out.append((float(norm_log_prob[i, j]), captions[j]))
        videoid2caption[videoid] = out

      cnt += captionids.shape[0]

    json.dump(videoid2caption, open(predict_file, 'w'), indent=2)


class TrnReader(framework.model.data.Reader):
  def __init__(self, ft_files, quotient_file, annotation_file, num_neg):
    self.fts = np.empty(0)
    self.ft_idxs = np.empty(0)
    self.captionids = np.empty(0)
    self.caption_masks = np.empty(0)
    self.quotient_dict = {}
    self.num_neg = num_neg

    self.shuffled_idxs = []
    self.num_caption = 0

    fts = []
    for ft_file in ft_files:
      ft = np.load(ft_file)
      fts.append(ft)
    self.fts = np.concatenate(tuple(fts), axis=1)
    self.fts = self.fts.astype(np.float32)

    data = cPickle.load(file(annotation_file))
    self.ft_idxs = data[0]
    self.captionids = data[1]
    self.caption_masks = data[2]
    self.num_caption = self.ft_idxs.shape[0]

    with open(quotient_file) as f:
      data = json.load(f)
      for key in data:
        self.quotient_dict[key] = set(data[key])

    self.shuffled_idxs = range(self.num_caption)
    random.shuffle(self.shuffled_idxs)

  def num_record(self):
    return self.num_caption

  def yield_trn_batch(self, batch_size):
    for i in range(0, self.num_caption, batch_size):
      start = i
      end = i + batch_size
      idxs = self.shuffled_idxs[start:end]

      fts = self.fts[self.ft_idxs[idxs]]
      captionids = self.captionids[idxs]
      caption_masks = self.caption_masks[idxs]

      quotient_set = set()
      for idx in idxs:
        quotient_set.add(idx)
        if idx in self.quotient_dict:
          quotient_set = quotient_set | self.quotient_dict[idx]

      idxs = range(self.num_caption)
      random.shuffle(idxs)
      neg_idxs = []
      for idx in idxs:
        if idx not in quotient_set:
          neg_idxs.append(idx)
        if len(neg_idxs) == self.num_neg:
          break

      neg_captionids = self.captionids[neg_idxs]
      neg_caption_masks = self.caption_masks[neg_idxs]

      yield {
        'fts': fts,
        'captionids': captionids,
        'caption_masks': caption_masks,
        'neg_captionids': neg_captionids,
        'neg_caption_masks': neg_caption_masks,
      }


class ValReader(framework.model.data.Reader):
  def __init__(self, ft_files, videoid_file, 
      retrieval_annotation_file, captionstr_file):
    self.fts = np.empty(0)
    self.retrieval_ft_idxs = np.empty(0)
    self.retrieval_captionids = np.empty(0)
    self.retrieval_caption_masks = np.empty(0)
    self.videoid2captions = {}

    fts = []
    for ft_file in ft_files:
      ft = np.load(ft_file)
      fts.append(ft)
    self.fts = np.concatenate(tuple(fts), axis=1)
    self.fts = self.fts.astype(np.float32)
    self.num_ft = self.fts.shape[0]

    self.videoids = np.load(open(videoid_file))

    if retrieval_annotation_file != '':
      data = cPickle.load(file(retrieval_annotation_file))
      self.retrieval_ft_idxs = data[0]
      self.retrieval_captionids = data[1]
      self.retrieval_caption_masks = data[2]
      self.num_caption = self.retrieval_ft_idxs.shape[0]

    videoid2captions = cPickle.load(open(captionstr_file))
    for videoid in self.videoids:
      self.videoid2captions[videoid] = videoid2captions[videoid]

  def yield_tst_batch(self, batch_size, **kwargs):
    assert 'task' in kwargs
    if kwargs['task'] == 'generation':
      for i in range(0, self.num_ft, batch_size):
        start = i
        end = i + batch_size

        yield {
          'fts': self.fts[start:end],
        }
    elif kwargs['task'] == 'retrieval':
      for i in range(self.num_caption):
        yield {
          'ft': self.fts[self.retrieval_ft_idxs[i]],
          'captionids': self.retrieval_captionids,
          'caption_masks': self.retrieval_caption_masks,
        }


class TstGenerationReader(framework.model.data.Reader):
  def __init__(self, ft_files, videoid_file):
    self.fts = np.empty(0)

    fts = []
    for ft_file in ft_files:
      ft = np.load(ft_file)
      fts.append(ft)
    self.fts = np.concatenate(tuple(fts), axis=1)
    self.fts = self.fts.astype(np.float32)
    self.num_ft = self.fts.shape[0]

    self.videoids = np.load(open(videoid_file))

  def yield_tst_batch(self, batch_size, **kwargs):
    for i in range(0, self.num_ft, batch_size):
      start = i
      end = i + batch_size

      yield {
        'fts': self.fts[start:end],
      }


class TstRetrievalReader(framework.model.data.Reader):
  def __init__(self, ft_files, annotation_file):
    self.fts = np.empty(0)
    self.ft_idxs = np.empty(0)
    self.captionids = np.empty(0)
    self.caption_masks = np.empty(0)

    fts = []
    for ft_file in ft_files:
      ft = np.load(ft_file)
      fts.append(ft)
    self.fts = np.concatenate(tuple(fts), axis=1)
    self.fts = self.fts.astype(np.float32)
    self.num_ft = self.fts.shape[0]

    data = cPickle.load(file(annotation_file))
    self.ft_idxs = data[0]
    self.captionids = data[1]
    self.caption_masks = data[2]
    self.num_caption = self.ft_idxs.shape[0]

  def yield_tst_batch(self, batch_size, **kwargs):
    for i in range(self.num_caption):
      yield {
        'ft': self.fts[self.ft_idxs[i]],
        'captionids': self.captionids,
        'caption_masks': self.caption_masks,
      }


class TstRetrievalMSCOCOReader(framework.model.data.Reader):
  def __init__(self, ft_files, annotation_file):
    self.fts = np.empty(0)
    self.ft_idxs = np.empty(0)
    self.captionids = np.empty(0)
    self.caption_masks = np.empty(0)

    fts = []
    for ft_file in ft_files:
      ft = np.load(ft_file)
      fts.append(ft)
    self.fts = np.concatenate(tuple(fts), axis=1)
    self.fts = self.fts.astype(np.float32)
    self.num_ft = self.fts.shape[0]

    data = cPickle.load(file(annotation_file))
    self.ft_idxs = data[0]
    self.captionids = data[1]
    self.caption_masks = data[2]

  def yield_tst_batch(self, batch_size, **kwargs):
    prev_idx = -1
    for ft_idx in self.ft_idxs:
      if ft_idx != prev_idx:
        yield {
          'ft': self.fts[ft_idx],
          'captionids': self.captionids,
          'caption_masks': self.caption_masks,
        }
        prev_idx = ft_idx
